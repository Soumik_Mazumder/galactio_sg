import io.appium.java_client.TouchAction;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

/**
 * Created by Soumik on 5/3/2016.
 */
public class RECENT_FUNCTION {

    @Test(priority = 1)
    public void C001_Accessing_Recents_from_Main_Menu() throws Exception {
        TestBase utill = new TestBase();
        utill.initialize();
        Thread.sleep(2000);

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        }
        catch(Exception e){
            System.out.println("No Journey Popup Displaying");
        }

        //Open some location in map view and observe the Recent List
        TestBase.getObject("Search_Button").click();
        TestBase.getObject("POI_Options").click();
        TestBase.getObject("Emergency").click();
        TestBase.getObject("All_Emergency").click();
        Thread.sleep(2000);
        TestBase.getObject("Location_Name").click();
        TestBase.getObject("Back_Button").click();
        TestBase.getObject("Location_Name1").click();
        TestBase.getObject("Back_Button").click();
        TestBase.getObject("Location_Name2").click();

        //  Creating Loop to back from the screen
        for (int i = 0; i < 5; i++) {
            TestBase.getObject("Back_Button").click();
        }

        //Verify the Recent location should display in the Main manue
        //Scroll for Last selected Location in dashboard
        TestBase.driver.scrollTo("KEBUN BARU");
        Thread.sleep(2000);

        //Take a screen shot for Recent
        TestBase.get_Screenshot("src\\test\\resource\\RECENT_FUNCTION\\C001_Accessing_Recents_from_Main_Menu/" + "Recent_Verify_Dashboard" + ".jpg");

        TestBase.driver.scrollTo("KEBUN BARU");

    }

    @Test(priority = 2)
    public void C002_Accessing_Recent_from_Search_page_when_DataAvailable() throws Exception {

        TestBase.driver.scrollTo("Home");
        //Verify the Recent page when locations are present
        TestBase.getObject("Search_Button").click();
        TestBase.getObject("Recent_Options").click();
        Thread.sleep(2000);

        //Take a screen shot for Recent
        TestBase.get_Screenshot("src\\test\\resource\\RECENT_FUNCTION\\C002_Accessing_Recent_from_Search_page/" + "Recent_Page" + ".jpg");
        TestBase.getObject("Back_Button").click();
        TestBase.getObject("Back_Button").click();


        //Clear all the data Functionality Flow:
        TestBase.clear_data();
        TestBase.getObject("Back_Button").click();

        //Verify the Recent page where no locations are there
        TestBase.getObject("Search_Button").click();
        TestBase.getObject("Recent_Options").click();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\RECENT_FUNCTION\\C002_Accessing_Recent_from_Search_page/" + "Recent_Page_NoLocation" + ".jpg");
    }

    @Test(priority = 3)
    public void C003_Checking_the_display_of_Recent_window() throws Exception {

        //Take a screen shots
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\RECENT_FUNCTION\\C003_Checking_the_display_of_Recent_window/" + "Recent_Page_NoLocation" + ".jpg");
        TestBase.getObject("Back_Button").click();
        TestBase.getObject("Back_Button").click();

    }

    @Test(priority = 4)
    public void C004_Adding_Location_In_Recent_List() throws Exception {

        TestBase.getObject("Search_Button").click();
        TestBase.getObject("POI_Options").click();
        TestBase.getObject("Emergency").click();
        TestBase.getObject("All_Emergency").click();
        Thread.sleep(5000);
        TestBase.getObject("Location_Name1").click();
        TestBase.getObject("Back_Button").click();
        TestBase.getObject("Location_Name2").click();

        //  Creating Loop to back from the screen
        for (int i = 0; i < 4; i++) {
            TestBase.getObject("Back_Button").click();
        }

        //Click on Recent and take a screen shots
        Thread.sleep(2000);
        TestBase.getObject("Recent_Options").click();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\RECENT_FUNCTION\\C004_Adding_Location_In_Recent_List/" + "Location_list_SortedByTime" + ".jpg");
    }

    @Test(priority = 5)
    public void C005_Check_The_Sorting_Functionality() throws Exception {
        //Click on Sorting button
        TestBase.getObject("Sort_Button").click();

        //Verify the Sorting options popup
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\RECENT_FUNCTION\\C005_Check_The_Sorting_Functionality/" + "Sorting_Popup1" + ".jpg");
        Assert.assertTrue(TestBase.isElementPresent("SortBy_Text"), "C005: SortBy_Text is not present in the sorting popup");
        Assert.assertTrue(TestBase.isElementPresent("Time_Text"), "C005: Time_Text is not present in the sorting popup");
        Assert.assertTrue(TestBase.isElementPresent("Distance_Text"), "C005: Distance_Text is not present in the sorting popup");
        Assert.assertTrue(TestBase.isElementPresent("Frequency_Text"), "C005: Frequency_Text is not present in the sorting popup");

        //Change the sorting options Time to Distance
        TestBase.getObject("Distance_Text").click();
        //Check the sorting result screen(Sorting_By_Distance)
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\RECENT_FUNCTION\\C005_Check_The_Sorting_Functionality/" + "Sorted_By_Distance" + ".jpg");

        //Check the sorting result screen(Sorting_By_Frequency)
        TestBase.getObject("Sort_Button").click();
        TestBase.getObject("Frequency_Text").click();
        Thread.sleep(2000);

        TestBase.get_Screenshot("src\\test\\resource\\RECENT_FUNCTION\\C005_Check_The_Sorting_Functionality/" + "Sorted_By_Frequency" + ".jpg");

    }

    @Test(priority = 6)
    public void C006_Check_The_Delete_functionality_From_Recent() throws Exception {
        //Long Press On My Office Options
        TouchAction action = new TouchAction(TestBase.driver);
        action.longPress(TestBase.getObject("Location_Name1")).perform();
        Thread.sleep(5000);

        //Verify the Delete popup for Recent
        TestBase.get_Screenshot("src\\test\\resource\\RECENT_FUNCTION\\C006_Check_The_Delete_functionality_From_Recent/" + "Delete_Popup" + ".jpg");

        //Verify the Cancel button
        Assert.assertTrue(TestBase.isElementPresent("Cancel_Button"), "Cancel_Button is not present in the sorting popup");
        TestBase.getObject("Cancel_Button").click();

        //Long Press On My Office Options
        //TouchAction action = new TouchAction(TestBase.driver);
        action.longPress(TestBase.getObject("Location_Name1")).perform();
        Thread.sleep(5000);

        //Click on Delete button and verify the location is deleted successfully
        TestBase.getObject("Delete_Recent_Button").click();

        //Verify the Confirmation Popup
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\RECENT_FUNCTION\\C006_Check_The_Delete_functionality_From_Recent/" + "Confirmation_Popup" + ".jpg");
        //Tap on Yes to delete the record
        TestBase.getObject("Yes_Button").click();

        //Verify after the first record deleted
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\RECENT_FUNCTION\\C006_Check_The_Delete_functionality_From_Recent/" + "AfterFirst_Record_Deleted" + ".jpg");




        TouchAction action1 = new TouchAction(TestBase.driver);
        action.longPress(TestBase.getObject("Location_Name2")).perform();
        Thread.sleep(5000);
        //Verify the Delete popup for Recent
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\RECENT_FUNCTION\\C006_Check_The_Delete_functionality_From_Recent/" + "Delete_Popup" + ".jpg");

        //Click on Delete button and verify the location is deleted successfully
        TestBase.getObject("Delete_Recent_Button").click();
        //Tap on Yes to delete the record
        TestBase.getObject("Yes_Button").click();

        //Verify after the Second record deleted
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\RECENT_FUNCTION\\C006_Check_The_Delete_functionality_From_Recent/" + "AfterSecond_Record_Deleted" + ".jpg");

    }

    @Test(priority = 7)
    public void C007_Checking_The_Back_Function() throws Exception {

        //  Creating Loop to back from the screen
        for (int i = 0; i < 2; i++) {
            TestBase.getObject("Back_Button").click();
        }
        //Clear all the data Functionality Flow:
        TestBase.clear_data();
        TestBase.getObject("Back_Button").click();
    }

    @Test(priority =8 )
    public void C008_Accessing_Recent_from_Search_page_when_DataNotAvailable() throws Exception {

        //click search and open the fav page from "Search Destination" page
        TestBase.getObject("Search_Button").click();
        TestBase.getObject("Recent_Options").click();


        //Verify No Recent present text
        Assert.assertTrue(TestBase.isElementPresent("No_Recent_Text"), "No_Recent_Text  is not present");
        TestBase.getObject("Back_Button").click();
        TestBase.getObject("Back_Button").click();
    }


    @Test(priority =9 )
    public void C009_Check_The_Delete_functionality_From_Recent_when_OneRecentAvailable() throws Exception {

        //Open any POI and Open in map view
        TestBase.getObject("Search_Button").click();
        TestBase.getObject("POI_Options").click();
        TestBase.getObject("Emergency").click();
        TestBase.getObject("All_Emergency").click();
        Thread.sleep(5000);
        TestBase.getObject("Location_Name").click();
        Thread.sleep(2000);


        // Creating Loop to back from the screen
        for (int i = 0; i < 4; i++) {
            TestBase.getObject("Back_Button").click();
        }

        //Click on Recent and verify
        TestBase.getObject("Recent_Options").click();

        Assert.assertTrue(TestBase.isElementPresent("Location_Name"), "Location_Name  is not present");

        //Delete the location and verify the NO recent message
        //Long Press On My Office Options
        TouchAction action = new TouchAction(TestBase.driver);
        action.longPress(TestBase.getObject("Location_Name")).perform();
        Thread.sleep(3000);
        TestBase.getObject("Delete_Recent_Button").click();
        TestBase.getObject("Yes_Button").click();
        Thread.sleep(3000);
        Assert.assertTrue(TestBase.isElementPresent("No_Recent_Text"), "No_Recent_Text  is not present");

        // Creating Loop to back from the screen
        for (int i = 0; i < 4; i++) {
            TestBase.getObject("Back_Button").click();
        }

        //Clear all the data Functionality Flow:
        TestBase.clear_data();
        TestBase.getObject("Back_Button").click();

    }
    @AfterTest
    public void quit() throws InterruptedException {

        if (TestBase.driver != null)
            Thread.sleep(4000);
        TestBase.driver.quit();
        System.out.println("exit");
    }
}
