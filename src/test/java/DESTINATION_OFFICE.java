import io.appium.java_client.TouchAction;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

/**
 * Created by Soumik on 6/21/2016.
 */
public class DESTINATION_OFFICE {

    SoftAssert softAssert= new SoftAssert();
    @Test(priority=1)
    public void C001_Verify_The_Office_PopUp_Functionality() throws Exception {
        TestBase utill = new TestBase();
        utill.initialize();
        Thread.sleep(2000);

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }

        //Screen shot for application Dashboard
        TestBase.get_Screenshot("src\\test\\resource\\DESTINATION_OFFICE\\C001_Verify_The_Office_PopUp_Functionality/" + "Application_Dashboard" + ".jpg");


        //Verify the Office sections in DashBoard and Click(When Office is not Set)
        Assert.assertTrue(TestBase.isElementPresent("Office_Dashborad"), "Office options is not present in the Dashboard");
        TestBase.getObject("Office_Dashborad").click();
        Thread.sleep(3000);

        //Take a Screenshot For
        TestBase.get_Screenshot("src\\test\\resource\\DESTINATION_Office\\C001_Verify_The_Office_PopUp_Functionality/" + "Set_OfficePopup" + ".jpg");

    }

    @Test(priority=2)
    public void C002_Verify_the_SearchDestination_page_for_Set_Office() throws Exception {

        //Click on No Button
        // TestBase.getObject("Office_Dashboard").click();
        try {
            TestBase.getObject("No_Button").click();
            Assert.assertFalse(TestBase.isElementPresent("No_Button"), "No_Button is still present for the Set Office popup");
        }catch (Exception e){
            System.out.println("No Button Not Clicked");

        }

        TestBase.getObject("Office_Dashborad").click();

        //Click on Yes button
        TestBase.getObject("Yes_Button").click();

        //Verify the available options
        Assert.assertTrue(TestBase.isElementPresent("Current_Location"), "Current_Location options is not present in the Set Office popup");
        Assert.assertTrue(TestBase.isElementPresent("POI_Options"), "C001: POI_Options is not present in the Set Office popup");
        Assert.assertTrue(TestBase.isElementPresent("Recent_Options"), "C001: Recent_Options is not present in the Set Office popup");
        Assert.assertTrue(TestBase.isElementPresent("Favourite_Options"), "C001: Favourite_Options is not present in the Set Office popup");
        TestBase.driver.navigate().back();
        Assert.assertTrue(TestBase.isElementPresent("Co-Ordinate_Options"), "C001: Co-Ordinate_Options is not present in the Set Office popup");
        TestBase.getObject("Back_Button").click();



    }


    @Test(priority=3)
    public void C003_Verify_the_Office_PopUp_when_Office_is_not_Set() throws Exception {

        TestBase.getObject("Office_Dashborad").click();


        //Verify the Office popup text
        Assert.assertTrue(TestBase.isElementPresent("Office_Text"), "C001: Office text is not present in the Set Office popup");
        Assert.assertTrue(TestBase.isElementPresent("SetNow_Text_Office"), "C001: SetNow_Text is not present in the Set Office popup");
        Assert.assertTrue(TestBase.isElementPresent("No_Button"), "C001: No Button is not present in the Set Office popup");
        Assert.assertTrue(TestBase.isElementPresent("Yes_Button"), "C001: Yes Button is not present in the Set Office popup");

        TestBase.getObject("No_Button").click();
        Thread.sleep(2000);

        //Open any Location in Map View and Select Favourite for Recent & Favourite Page
        TestBase.getObject("Search_Button").click();
        TestBase.getObject("POI_Options").click();
        TestBase.getObject("Emergency").click();
        TestBase.getObject("All_Emergency").click();
        Thread.sleep(5000);
        TestBase.getObject("Location_Name").click();
        Thread.sleep(2000);
        //Select favourite
        TestBase.getObject("Favourite_Star_Button").click();
        TestBase.getObject("Ok_Button").click();
        Thread.sleep(2000);
        //Click on Back to dashboard
        TestBase.getObject("Back_Button").click();
        TestBase.getObject("Back_Button").click();
        TestBase.getObject("Back_Button").click();
        TestBase.getObject("Back_Button").click();
        TestBase.getObject("Back_Button").click();

    }

    @Test(priority=4)
    public void C004_Select_Office_From_Recent_Using_SaveOnly_Button() throws Exception {

        //Verify the Office sections in DashBoard and Click(When Office is not Set)
        Assert.assertTrue(TestBase.isElementPresent("Office_Dashborad"), "C002: Office options is not present in the Dashboard");
        TestBase.getObject("Office_Dashborad").click();

        //Click on Yes Button from Set Office popup
        Assert.assertTrue(TestBase.isElementPresent("Yes_Button"), "C002: Yes Button is not present in the Set Office popup");
        TestBase.getObject("Yes_Button").click();

        //Select any location from Recent List (When some locations are present)
        TestBase.driver.scrollTo("Recent");
        Assert.assertTrue(TestBase.isElementPresent("Recent_Options"), "C002: Recent is not present in the Search Destination page");
        TestBase.getObject("Recent_Options").click();

        //Verify the added location should present and set as Office
        Assert.assertTrue(TestBase.isElementPresent("Location_Name"), "C002: Location_Name is not present in the Recent List");
        TestBase.getObject("Location_Name").click();

        //Tap On Save Only Options
        Assert.assertTrue(TestBase.isElementPresent("Save_Only_Button"), "C002: Save_Only_Button is not present ");
        TestBase.getObject("Save_Only_Button").click();
        Thread.sleep(2000);

        //Screen shots for Set Office successfully
        TestBase.get_Screenshot("src\\test\\resource\\DESTINATION_OFFICE\\C004_Select_Office_From_Recent_Using_SaveOnly_Button/" + "Set_Office_Successfully" + ".jpg");

        //Verification Office is set or not
        TestBase.getObject("Search_Button").click();
        TestBase.driver.scrollTo("My Office");
        Assert.assertTrue(TestBase.isElementPresent("Location_Name"), "Location_Name is not present in the Search Destination page");
        TestBase.getObject("Back_Button").click();


        //Clear all the data Functionality Flow:
        TestBase.clear_data();

        TestBase.getObject("Back_Button").click();

    }

    @Test(priority=5)
    public void C005_Select_Office_From_Recent_Using_SaveAndNavigate_Button() throws Exception {



        //Extra Flow//Open any Location in Map View and Select Favourite for Recent & Favourite Page
        TestBase.getObject("Search_Button").click();
        TestBase.getObject("POI_Options").click();
        TestBase.getObject("Emergency").click();
        TestBase.getObject("All_Emergency").click();
        Thread.sleep(5000);
        TestBase.getObject("Location_Name").click();

        //  Creating Loop to back from the screen
        for (int i = 0; i < 5; i++) {
            TestBase.getObject("Back_Button").click();
        }

        //Verify the Office sections in DashBoard and Click(When Office is not Set)
        Assert.assertTrue(TestBase.isElementPresent("Office_Dashborad"), "C002: Office options is not present in the Dashboard");
        TestBase.getObject("Office_Dashborad").click();

        //Click on Yes Button from Set Office popup
        Assert.assertTrue(TestBase.isElementPresent("Yes_Button"), "C002: Yes Button is not present in the Set Office popup");
        TestBase.getObject("Yes_Button").click();

        //Select any location from Recent List (When some locations are present)
        Assert.assertTrue(TestBase.isElementPresent("Recent_Options"), "C002: Recent is not present in the Search Destination page");
        TestBase.getObject("Recent_Options").click();

        //Verify the added location should present and set as Office
        Assert.assertTrue(TestBase.isElementPresent("Location_Name"), "C002: Location_Name is not present in the Recent List");
        TestBase.getObject("Location_Name").click();

        //Tap on "SAVE AND NAVIGATE" button
        TestBase.getObject("Save_And_Navigate").click();
        Thread.sleep(2000);


        //Take a screen shots where we are done "SAVE AND NAVIGATE" for setting the Office
        TestBase.get_Screenshot("src\\test\\resource\\DESTINATION_OFFICE\\C005_Select_Office_From_Recent_Using_SaveAndNavigate_Button/" + "Save&Navigate_Office_Setting" + ".jpg");

        //Click on Back From Navigation screen
        TestBase.getObject("NavigationScreen_Back").click();

        //Verification Office is set or not
        TestBase.getObject("Search_Button").click();
        TestBase.driver.scrollTo("My Office");
        Assert.assertTrue(TestBase.isElementPresent("Location_Name"), "Location_Name is not present in the Search Destination page");
        TestBase.getObject("Back_Button").click();

        //Clear all the data Functionality Flow:
        TestBase.clear_data();

        TestBase.getObject("Back_Button").click();

    }



    @Test(priority=6)
    public void C006_Select_Office_From_POI_Using_SaveAndNavigate_Button() throws Exception {

        //Verify the Office sections in DashBoard and Click(When Office is not Set)
        Assert.assertTrue(TestBase.isElementPresent("Office_Dashborad"), "C003: Office options is not present in the Dashboard");
        TestBase.getObject("Office_Dashborad").click();

        //Click on Yes Button from Set Office popup
        Assert.assertTrue(TestBase.isElementPresent("Yes_Button"), "C003: Yes Button is not present in the Set Office popup");
        TestBase.getObject("Yes_Button").click();


        //Open the POI Locations list
        TestBase.getObject("POI_Options").click();
        TestBase.getObject("Emergency").click();
        TestBase.getObject("All_Emergency").click();
        Thread.sleep(5000);
        TestBase.getObject("Location_Name").click();
        Thread.sleep(2000);

        //Verify the "Set Office" popup verification
        Assert.assertTrue(TestBase.isElementPresent("Office_Text"), "C003: Office_Text is not present in the Set Office popup");
        Assert.assertTrue(TestBase.isElementPresent("Save_Location_Text_Office"), "C003: Save_Location_Text is not present in the Set Office popup");
        Assert.assertTrue(TestBase.isElementPresent("Save_And_Navigate"), "C003: Save_And_Navigate button is not present in the Set Office popup");
        Assert.assertTrue(TestBase.isElementPresent("Save_Only_Button"), "C003: Save_Only_Button is not present ");
        Assert.assertTrue(TestBase.isElementPresent("Cancel_Button"), "C003: Cancel_Button is not present ");

        //Tap on "SAVE AND NAVIGATE" button
        TestBase.getObject("Save_And_Navigate").click();
        Thread.sleep(2000);

        //Take a screen shots where we are done "SAVE AND NAVIGATE" for setting the Office
        TestBase.get_Screenshot("src\\test\\resource\\DESTINATION_OFFICE\\C006_Select_Office_From_POI_Using_SaveAndNavigate_Button/" + "Save&Navigate_Office_Setting" + ".jpg");

        //Click on Back From Navigation screen
        TestBase.getObject("NavigationScreen_Back").click();

        //Verification Office is set or not
        TestBase.getObject("Search_Button").click();
        TestBase.driver.scrollTo("My Office");
        Assert.assertTrue(TestBase.isElementPresent("Location_Name"), "Location_Name is not present in the Search Destination page");
        TestBase.getObject("Back_Button").click();

        //Clear all the data Functionality Flow:
        TestBase.clear_data();
        TestBase.getObject("Back_Button").click();


    }

    @Test(priority=7)
    public void C007_Select_Office_From_Favourite_Using_Save_Only_Button() throws Exception {

        //Open any Location in Map View and Select Favourite for Recent & Favourite Page
        TestBase.getObject("Search_Button").click();
        TestBase.getObject("POI_Options").click();
        TestBase.getObject("Emergency").click();
        TestBase.getObject("All_Emergency").click();
        Thread.sleep(5000);
        TestBase.getObject("Location_Name").click();
        Thread.sleep(2000);
        //Select favourite
        TestBase.getObject("Favourite_Star_Button").click();
        TestBase.getObject("Ok_Button").click();
        Thread.sleep(2000);
        //Click on Back to dashboard
        TestBase.getObject("Back_Button").click();
        TestBase.getObject("Back_Button").click();
        TestBase.getObject("Back_Button").click();
        TestBase.getObject("Back_Button").click();
        TestBase.getObject("Back_Button").click();

        //Verify the Office sections in DashBoard and Click(When Office is not Set)
        Assert.assertTrue(TestBase.isElementPresent("Office_Dashborad"), "C004: Office options is not present in the Dashboard");
        TestBase.getObject("Office_Dashborad").click();

        //Click on Yes Button from Set Office popup
        Assert.assertTrue(TestBase.isElementPresent("Yes_Button"), "C004: Yes Button is not present in the Set Office popup");
        TestBase.getObject("Yes_Button").click();

        TestBase.driver.scrollTo("Favourites");

        //Click on "Favourite" Options
        TestBase.getObject("Favourite_Options").click();

        //Select the Location Name
        TestBase.getObject("Location_Name").click();

        //Tap On Save Only Options
        TestBase.getObject("Save_Only_Button").click();
        Thread.sleep(2000);

        //Take a screen shots where we are done "SAVE AND NAVIGATE" for setting the Office
        TestBase.get_Screenshot("src\\test\\resource\\DESTINATION_OFFICE\\C007_Select_Office_From_Favourite_Using_Save_Only_Button/" + "SaveOnly_Favourite" + ".jpg");

        //Verification Office is set or not
        TestBase.getObject("Search_Button").click();
        TestBase.driver.scrollTo("My Office");
        Assert.assertTrue(TestBase.isElementPresent("Location_Name"), "Location_Name is not present in the Search Destination page");
        TestBase.getObject("Back_Button").click();

        //Clear all the data Functionality Flow:
        TestBase.clear_data();

        TestBase.getObject("Back_Button").click();

    }

    @Test(priority=8)
    public void C008_Select_Office_From_CoOrdinate_Using_Save_Only_Button() throws Exception {



        //Verify the Office sections in DashBoard and Click(When Office is not Set)
        Assert.assertTrue(TestBase.isElementPresent("Office_Dashborad"), "C005: Office options is not present in the Dashboard");
        TestBase.getObject("Office_Dashborad").click();

        //Click on Yes Button from Set Office popup
        Assert.assertTrue(TestBase.isElementPresent("Yes_Button"), "C005: Yes Button is not present in the Set Office popup");
        TestBase.getObject("Yes_Button").click();

        TestBase.driver.scrollTo("Coordinates");

        //Click on "Co-Ordinate" options
        TestBase.getObject("Co-Ordinate_Options").click();

        //Enter Latitude and Logitude for search Location
        TestBase.getObject("Latitude_TextBox").sendKeys(TestBase.TestData.getProperty("Latitude"));
        Thread.sleep(2000);
        TestBase.getObject("Logitide_TextBox").sendKeys(TestBase.TestData.getProperty("Logitude"));

        //Tap on Ok button
        TestBase.getObject("Ok_Button").click();

        //Tap on 'Select Location' button
        TestBase.getObject("Select_Location_Button").click();

        //Tap On Save Only Options.
        TestBase.getObject("Save_Only_Button").click();
        Thread.sleep(2000);

        //Take a screen shots where we are done "SAVE Only" for setting the Office
        TestBase.get_Screenshot("src\\test\\resource\\DESTINATION_OFFICE\\C008_Select_Office_From_CoOrdinate_Using_Save_Only_Button/" + "SaveOnly_CoOrdinate" + ".jpg");


        //Verification Office is set or not
        TestBase.getObject("Search_Button").click();
        TestBase.driver.scrollTo("My Office");
        softAssert.assertTrue(TestBase.isElementPresent("Office_Current_Coordinate1"), "Office_Current_Coordinate is not present in the Search Destination page");
        Thread.sleep(1000);
        TestBase.getObject("Back_Button").click();
        softAssert.assertAll();
    }

    @Test(priority=9)
    public void C009_Verify_The_Change_Office_Popup_verification() throws Exception {

        //Search Button
        TestBase.getObject("Search_Button").click();
        TestBase.driver.scrollTo("My Office");

        //Long Press On My Office Options
        TouchAction action = new TouchAction(TestBase.driver);
        action.longPress(TestBase.getObject("My_Office")).perform();
        Thread.sleep(5000);

        //Verification about Change the Office
        TestBase.get_Screenshot("src\\test\\resource\\DESTINATION_OFFICE\\C009_Verify_The_Change_Office_Popup_verification/" + "Change_Office_Popup" + ".jpg");

        //Verify the change 'Office' options
        Assert.assertTrue(TestBase.isElementPresent("Office_Text"), "C007:Office_Text is not present in the change Office popup");
        Assert.assertTrue(TestBase.isElementPresent("Change_Office_Address"), "C007:Change_Office_Address text is not present in the change Office popup");
        Assert.assertTrue(TestBase.isElementPresent("Clear_Button"), "C007:Clear_Button is not present in the change Office popup");
        Assert.assertTrue(TestBase.isElementPresent("Yes_Button"), "C007:Yes_Button is not present in the change Office popup");
        Assert.assertTrue(TestBase.isElementPresent("No_Button"), "C007:No_Button is not present in the change Office popup");

    }

    @Test(priority=10)
    public void C010_Verify_The_Change_Office_Functionality_using_Clear_Button() throws Exception {

        //Click on  Clear button and observe
        TestBase.getObject("Clear_Button").click();

        //Verify the after clear the Office data
        TestBase.driver.scrollTo("My Office");
        Thread.sleep(2000);
        //Assert.assertFalse(TestBase.isElementPresent("Office_Current_Coordinate1"), "Location still exist");

        //Verification about clear the Office
        TestBase.get_Screenshot("src\\test\\resource\\DESTINATION_OFFICE\\C010_Verify_The_Change_Office_Functionality_using_Clear_Button/" + "Clear_Office" + ".jpg");

        TestBase.getObject("Back_Button").click();

    }

    @Test(priority=11)
    public void C011_Verify_No_Button_Functionality_In_ChangeLocation_Popup() throws Exception {

        //Click on search button
        TestBase.getObject("Search_Button").click();
        TestBase.driver.scrollTo("My Office");

        //Click on My Office button
        TestBase.getObject("My_Office").click();
        TestBase.getObject("Yes_Button").click();
        TestBase.getObject("Current_Location").click();
        TestBase.getObject("Save_Only_Button").click();

        TestBase.driver.scrollTo("My Office");
        Thread.sleep(2000);

        //Long press on the Office options when Office is set
        TouchAction action = new TouchAction(TestBase.driver);
        action.longPress(TestBase.getObject("My_Office")).perform();
        Thread.sleep(5000);

        //Click on No button and verify the Popup is closed
        TestBase.getObject("No_Button").click();

        //Verify the Popup is not present
        Assert.assertFalse(TestBase.isElementPresent("No_Button"), "No_Button popup is present in the screen");


        //Tap on Cancel Button and Back button
        //TestBase.getObject("Cancel_Button").click();
        TestBase.getObject("Back_Button").click();

    }

    @Test(priority=12)
    public void C012_Verify_and_Click_On_Yes_Button_From_Change_Office_Popup() throws Exception {

        //Go to Office button
        TestBase.getObject("Search_Button").click();
        TestBase.driver.scrollTo("My Office");

        //Long press on the Office options when Office is set
        TouchAction action = new TouchAction(TestBase.driver);
        action.longPress(TestBase.getObject("My_Office")).perform();
        Thread.sleep(5000);

        //Verify and Click on Yes button
        Assert.assertTrue(TestBase.isElementPresent("Yes_Button"), "Yes Button is not present in the Change Office popup");
        TestBase.getObject("Yes_Button").click();

    }

    @Test(priority=13)
    public void C013_Verify_the_SendDestination_Page_Using_Yes_button_to_Change_Office() throws Exception {

        TestBase.get_Screenshot("src\\test\\resource\\DESTINATION_OFFICE\\C013_Verify_the_SendDestination_Page_Using_Yes_button_to_Change_Office/" + "Change_Office-Destination_Page" + ".jpg");

    }

    @Test(priority=14)
    public void C014_Change_the_Office_Using_Current_Location() throws Exception {
        //Select Current Location to set the Office
        Assert.assertTrue(TestBase.isElementPresent("Current_Location"), "Current_Location option is not present in the Search Destination page");
        TestBase.getObject("Current_Location").click();

        //Select Save Only button to select the Office
        TestBase.getObject("Save_Only_Button").click();
        Thread.sleep(2000);

        //Verify Office is change successfully
        TestBase.get_Screenshot("src\\test\\resource\\DESTINATION_OFFICE\\C014_Change_the_Office_Using_Current_Location/" + "Office-Set_Successfully_CurrentLocation_Using_Change_Function" + ".jpg");


    }

    @Test(priority=15)
    public void C015_Verify_Office_Changed_Successfully_using_CurrentLocation() throws Exception {

        //Verify the Office is set using Current Location
        TestBase.driver.scrollTo("My Office");
        softAssert.assertTrue(TestBase.isElementPresent("cURRENT_lOCATION"), "Using_Current_Coordinate is not set as Office");
        TestBase.driver.scrollTo("My Office");
        softAssert.assertAll();

        //if(TestBase.getObject("my_Office_not_set_text_ver").getText().)

    }


    @Test(priority=16)
    public void C016_Clear_Data_FunctionalityChecking_For_Office() throws Exception {

        //Click on Back button
        TestBase.getObject("Back_Button").click();

        //Clear all the data Functionality Flow:
        TestBase.clear_data();
        TestBase.getObject("Back_Button").click();


    }

    @Test(priority=17)
    public void C017_Checking_the_CANCEL_button_in_Set_Office_Popup() throws Exception {
        //Search Button
        TestBase.getObject("Search_Button").click();
        TestBase.driver.scrollTo("My Office");

        TestBase.getObject("My_Office").click();

        TestBase.getObject("Yes_Button").click();

        //Click on Current_Location option
        TestBase.getObject("Current_Location").click();

        //Verify the Cancel button from Office popup
        Assert.assertTrue(TestBase.isElementPresent("Cancel_Button"), "Cancel_Button is not set as Office popup");
        TestBase.getObject("Cancel_Button").click();

        //Verify the Office Popup should be closed
        Assert.assertFalse(TestBase.isElementPresent("Save_Location_Text"), "Cancel_Button is not set as Office popup");

        //Click back to the Dashboard
        TestBase.getObject("Back_Button").click();
        TestBase.getObject("Back_Button").click();

    }

    @Test(priority=18)
    public void C018_Verify_the_MapView_page_after_Setting_Office() throws Exception {
        TestBase.getObject("Search_Button").click();

        TestBase.driver.scrollTo("My Office");
        Thread.sleep(2000);
        TestBase.getObject("My_Office").click();

        TestBase.getObject("Yes_Button").click();
        TestBase.getObject("POI_Along_the_Route").click();
        TestBase.getObject("Suggested_Image").click();
        TestBase.getObject("Save_Only_Button").click();
        Thread.sleep(2000);

        //Verify the Office is set or not and open in map view
        TestBase.driver.scrollTo("My Office");
        TestBase.getObject("My_Office").click();

        Assert.assertTrue(TestBase.isElementPresent("Map_View_Text"), "Map_View_Text is not present");

        TestBase.get_Screenshot("src\\test\\resource\\DESTINATION_OFFICE\\C018_Verify_the_MapView_page_after_Setting_Office/" + "Map_View_Office" + ".jpg");
        TestBase.getObject("Back_Button").click();
        TestBase.getObject("Back_Button").click();

        //Clear all the data Functionality Flow:
        TestBase.clear_data();
        TestBase.getObject("Back_Button").click();



    }



    @AfterTest
    public void quit() throws InterruptedException {

        if (TestBase.driver != null)
            Thread.sleep(4000);
        TestBase.driver.quit();
        System.out.println("exit");
    }


}

