import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

/**
 * Created by QI-37 on 01-06-2016.
 */
public class Refferal_Feature {

    @Test(priority = 1)
            public void C001_Verify_Share_App_btn_in_About_Page() throws Exception {
    TestBase utill = new TestBase();
        try {
            utill.initialize();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Assert.assertTrue(TestBase.isElementPresent("Favourites"), "C001: Favourites_text is not present in the Set Home popup");
        TestBase.getObject("Option_Button").click();
        Thread.sleep(2000);
        TestBase.driver.scrollTo("About");
        TestBase.getObject("about").click();
        TestBase.get_Screenshot("src\\test\\resource\\Referral_Feature\\C001_Verify_Share_App_btn_in_About_Page/" + "screen1 " + ".jpg");

        Assert.assertTrue(TestBase.isElementPresent("about_page"), "about page label is not there");
        TestBase.driver.scrollTo("Share App");
        Assert.assertTrue(TestBase.isElementPresent("share_app"), "share app button is not there");
    }

    @Test(priority = 2)
    public void C002_Verify_the_ReferralLink_in_Share_App_Page_When_Not_Login_into_MapSYNQ() throws Exception {

        TestBase.getObject("share_app").click();
        Assert.assertTrue(TestBase.isElementPresent("share_app_referral_msg"), "share_app_referral_msg label is not there");
        Assert.assertTrue(TestBase.isElementPresent("r_msg1"), "Like the Galactio experience? label is not there");
        Assert.assertTrue(TestBase.isElementPresent("r_msg2"), "Tell your friends about it. label is not there");

        Assert.assertTrue(TestBase.isElementPresent("facebook"), "facebook label is not there");
        Assert.assertTrue(TestBase.isElementPresent("r_email"), "r_email label is not there");
        Assert.assertTrue(TestBase.isElementPresent("copy_link"), "copy_link label is not there");
        Assert.assertTrue(TestBase.isElementPresent("more"), "more label is not there");

        TestBase.get_Screenshot("src\\test\\resource\\Referral_Feature\\C002_Verify_the_ReferralLink_in_Share_App_Page_When_Not_Login_into_MapSYNQ/" + "screen1 " + ".jpg");

    }

    @Test(priority = 3)
    public void C003_Verify_the_ShareApp_Page_When_User_Logged_in_into_MapSYNQ() throws Exception {

        TestBase.getObject("share_app_referral_msg").click();
        TestBase.login_function();
        TestBase.getObject("back_btn").click();
        //TestBase.driver.navigate().back();
        TestBase.get_Screenshot("src\\test\\resource\\Referral_Feature\\C003_Verify_the_dissapearance_of_ReferralLink_in_ShareApp_Page_When_User_Loged_in_into_MapSYNQ_using_ ReferralLink/" + "screen1" + ".jpg");

        /*TestBase.getObject("Option_Button").click();
        Thread.sleep(2000);
        TestBase.driver.scrollTo("About");
        TestBase.getObject("about").click();
        TestBase.get_Screenshot("src\\test\\resource\\Referral_Feature\\C003_Verify_the_ShareApp_Page_When_User_Logged_in_into_MapSYNQ/" + "screen2" + ".jpg");

        Assert.assertTrue(TestBase.isElementPresent("about_page"), "about page label is not there");
        TestBase.driver.scrollTo("Share App");
        TestBase.getObject("share_app").click();
*/
        Assert.assertTrue(TestBase.isElementPresent("r_msg1"), "Like the Galactio experience? label is not there");
        Assert.assertTrue(TestBase.isElementPresent("r_msg2"), "Tell your friends about it. label is not there");

        Assert.assertFalse(TestBase.isElementPresent("share_app_referral_msg"), "share app button is not there");
        TestBase.get_Screenshot("src\\test\\resource\\Referral_Feature\\C003_Verify_the_dissapearance_of_ReferralLink_in_ShareApp_Page_When_User_Loged_in_into_MapSYNQ_using_ ReferralLink/" + "screen2" + ".jpg");

    }

    @Test(priority = 4)
    public void C004_Logout_verify_presence_of_ReferralLink_ShareApp_Page() throws Exception {

        System.out.println("before");
        TestBase.getObject("back_btn").click();
        System.out.println("after");
        TestBase.getObject("back_btn").click();
        TestBase.getObject("Option_Button").click();
        System.out.println("after");
        TestBase.driver.scrollTo("Account");
        TestBase.logout_function();
        TestBase.getObject("back_btn").click();

        TestBase.getObject("Option_Button").click();
        Thread.sleep(2000);

        TestBase.driver.scrollTo("About");
        TestBase.getObject("about").click();
        Assert.assertTrue(TestBase.isElementPresent("about_page"), "about page label is not there");

        TestBase.driver.scrollTo("Share App");
        TestBase.getObject("share_app").click();
        Assert.assertTrue(TestBase.isElementPresent("share_app_referral_msg"), "share_app_referral_msg label is not there");

        TestBase.get_Screenshot("src\\test\\resource\\Referral_Feature\\C004_Logout_verify_presence_of_ReferralLink_ShareApp_Page/" + "screen1" + ".jpg");

    }

    @Test(priority = 5)
    public void C005_LogIn_into_MapSYNQ_for_Referral_Account_screen_and_verify_Share_App_Page() throws Exception {

        TestBase.getObject("back_btn").click();
        TestBase.getObject("back_btn").click();
        TestBase.getObject("Option_Button").click();
        TestBase.driver.scrollTo("Account");
        TestBase.login_function();

        TestBase.getObject("back_btn").click();
        //TestBase.driver.navigate().back();
        TestBase.get_Screenshot("src\\test\\resource\\Referral_Feature\\C005_LogIn_into_MapSYNQ_for_Referral_Account_screen_and_verify_Share_App_Page/" + "screen1" + ".jpg");

        TestBase.getObject("Option_Button").click();
        Thread.sleep(2000);
        TestBase.driver.scrollTo("About");
        TestBase.getObject("about").click();
        TestBase.get_Screenshot("src\\test\\resource\\Referral_Feature\\C005_LogIn_into_MapSYNQ_for_Referral_Account_screen_and_verify_Share_App_Page/" + "screen2" + ".jpg");

        Assert.assertTrue(TestBase.isElementPresent("about_page"), "about page label is not there");
        TestBase.driver.scrollTo("Share App");
        Assert.assertTrue(TestBase.isElementPresent("share_app"), "share app button is not there");


        TestBase.getObject("share_app").click();
        Assert.assertFalse(TestBase.isElementPresent("share_app_referral_msg"), "share_app_referral_msg label is not there");
        Assert.assertTrue(TestBase.isElementPresent("r_msg1"), "Like the Galactio experience? label is not there");
        Assert.assertTrue(TestBase.isElementPresent("r_msg2"), "Tell your friends about it. label is not there");

        Assert.assertTrue(TestBase.isElementPresent("facebook"), "facebook label is not there");
        Assert.assertTrue(TestBase.isElementPresent("r_email"), "r_email label is not there");
        Assert.assertTrue(TestBase.isElementPresent("copy_link"), "copy_link label is not there");
        Assert.assertTrue(TestBase.isElementPresent("more"), "more label is not there");

        TestBase.get_Screenshot("src\\test\\resource\\Referral_Feature\\C005_LogIn_into_MapSYNQ_for_Referral_Account_screen_and_verify_Share_App_Page/" + "screen3" + ".jpg");


    }

    @Test(priority = 6)
    public void C006_Verify_the_Back_button_Function() throws Exception {

        TestBase.getObject("back_btn").click();
        Assert.assertTrue(TestBase.isElementPresent("share_app"), "share app button is not there");
        TestBase.get_Screenshot("src\\test\\resource\\Referral_Feature\\C006_Verify_the_Back_button_Function/" + "screen1" + ".jpg");

    }

    @Test(priority = 7)
    public void C007_Verify_the_Share_App_Page_UI() throws Exception {

        TestBase.getObject("share_app").click();
        Assert.assertFalse(TestBase.isElementPresent("share_app_referral_msg"), "share_app_referral_msg label is not there");
        Assert.assertTrue(TestBase.isElementPresent("r_msg1"), "Like the Galactio experience? label is not there");
        Assert.assertTrue(TestBase.isElementPresent("r_msg2"), "Tell your friends about it. label is not there");

        Assert.assertTrue(TestBase.isElementPresent("facebook"), "facebook label is not there");
        Assert.assertTrue(TestBase.isElementPresent("r_email"), "r_email label is not there");
        Assert.assertTrue(TestBase.isElementPresent("copy_link"), "copy_link label is not there");
        Assert.assertTrue(TestBase.isElementPresent("more"), "more label is not there");

        TestBase.get_Screenshot("src\\test\\resource\\Referral_Feature\\C007_Verify_the_Share_App_Page_UI/" + "screen1 " + ".jpg");

    }

    @Test(priority = 8)
    public void C008_Checking_The_Email_option_for_Referral_Feature() throws InterruptedException {

        Assert.assertTrue(TestBase.isElementPresent("r_email"), "r_email label is not there");

    }
        @Test(priority = 9)
        public void C009_Verify_the_Email_Content_for_referral() throws Exception {


        TestBase.getObject("r_email").click();
        try{
            TestBase.getObject("r_gmail").click();
            }catch(Exception e){

            Thread.sleep(3000);
            TestBase.driver.navigate().back();

            if(TestBase.getObject("mail_body").getText().contains("Hey,\n" +
                    "\n" +
                    "I'm driving with Galactio GPS Navigation App everyday.\n" +
                    "You can download it for free from the Android play store http://ACCOUNT.mapsynq.com/rf/720226KTFXL \n" +
                    "Start avoiding traffic, ERP Gantries and experience smoother drives while saving money.\n" +
                    "\n" +
                    "Cheers!")){
                System.out.println("passed");
            }else{
                Assert.assertTrue(1>2, "mail body shows wrong content");
            }

            if(TestBase.getObject("mail_subject").getText().contains("Galactio Android Navigation App!")){
                System.out.println("passed");
            }else{
                Assert.assertTrue(1>2, "mail subject shows wrong content");
            }

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }

            TestBase.get_Screenshot("src\\test\\resource\\Referral_Feature\\C009_Verify_the_Email_Content_for_referral/" + "screen1" + ".jpg");

    }

        }

    @Test(priority = 10)
    public void C010_Click_Send_and_Verify_link_Send_Successfully() throws Exception {

        TestBase.getObject("to_mail").sendKeys("angshuman.ray@quantuminventions.com");
        TestBase.getObject("email_send").click();

        TestBase.get_Screenshot("src\\test\\resource\\Referral_Feature\\C010_Click_Send_and_Verify_link_Send_Successfully/" + "screen1" + ".jpg");
        TestBase.get_Screenshot("src\\test\\resource\\Referral_Feature\\C010_Click_Send_and_Verify_link_Send_Successfully/" + "screen2" + ".jpg");
        TestBase.get_Screenshot("src\\test\\resource\\Referral_Feature\\C010_Click_Send_and_Verify_link_Send_Successfully/" + "screen3" + ".jpg");
        TestBase.get_Screenshot("src\\test\\resource\\Referral_Feature\\C010_Click_Send_and_Verify_link_Send_Successfully/" + "screen4" + ".jpg");

    }

    @Test(priority = 11)
    public void C011_Verify_the_facebook_link_navigates_to_facebook_login_page() throws Exception {

        TestBase.getObject("facebook").click();
        Thread.sleep(3000);
        TestBase.get_Screenshot("src\\test\\resource\\Referral_Feature\\C011_Verify_the_facebook_link_navigates_to_facebook_login_page/" + "screen1" + ".jpg");
        TestBase.getObject("POST_Button_For_Facebook").click();
        Thread.sleep(3000);
    }

    @Test(priority = 12)
    public void C012_Verify_the_copy_link_feature() throws Exception {

        TestBase.getObject("copy_link").click();
       // Thread.sleep(1000);
        TestBase.get_Screenshot("src\\test\\resource\\Referral_Feature\\C012_Verify_the_copy_link_feature/" + "screen1" + ".jpg");
        Thread.sleep(1000);
        TestBase.get_Screenshot("src\\test\\resource\\Referral_Feature\\C012_Verify_the_copy_link_feature/" + "screen2" + ".jpg");

    }

    @Test(priority = 13)
    public void C013_Verify_the_More_feature() throws Exception {

        TestBase.getObject("more").click();
        //Assert.assertTrue(TestBase.isElementPresent("share_galactio"), "share galactio label is not there");
        TestBase.get_Screenshot("src\\test\\resource\\Referral_Feature\\C013_Verify_the_More_feature/" + "screen1" + ".jpg");
    }

    @AfterTest
    public void quit() throws Exception {
        System.out.println("inside after test");
        TestBase.driver.navigate().back();
        TestBase.driver.navigate().back();
        TestBase.driver.navigate().back();
        TestBase.getObject("Option_Button").click();
        TestBase.driver.scrollTo("Account");
        TestBase.logout_function();
        TestBase.getObject("back_btn").click();
        Thread.sleep(3000);
        TestBase.driver.quit();


    }
}