import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

/**
 * Created by Soumik on 5/17/2016.
 */
public class PARKING_OPTION {

    @Test(priority = 1)
    public void C001_Verify_the_Parking_Page_from_Option_Section() throws Exception {

        TestBase utill = new TestBase();
        utill.initialize();
        Thread.sleep(2000);

        // Click on Option Button
        TestBase.getObject("Option_Button").click();

        //Scroll to Parking Option
        TestBase.driver.scrollTo("Parking");
    }

    @Test(priority = 2)
    public void C002_Verify_the_Parking_UI() throws Exception {

        TestBase.getObject("Verify_Parking_Button").click();
        Thread.sleep(5000);

        //Take a screen shot for Parking Page
        TestBase.get_Screenshot("src\\test\\resource\\ABOUT_PAGE\\C002_Verify_the_Parking_UI/" + "Parking_Screen" + ".jpg");
    }

    @Test(priority = 3)
    public void C003_Checking_the_Search_Functionality() throws Exception {
        Assert.assertTrue(TestBase.isElementPresent("Search_Button_Parking"), "C003:Search Button is not present in Parking screen");
        TestBase.getObject("Search_Button_Parking").click();

        //Type a Parking locaton name and search the data
        TestBase.getObject("Search_Text").sendKeys(TestBase.TestData.getProperty("Location_Parking"));

        //Click on Search Button for result list
        TestBase.getObject("Search_Button_Parking").click();

        //Take a screen shots with all the result list
        TestBase.get_Screenshot("src\\test\\resource\\PARKING_OPTION\\C003_Checking_the_Search_Functionality/" + "Search_Result" + ".jpg");

       }

    @Test(priority = 8)
    public void C004_Verify_the_Refresh_Functionality() throws Exception {
        // Click on Option Button
        TestBase.getObject("Option_Button").click();

        //Scroll to Parking button
        TestBase.driver.scrollTo("Parking");

        TestBase.getObject("Verify_Parking_Button").click();


        //Verify and click on Refresh button
        Assert.assertTrue(TestBase.isElementPresent("Refresh_Button_Parking"), "C004:Refresh_Button_Parking is not present in Parking screen");
        TestBase.getObject("Refresh_Button_Parking").click();
        TestBase.get_Screenshot("src\\test\\resource\\PARKING_OPTION\\C004_Verify_the_Refresh_Functionality/" + "Loading_For_refresh" + ".jpg");
        Thread.sleep(5000);
        TestBase.get_Screenshot("src\\test\\resource\\PARKING_OPTION\\C004_Verify_the_Refresh_Functionality/" + "After_Refresh_List" + ".jpg");
    }

    @Test(priority = 4)
    public void C005_Verify_the_iPark_Details() throws Exception {
        //Verify the Parking details (i) details button
        TestBase.getObject("Parking_Rates_Button").click();
        Thread.sleep(5000);

        //Take a screen shots for iPark details screen
        TestBase.get_Screenshot("src\\test\\resource\\PARKING_OPTION\\C005_Verify_the_iPark_Details/" + "iPark_Rates_Screen" + ".jpg");
    }

    @Test(priority = 5)
    public void C006_Checking_the_Show_On_Map_functionality() throws Exception {

        //Verify the 'Show On Map' button
        Assert.assertTrue(TestBase.isElementPresent("Show_On_Map"), "C006:Show_On_Map button is not present in Parking rate (i) screen");
        TestBase.getObject("Show_On_Map").click();

        //Take a screen shots for Show On Map details screen
        TestBase.get_Screenshot("src\\test\\resource\\PARKING_OPTION\\C006_Checking_the_Show_On_Map_functionality/" + "Map_View" + ".jpg");
        TestBase.getObject("Back_Button").click();
    }

    @Test(priority = 6)
    public void C007_Checking_the_Close_button_functionality() throws Exception {

        Assert.assertTrue(TestBase.isElementPresent("Close_Button_Parking"), "C007:Close_Button_ button is not present in Parking rate (i) screen");
        //Before Close button clicked
        TestBase.get_Screenshot("src\\test\\resource\\PARKING_OPTION\\C007_Checking_the_Close_button_functionality/" + "Before_Click_On_Close_Button" + ".jpg");
        TestBase.getObject("Close_Button_Parking").click();
        TestBase.get_Screenshot("src\\test\\resource\\PARKING_OPTION\\C007_Checking_the_Close_button_functionality/" + "After_Click_On_Close_Button" + ".jpg");
    }

    @Test(priority = 7)
    public void C008_Checking_the_Back_functionality() throws Exception {
        TestBase.getObject("Back_Button").click();
        TestBase.getObject("Back_Button").click();

        //Clear all the data Functionality Flow:
        TestBase.clear_data();
        TestBase.getObject("Back_Button").click();

    }

    @AfterTest
    public void quit() throws InterruptedException {

        if (TestBase.driver != null)
            Thread.sleep(4000);
        TestBase.driver.quit();
        System.out.println("exit");
    }








}
