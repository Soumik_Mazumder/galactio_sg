import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import java.io.IOException;

/**
 * Created by QI-37 on 24-06-2016.
 */
public class push_to_car {

    @Test(priority = 1)
            public void C001_Verify_Push2Car_Option_from_MapView() throws Exception {
        TestBase util = new TestBase();
        try {
            util.initialize();
        } catch (Exception e) {
            e.printStackTrace();
        }

        TestBase.getObject("Push2Car_search").click();
        TestBase.getObject("Push2Car_POI").click();
        TestBase.getObject("Push2Car_POI_shopping").click();
        TestBase.getObject("Push2Car_shopping_search").click();
        TestBase.getObject("Push2Car_results").click();
        Assert.assertTrue(TestBase.isElementPresent("Push2Car_map_view"), "map_view is not there");

        TestBase.getObject("Push2Car_shareicon").click();
        Assert.assertTrue(TestBase.isElementPresent("Push2Car_icon"), "Push2Car_icon is not there");

        TestBase.get_Screenshot("src\\test\\resource\\calender\\C001_Verify_Push2Car_Option_from_MapView/" + "screen1" + ".jpg");

        TestBase.driver.navigate().back();

    }

    @Test(priority = 2)
    public void C002_Verify_the_Push2Car_Page_When_Not_LogIn() throws IOException {

        TestBase.getObject("Push2Car_shareicon").click();
        TestBase.getObject("Push2Car_icon").click();

        Assert.assertTrue(TestBase.isElementPresent("Push2Car_page_header"), "Push2Car_icon is not there");
        Assert.assertTrue(TestBase.isElementPresent("Push2Car_msg1"), "Push2Car_msg1 is not there");
        Assert.assertTrue(TestBase.isElementPresent("Push2Car_msg2"), "Push2Car_msg2 is not there");

        TestBase.getScreenshotForParticularPart("mapsynq_toast", "src\\test\\resource\\Push2Car\\C002_Verify_the_Push2Car_Page_When_Not_LogIn/" + "screen1" + ".jpg");
    }

    @Test(priority = 3)
    public void C003_Verify_the_back_btn_navigation_from_push2car_to_mapView() throws IOException {

        TestBase.getObject("Back_button").click();
        Assert.assertTrue(TestBase.isElementPresent("Push2Car_map_view"), "map_view is not there");
        TestBase.getScreenshotForParticularPart("mapsynq_toast", "src\\test\\resource\\Push2Car\\C003_Verify_the_back_btn_navigation_from_push2car_to_mapView/" + "screen1" + ".jpg");
        TestBase.getObject("Back_button").click();

    }

    @Test(priority = 4)
    public void C004_Checking_Push2Car_page_After_Login(){

        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Account_Button").click();
        TestBase.getObject("email_field").clear();
        TestBase.getObject("email_label").sendKeys(TestBase.TestData.getProperty("push2car_user_name"));

        TestBase.getObject("pwd_field").clear();
        TestBase.getObject("pwd_field").sendKeys(TestBase.TestData.getProperty("push2car_pwd"));

        TestBase.driver.navigate().back();
        TestBase.getObject("log_in_btn").click();

        Assert.assertTrue(TestBase.isElementPresent("Account_display_after_login"), "Account_display_after_login is not there");
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Push2Car_search").click();
        TestBase.getObject("Push2Car_POI").click();
        TestBase.getObject("Push2Car_POI_shopping").click();
        TestBase.getObject("Push2Car_shopping_search").click();
        TestBase.getObject("Push2Car_results").click();
        Assert.assertTrue(TestBase.isElementPresent("Push2Car_map_view"), "map_view is not there");

        TestBase.getObject("Push2Car_shareicon").click();
        Assert.assertTrue(TestBase.isElementPresent("Push2Car_icon"), "Push2Car_icon is not there");

        TestBase.getObject("Push2Car_icon").click();
        Assert.assertTrue(TestBase.isElementPresent("Push2Car_page_header"), "Push2Car_icon is not there");

    }


    @AfterTest
    public void quit() throws InterruptedException {

        if (TestBase.driver != null)
            Thread.sleep(4000);
        TestBase.driver.quit();
        System.out.println("exit");
    }



}
