import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

/**
 * Created by Soumik Mazumder on 8/8/2016.
 */
public class OFFLIN_FEATURE {



    @Test(priority = 1)
    public void C001_Offline_Feature_Normal_5Munite_Flow() throws Exception {
        SoftAssert softAssert= new SoftAssert();
        TestBase utill = new TestBase();
        utill.initialize();

        //OPen any location and start the simulation
        //Open any Location in Map View and Select Favourite for Recent & Favourite Page
        TestBase.getObject("Search_Button").click();
        TestBase.getObject("POI_Options").click();
        TestBase.getObject("Emergency").click();
        TestBase.getObject("All_Emergency").click();
        Thread.sleep(5000);
        TestBase.getObject("Location_Name").click();
        Thread.sleep(2000);
        TestBase.getObject("Simulate_Button").click();
        Thread.sleep(2000);

        //Open the notification and switchoff the internet
        TestBase.driver.openNotifications();
        TestBase.getObject("WIFI").click();
        TestBase.driver.navigate().back();




        //Waite for five minute and then go
        for (int i = 0; i <= 9; i++) {
            Assert.assertTrue(TestBase.isElementPresent("NavigationScreen_Back"), "NavigationScreen_Back header is missing");
            System.out.println(i);
            Thread.sleep(30000);
        }
            if (TestBase.getObject("Offline").getText().contentEquals("Offline")) {

                //Verify the Offline popup
                softAssert.assertTrue(TestBase.isElementPresent("Offline"), "offline text not present");
                softAssert.assertTrue(TestBase.isElementPresent("Offline_Popup_Text"), "Offline_Popup_Text text not present");
                softAssert.assertTrue(TestBase.isElementPresent("OK_offlin"), "OK btn not present");

                TestBase.getObject("OK_offlin").click();
            }

            else{
                System.out.println("inside waIT");
                WebElement e = TestBase.getObject("Offline");
                WebDriverWait wait = new WebDriverWait(TestBase.driver, 60);
                wait.until(ExpectedConditions.visibilityOf(e));

    softAssert.assertTrue(TestBase.isElementPresent("Offline"), "offline text not present");
    softAssert.assertTrue(TestBase.isElementPresent("Offline_Popup_Text"), "Offline_Popup_Text text not present");
    softAssert.assertTrue(TestBase.isElementPresent("OK_offlin"), "OK btn not present");

    TestBase.getObject("OK_offlin").click();

}
        softAssert.assertAll();

    }
    @Test(priority = 2)
    public void C002_Verify_Heartbeat_Feature_and_functions() throws Exception {
        SoftAssert softAssert= new SoftAssert();
        //Go to Dashboard and check Heartbeat feature
        TestBase.driver.scrollTo("Dismiss");
        Thread.sleep(20000);
        softAssert.assertTrue(TestBase.isElementPresent("offile_msg_home_page"), "offile_msg_home_page not present");
        softAssert.assertTrue(TestBase.isElementPresent("dismiss_btn"), "dismiss_btn not present");
        softAssert.assertTrue(TestBase.isElementPresent("remind_btn"), "remind_btn not present");
        TestBase.getObject("remind_btn").click();
        Thread.sleep(30000);


        //Take a screen shots for Heart Beat feature
        TestBase.get_Screenshot("src\\test\\resource\\OFFLIN_FEATURE\\C001_Wifi_Test/" + "HeartBeat_Feature" + ".jpg");


        //Verify the Thank-You popup
        softAssert.assertTrue(TestBase.isElementPresent("Thank_U_Header"), "Thank_U_Header not present");
        softAssert.assertTrue(TestBase.isElementPresent("Thank_U_text"), "Thank_U_text not present");
        softAssert.assertTrue(TestBase.isElementPresent("OK_offlin"), "OK btn not present");
        TestBase.getObject("OK_offlin").click();
        softAssert.assertAll();

    }

    @Test(priority = 3)
    public void C003_Verify_and_check_QuickTip_popup() throws Exception {

        SoftAssert softAssert= new SoftAssert();
        //Open the internet connection and and verify the Quicktip
        TestBase.driver.openNotifications();
        TestBase.getObject("WIFI").click();
        TestBase.driver.navigate().back();
        Thread.sleep(3000);

        //Verify the quick-tip popup verification
        softAssert.assertTrue(TestBase.isElementPresent("Image_View"), "Image_View logo is not present");
        softAssert.assertTrue(TestBase.isElementPresent("Quick_Tip"), "Quick_Tip is not present");
        softAssert.assertTrue(TestBase.isElementPresent("Text"), "Text is not present");
        softAssert.assertTrue(TestBase.isElementPresent("Login_unlock_text"), "Login_unlock_text is not present");
        softAssert.assertTrue(TestBase.isElementPresent("LogIn_Button"), "LogIn_Button is not present");
        softAssert.assertTrue(TestBase.isElementPresent("Dismiss_Button"), "Dismiss_Button is not present");
        Thread.sleep(5000);

        //Click on login and verify the login page
        TestBase.getObject("LogIn_Button").click();
        Thread.sleep(1000);
        softAssert.assertTrue(TestBase.isElementPresent("Account_Page"), "Account_Page is not present");
        TestBase.getObject("Back_Button").click();
        Thread.sleep(1000);

        softAssert.assertAll();
    }

    @Test(priority = 4)
    public void C004_Verify_the_QuickTip_Functionality_without_using_RemindMe_button() throws Exception {

        SoftAssert softAssert= new SoftAssert();
        //1st time
        // Click on Navigation button
        TestBase.getObject("navigation_arrow").click();
        TestBase.driver.openNotifications();
        TestBase.getObject("WIFI").click();
        TestBase.driver.navigate().back();

        //Waite for five minute and then go
        for (int i = 0; i <= 9; i++) {
            Assert.assertTrue(TestBase.isElementPresent("NavigationScreen_Back"), "NavigationScreen_Back header is missing");
            System.out.println("i1");
            Thread.sleep(30000);
        }

        if (TestBase.getObject("Offline").getText().contentEquals("Offline")) {

            //Verify the Offline popup
            softAssert.assertTrue(TestBase.isElementPresent("Offline"), "offline text not present");
            softAssert.assertTrue(TestBase.isElementPresent("Offline_Popup_Text"), "Offline_Popup_Text text not present");
            softAssert.assertTrue(TestBase.isElementPresent("OK_offlin"), "OK btn not present");

            TestBase.getObject("OK_offlin").click();
            Thread.sleep(20000);
            //Click on OK if fist time it's not clicked
            try {
                System.out.println("Click on OK button");
                TestBase.getObject("OK_offlin").click();}
            catch (Exception e){ System.out.println(" First OK button click ");
            }
        }

        else{
            System.out.println("inside waite");
            WebElement e = TestBase.getObject("Offline");
            WebDriverWait wait = new WebDriverWait(TestBase.driver, 60);
            wait.until(ExpectedConditions.visibilityOf(e));

            softAssert.assertTrue(TestBase.isElementPresent("Offline"), "offline text not present");
            softAssert.assertTrue(TestBase.isElementPresent("Offline_Popup_Text"), "Offline_Popup_Text text not present");
            softAssert.assertTrue(TestBase.isElementPresent("OK_offlin"), "OK btn not present");

            TestBase.getObject("OK_offlin").click();
            Thread.sleep(20000);

        }


        //2nd Time
        TestBase.driver.openNotifications();
        TestBase.getObject("WIFI").click();
        TestBase.driver.navigate().back();
        Thread.sleep(30000);
        TestBase.getObject("navigation_arrow").click();

        TestBase.driver.openNotifications();
        TestBase.getObject("WIFI").click();
        TestBase.driver.navigate().back();
        //Thread.sleep(1000);


        //Waite for five minute and then go
        for (int i = 0; i <= 9; i++) {
            //System.out.println("Hello");
            Assert.assertTrue(TestBase.isElementPresent("NavigationScreen_Back"), "NavigationScreen_Back header is missing");
            System.out.println("i2");
            Thread.sleep(30000);
        }
        if (TestBase.getObject("Offline").getText().contentEquals("Offline")) {

            //Verify the Offline popup
            softAssert.assertTrue(TestBase.isElementPresent("Offline"), "offline text not present");
            softAssert.assertTrue(TestBase.isElementPresent("Offline_Popup_Text"), "Offline_Popup_Text text not present");
            softAssert.assertTrue(TestBase.isElementPresent("OK_offlin"), "OK btn not present");
            Thread.sleep(1000);
            TestBase.getObject("OK_offlin").click();

            //Click on OK if fist time it's not clicked
            try {
                System.out.println("Click on OK button");
                TestBase.getObject("OK_offlin").click();}
            catch (Exception e){ System.out.println(" First OK button click ");
            }
        }

        else{
            System.out.println("inside waite");
            WebElement e = TestBase.getObject("Offline");
            WebDriverWait wait = new WebDriverWait(TestBase.driver, 60);
            wait.until(ExpectedConditions.visibilityOf(e));

            softAssert.assertTrue(TestBase.isElementPresent("Offline"), "offline text not present");
            softAssert.assertTrue(TestBase.isElementPresent("Offline_Popup_Text"), "Offline_Popup_Text text not present");
            softAssert.assertTrue(TestBase.isElementPresent("OK_offlin"), "OK btn not present");

            TestBase.getObject("OK_offlin").click();
            Thread.sleep(20000);

        }

        //3nd Time
        TestBase.driver.openNotifications();
        TestBase.getObject("WIFI").click();
        TestBase.driver.navigate().back();
        Thread.sleep(30000);
        TestBase.getObject("navigation_arrow").click();
        Thread.sleep(10000);

        TestBase.driver.openNotifications();
        TestBase.getObject("WIFI").click();
        TestBase.driver.navigate().back();

        //Waite for five minute and then go
        for (int i = 0; i <= 9; i++) {
            Assert.assertTrue(TestBase.isElementPresent("NavigationScreen_Back"), "NavigationScreen_Back header is missing");
            System.out.println("i3");
            Thread.sleep(30000);
        }
        if (TestBase.getObject("Offline").getText().contentEquals("Offline")) {

            //Verify the Offline popup
            softAssert.assertTrue(TestBase.isElementPresent("Offline"), "offline text not present");
            softAssert.assertTrue(TestBase.isElementPresent("Offline_Popup_Text"), "Offline_Popup_Text text not present");
            softAssert.assertTrue(TestBase.isElementPresent("OK_offlin"), "OK btn not present");

            TestBase.getObject("OK_offlin").click();
            Thread.sleep(10000);

            //Click on OK if fist time it's not clicked
            try {
                System.out.println("Click on OK button");
                TestBase.getObject("OK_offlin").click();}
            catch (Exception e){ System.out.println(" First OK button click ");
            }
        }

        else{
            System.out.println("inside waite");
            WebElement e = TestBase.getObject("Offline");
            WebDriverWait wait = new WebDriverWait(TestBase.driver, 60);
            wait.until(ExpectedConditions.visibilityOf(e));

            softAssert.assertTrue(TestBase.isElementPresent("Offline"), "offline text not present");
            softAssert.assertTrue(TestBase.isElementPresent("Offline_Popup_Text"), "Offline_Popup_Text text not present");
            softAssert.assertTrue(TestBase.isElementPresent("OK_offlin"), "OK btn not present");

            TestBase.getObject("OK_offlin").click();
            Thread.sleep(20000);

        }

        //4th Time
        TestBase.driver.openNotifications();
        TestBase.getObject("WIFI").click();
        TestBase.driver.navigate().back();
        Thread.sleep(30000);
        TestBase.getObject("navigation_arrow").click();

        TestBase.driver.openNotifications();
        TestBase.getObject("WIFI").click();
        TestBase.driver.navigate().back();
        //Waite for five minute and then go
        for (int i = 0; i <= 9; i++) {
            Assert.assertTrue(TestBase.isElementPresent("NavigationScreen_Back"), "NavigationScreen_Back header is missing");
            System.out.println("i4");
            Thread.sleep(30000);
        }
        if (TestBase.getObject("Offline").getText().contentEquals("Offline")) {

            //Verify the Offline popup
            softAssert.assertTrue(TestBase.isElementPresent("Offline"), "offline text not present");
            softAssert.assertTrue(TestBase.isElementPresent("Offline_Popup_Text"), "Offline_Popup_Text text not present");
            softAssert.assertTrue(TestBase.isElementPresent("OK_offlin"), "OK btn not present");

            TestBase.getObject("OK_offlin").click();
            Thread.sleep(10000);

            //Click on OK if fist time it's not clicked
            try {
                System.out.println("Click on Ok");
                TestBase.getObject("OK_offlin").click();}
            catch (Exception e){ System.out.println(" First OK button click ");
            }
        }

        else{
            System.out.println("inside waite");
            WebElement e = TestBase.getObject("Offline");
            WebDriverWait wait = new WebDriverWait(TestBase.driver, 60);
            wait.until(ExpectedConditions.visibilityOf(e));

            softAssert.assertTrue(TestBase.isElementPresent("Offline"), "offline text not present");
            softAssert.assertTrue(TestBase.isElementPresent("Offline_Popup_Text"), "Offline_Popup_Text text not present");
            softAssert.assertTrue(TestBase.isElementPresent("OK_offlin"), "OK btn not present");

            TestBase.getObject("OK_offlin").click();
            Thread.sleep(20000);

        }


        //5th Time
        TestBase.driver.openNotifications();
        TestBase.getObject("WIFI").click();
        TestBase.driver.navigate().back();
        Thread.sleep(30000);
        TestBase.getObject("navigation_arrow").click();

        TestBase.driver.openNotifications();
        TestBase.getObject("WIFI").click();
        TestBase.driver.navigate().back();

        //Waite for five minute and then go
        for (int i = 0; i <= 9; i++) {
            Assert.assertTrue(TestBase.isElementPresent("NavigationScreen_Back"), "NavigationScreen_Back header is missing");
            System.out.println("i5");
            Thread.sleep(30000);
        }
        if (TestBase.getObject("Offline").getText().contentEquals("Offline")) {

            //Verify the Offline popup
            softAssert.assertTrue(TestBase.isElementPresent("Offline"), "offline text not present");
            softAssert.assertTrue(TestBase.isElementPresent("Offline_Popup_Text"), "Offline_Popup_Text text not present");
            softAssert.assertTrue(TestBase.isElementPresent("OK_offlin"), "OK btn not present");

            TestBase.getObject("OK_offlin").click();
            Thread.sleep(10000);

            //Click on OK if fist time it's not clicked
            try {
                System.out.println("Click on Ok");
                TestBase.getObject("OK_offlin").click();}
            catch (Exception e){ System.out.println(" First OK button click ");
            }
        }

        else{
            System.out.println("inside waite");
            WebElement e = TestBase.getObject("Offline");
            WebDriverWait wait = new WebDriverWait(TestBase.driver, 60);
            wait.until(ExpectedConditions.visibilityOf(e));

            softAssert.assertTrue(TestBase.isElementPresent("Offline"), "offline text not present");
            softAssert.assertTrue(TestBase.isElementPresent("Offline_Popup_Text"), "Offline_Popup_Text text not present");
            softAssert.assertTrue(TestBase.isElementPresent("OK_offlin"), "OK btn not present");

            TestBase.getObject("OK_offlin").click();

        }
        TestBase.driver.openNotifications();
        TestBase.getObject("WIFI").click();
        TestBase.driver.navigate().back();

        Thread.sleep(2000);
        softAssert.assertTrue(TestBase.isElementPresent("Quick_Tip"), "Quick_Tip is not present");



        /*//6th Time
        TestBase.getObject("navigation_arrow").click();
        Thread.sleep(1000);
        TestBase.driver.openNotifications();
        TestBase.getObject("WIFI").click();
        TestBase.driver.navigate().back();
        //Waite for five minute and then go
        for (int i = 0; i <= 9; i++) {
            Assert.assertTrue(TestBase.isElementPresent("NavigationScreen_Back"), "NavigationScreen_Back header is missing");
            System.out.println(i);
            Thread.sleep(30000);
        }
        TestBase.getObject("OK_offlin").click();
        Thread.sleep(2000);
        TestBase.driver.openNotifications();
        TestBase.getObject("WIFI").click();
        TestBase.driver.navigate().back();
        Thread.sleep(2000);*/


        softAssert.assertAll();
    }

}
