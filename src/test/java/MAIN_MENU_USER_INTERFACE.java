import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidElement;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

/**
 * Created by QI 33 on 22-06-2016.
 */
public class MAIN_MENU_USER_INTERFACE {

    //Verifying the splash screen
    @Test(priority = 1)
    public void C001_Galactio_Splash_Screen_Verification() throws Exception {

        TestBase utill = new TestBase();
        utill.initialize();
        Thread.sleep(5000);
        TestBase.get_Screenshot("src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C001_Splash_Screen_Verification/" + "Splash screen" + ".jpg");
    }


    // Verifiying Dashboard_UI through screenshot
    @Test(priority = 2)
    public void C002_Verify_the_Dashboard_UI() throws Exception {

        TestBase.get_Screenshot("src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C002_Verify_the_Dashboard_UI/" + "Dashboard_Screen" + ".jpg");
    }


    // Verifiying AAll section under SET_DESTINATION section through assert and screenshot
    @Test(priority = 3)
    public void C003_Verify_SET_DESTINATION_Section_In_Application_Dashboard() throws Exception {


        Assert.assertTrue(TestBase.isElementPresent("Set_Destination_Text"), "C001: Set_Destination_Text is not present in the Dashboard screen");
        Assert.assertTrue(TestBase.isElementPresent("Home_Text"), "C001: Home text is not present in the Set Home popup");
        Assert.assertTrue(TestBase.isElementPresent("Office_Text"), "C001: Office_Textis not present under SET_DESTINATION section on Dashboard screen");
        Assert.assertTrue(TestBase.isElementPresent("Favourite_Text"), "C001: Favourite_text is not present under SET_DESTINATION section on Dashboard screen");
        Assert.assertTrue(TestBase.isElementPresent("Search_option"), "C001: Search_option is not present under SET_DESTINATION section on Dashboard screen");
        TestBase.getScreenshotForParticularPart("Set_Destination_Box", "src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C003_Verify_SET_DESTINATION_Section_In_Application_Dashboard/" + "Verify Icons under SET DESTINATION sections" + ".jpg");

    }


    // Verifiying AAll section under SET_DESTINATION section through assert and screenshot
    @Test(priority = 4)
    public void C004_Verify_INCIDENTS_Section_In_Application_Dashboard() throws Exception {

        Assert.assertTrue(TestBase.isElementPresent("Incidents_field"), "C001: Favourites_text is not present in the Set Home popup");

        //Verify the incident box is blank or not
        String Incident = TestBase.getObject("Dashboard_Incidents").getText();
        if ((TestBase.getObject("Dashboard_Incidents").getText().equals(Incident))) {
            System.out.println("Incidents are present");
        } else {
            System.out.println("Incidents Sections are blank");
        }

    }


    @Test(priority = 5)

    public void C005_Verify_SUGGESTED_FOR_YOU_Section_In_Application_Dashboard() throws Exception {


        Assert.assertTrue(TestBase.isElementPresent("Suggested_For_You_field"), "C001: Suggested_For_You_box is not present in the dashboard");
        TestBase.getScreenshotForParticularPart("Suggested_For_You_field", "src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C005_Verify_SUGGESTED_FOR_YOU_Section_In_Application_Dashboard/" + "Verify first card is present or not" + ".jpg");
    }


    @Test(priority = 6)

    public void C006_Verify_All_the_Cards_in_Suggested_for_you_Section() throws Exception {

        MobileElement element=TestBase.driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(resourceId(\"com.galactio.mobile.sg:id/ignore\"))");

        TestBase.getScreenshotForParticularPart("Suggested_For_You_field", "src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C007_Verify_Suggested_For_You_Section/" + "Card1" + ".jpg");
        TestBase.getObject("Suggested_For_You_Text_Button").click();
        TestBase.getScreenshotForParticularPart("Suggested_For_You_field", "src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C007_Verify_Suggested_For_You_Section/" + "Card2" + ".jpg");
        TestBase.getObject("Suggested_For_You_Text_Button").click();
        TestBase.getScreenshotForParticularPart("Suggested_For_You_field", "src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C007_Verify_Suggested_For_You_Section/" + "Card3" + ".jpg");
        TestBase.getObject("Suggested_For_You_Text_Button").click();
        TestBase.getScreenshotForParticularPart("Suggested_For_You_field", "src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C007_Verify_Suggested_For_You_Section/" + "Card4" + ".jpg");
        TestBase.getObject("Suggested_For_You_Text_Button").click();
        TestBase.getScreenshotForParticularPart("Suggested_For_You_field", "src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C007_Verify_Suggested_For_You_Section/" + "Card5" + ".jpg");
        TestBase.getObject("Suggested_For_You_Text_Button").click();
        TestBase.getScreenshotForParticularPart("Suggested_For_You_field", "src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C007_Verify_Suggested_For_You_Section/" + "Card6" + ".jpg");
        TestBase.getObject("Suggested_For_You_Text_Button").click();
        TestBase.getScreenshotForParticularPart("Suggested_For_You_field", "src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C007_Verify_Suggested_For_You_Section/" + "Card7" + ".jpg");
        TestBase.getObject("Suggested_For_You_Text_Button").click();
        TestBase.getScreenshotForParticularPart("Suggested_For_You_field", "src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C007_Verify_Suggested_For_You_Section/" + "Card8" + ".jpg");
        TestBase.getObject("Suggested_For_You_Text_Button").click();
        TestBase.getScreenshotForParticularPart("Suggested_For_You_field", "src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C007_Verify_Suggested_For_You_Section/" + "Card9" + ".jpg");
        TestBase.getObject("Suggested_For_You_Text_Button").click();
        TestBase.getScreenshotForParticularPart("Suggested_For_You_field", "src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C007_Verify_Suggested_For_You_Section/" + "Card10" + ".jpg");
        TestBase.getObject("Suggested_For_You_Text_Button").click();
        TestBase.getScreenshotForParticularPart("Suggested_For_You_field", "src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C007_Verify_Suggested_For_You_Section/" + "Card11" + ".jpg");
        TestBase.getObject("Suggested_For_You_Text_Button").click();
        TestBase.getScreenshotForParticularPart("Suggested_For_You_field", "src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C007_Verify_Suggested_For_You_Section/" + "Card12" + ".jpg");
        TestBase.getObject("Suggested_For_You_Text_Button").click();
    }

    @Test(priority = 7)

    public void C007_Verify_Navi_Button_In_Application_Dashboard() throws Exception {

        Assert.assertTrue(TestBase.isElementPresent("Navi_Button"), "C001: Navi_Button is not present in the dashboard screen");
        TestBase.getObject("Navi_Button").click();
        TestBase.get_Screenshot("src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C007_Verify_Navi_Button_In_Application_Dashboard/" + "Navigation_screen_from dashboard" + ".jpg");
        TestBase.getObject("Back_button").click();
    }

    @Test(priority = 8)

    public void C008_Verify_Option_Button_In_Application_Dashboard() throws Exception {

        Assert.assertTrue(TestBase.isElementPresent("Option_Button"), "C001: Option_Button is not present in the dashboard screen");
        TestBase.getObject("Option_Button").click();
        Assert.assertTrue(TestBase.isElementPresent("Verify_Account_Button"), "C001: Verify_Account_Button is not present in the option section screen");
        Assert.assertTrue(TestBase.isElementPresent("Verify_Log_In_Register_Text"), "C001: Verify_Log_In/Register_Text is not present in the option section screen");
        Assert.assertTrue(TestBase.isElementPresent("Verify_Incidents_Button"), "C001: Verify_Incidents_Button is not present in the option section screen");
        Assert.assertTrue(TestBase.isElementPresent("Verify_Calendar_Button"), "C001: Verify_Calendar_Button is not present in the option section screen");
        Assert.assertTrue(TestBase.isElementPresent("Verify_Camera_Button"), "C001: Verify_Camera_Button is not present in the option section screen");
        Assert.assertTrue(TestBase.isElementPresent("Verify_Parking_Button"), "C001: Verify_Parking_Button is not present in the option section screen");
        Assert.assertTrue(TestBase.isElementPresent("Verify_Fuel_Button"), "C001: Verify_Fuel_Button is not present in the option section screen");
        Assert.assertTrue(TestBase.isElementPresent("Prayer_Time_Button"), "C001: Prayer_Time_Button is not present in the option section screen");
        Assert.assertTrue(TestBase.isElementPresent("Verify_Weather_Button"), "C001: Verify_Weather_Button is not present in the option section screen");
        Assert.assertTrue(TestBase.isElementPresent("Verify_Reported_Incidents"), "C001: Verify_Reported_Incidents is not present in the dashboard screen");
        TestBase.get_Screenshot("src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C001_Checking_display_of_Options_menu/" + "Verify_Icons_In_Option_Menu_Screen1" + ".jpg");
        TestBase.driver.scrollTo("Settings");
        Assert.assertTrue(TestBase.isElementPresent("Share_Position_Button"), "C001: Share_Position_Button is not present in the dashboard screen");
        Assert.assertTrue(TestBase.isElementPresent("Send_Location_Button"), "C001: Send_Location_Button is not present in the dashboard screen");
        Assert.assertTrue(TestBase.isElementPresent("Saved_Ads_Button"), "C001: Saved_Ads_Button is not present in the dashboard screen");
        Assert.assertTrue(TestBase.isElementPresent("Settings_Text"), "C001: Settings_Text is not present in the dashboard screen");
        Assert.assertTrue(TestBase.isElementPresent("Set_Origin_Button"), "C001: Set_Origin_Button is not present in the dashboard screen");
        Assert.assertTrue(TestBase.isElementPresent("Verify_About_Button"), "C001: Verify_About_Button is not present in the dashboard screen");
        TestBase.get_Screenshot("src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C001_Checking_display_of_Options_menu/" + "Verify_Icons_In_Option_Menu_Screen2" + ".jpg");
        TestBase.driver.navigate().back();

    }

    @Test(priority = 9)

    public void C009_Verify_Home_Button_In_Application_Dashboard() throws Exception {

        Assert.assertTrue(TestBase.isElementPresent("Home_Text"), "C001: Home text is not present in the Set Home popup");
        TestBase.getObject("Home_Text").click();

        //Verify the home set popup message along with each text
        TestBase.get_Screenshot("src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C009_Verify_Home_Button_In_Application_Dashboard/" + "Home Set Popup" + ".jpg");
        Assert.assertTrue(TestBase.isElementPresent("SetNow_Home_Text"), "C001: SetNow_Text is not present in the Set Home popup");
        Assert.assertTrue(TestBase.isElementPresent("No_Button"), "C001: No Button is not present in the Set Home popup");
        Assert.assertTrue(TestBase.isElementPresent("Yes_Button"), "C001: Yes Button is not present in the Set Home popup");
        TestBase.getObject("No_Button").click();
    }

    @Test(priority = 10)

    public void C010_Verify_Office_Button_In_Application_Dashboard() throws Exception {

        Assert.assertTrue(TestBase.isElementPresent("Office_Text"), "C001: Office text is not present in the Set Home popup");
        TestBase.getObject("Office_Text").click();

        //Verify the office set popup message along with each text
        TestBase.get_Screenshot("src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C010_Verify_Office_Button_In_Application_Dashboard/" + "Office Set Popup" + ".jpg");
        Assert.assertTrue(TestBase.isElementPresent("SetNow_Office_Text"), "C001: SetNow_Text is not present in the Set Home popup");
        Assert.assertTrue(TestBase.isElementPresent("No_Button"), "C001: No Button is not present in the Set Home popup");
        Assert.assertTrue(TestBase.isElementPresent("Yes_Button"), "C001: Yes Button is not present in the Set Home popup");
        TestBase.getObject("No_Button").click();

    }


    @Test(priority = 11)

    public void C011_Verify_Favourite_Button_In_Application_Dashboard() throws Exception {

        Assert.assertTrue(TestBase.isElementPresent("Favourites_button"), "C001: Favourites_text is not present in the Set Home popup");
        TestBase.getObject("Favourites_button").click();
        Thread.sleep(2000);
        Assert.assertTrue(TestBase.isElementPresent("no_favourites_msg1"), "C001: No Favourites yet... is not present in the Set Home popup");
        TestBase.get_Screenshot("src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C011_Verify_Favourite_Button_In_Application_Dashboard/" + "Blank_Favourite screen" + ".jpg");
        TestBase.getObject("Back_button").click();
    }

    @Test(priority = 12)

    public void C012_Verify_Search_Button_In_Application_Dashboard() throws Exception {


        Assert.assertTrue(TestBase.isElementPresent("Search_option"), "C001: Search text is not present in the Daashboard screen");
        TestBase.getObject("Search_option").click();

        //Click on phone back button to verify the each option in search destination screen and verify each options
        TestBase.driver.navigate().back();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C002_Verify_the_Set_Destination_Page/" + "Search_Destination_screen" + ".jpg");
        Assert.assertTrue(TestBase.isElementPresent("Recent_button"), "C002:Recent text is not present in the Search_Destination_screen");
        Assert.assertTrue(TestBase.isElementPresent("POI"), "C003: POI text is not present in the Daashboard screen");
        Assert.assertTrue(TestBase.isElementPresent("Favourites"), "C004: Favourites text is not present in the Daashboard screen");
        Assert.assertTrue(TestBase.isElementPresent("Coordinates"), "C005: Coordinates text is not present in the Daashboard screen");
        Assert.assertTrue(TestBase.isElementPresent("My_Home_text_1"), "C006: My_Home text is not present in the Search_Location_Screen");
        Assert.assertTrue(TestBase.isElementPresent("My_Home_text_2"), "C006: Not set text is not present in the Search_Location_Screen");
        Assert.assertTrue(TestBase.isElementPresent("My_Office_text_1"), "C006: My_Office_text is not present in the Search_Location_Screen");
        Assert.assertTrue(TestBase.isElementPresent("My_Office_text_2"), "C007: Not set text is not present in the Search_Location_Screen");
        TestBase.get_Screenshot("src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C012_Verify_Search_Button_In_Application_Dashboard/" + "All Icons in Search destination screen" + ".jpg");
        TestBase.getObject("Back_button").click();


    }
    @Test(priority = 13)

    public void C013_Verify_Favourite_and_Recent_Location_is_displaying_in_Application_dashboard_Screen() throws Exception {

        TestBase.getObject("Search_option").click();
        TestBase.getObject("Search_destination_edit_field").click();
        TestBase.getObject("Search_destination_edit_field").sendKeys(TestBase.TestData.getProperty("Location_1"));
        TestBase.selectFavourite();
        Thread.sleep(1000);
        String POI_1=TestBase.getObject("verify_POI").getText();
        TestBase.get_Screenshot("src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C013_Verify_Favourite_and_Recent_Location_is_displaying_in_Application_dashboard_Screen/" + "Saved to favourite toast message for coordinates location_2" + ".jpg");

       for(int i=0;i<=1;i++) {
           TestBase.getObject("Back_button").click();
       }
        TestBase.getObject("Clear_Data_button").click();

        //Add favourite by search destination for location_2
        TestBase.getObject("Search_destination_edit_field").click();
        TestBase.getObject("Search_destination_edit_field").sendKeys(TestBase.TestData.getProperty("Location_2"));
        TestBase.selectFavourite();
        Thread.sleep(1000);
        String POI_2=TestBase.getObject("verify_POI").getText();
        TestBase.get_Screenshot("src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C013_Verify_Favourite_and_Recent_Location_is_displaying_in_Application_dashboard_Screen/" + "Saved to favourite toast message for coordinates location_2" + ".jpg");
        for(int i=0;i<=1;i++) {
            TestBase.getObject("Back_button").click();
        }
        TestBase.getObject("Clear_Data_button").click();

        //Add favourite by search destination for location_3
        TestBase.getObject("Search_destination_field").click();
        TestBase.getObject("Search_destination_field").sendKeys(TestBase.TestData.getProperty("Location_3"));
        TestBase.getObject("Search_button").click();
        TestBase.getObject("Location_Id").click();
        Thread.sleep(1000);
        String POI_3=TestBase.getObject("verify_POI").getText();
        TestBase.get_Screenshot("src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C013_Verify_Favourite_and_Recent_Location_is_displaying_in_Application_dashboard_Screen/" + "Saved to favourite toast message for coordinates location_3" + ".jpg");
        for(int i=0;i<=1;i++) {
            TestBase.getObject("Back_button").click();
        }
        TestBase.getObject("Clear_Data_button").click();

        //Add favourite by search destination for location_3
        TestBase.getObject("Search_destination_field").click();
        TestBase.getObject("Search_destination_field").sendKeys(TestBase.TestData.getProperty("Location_4"));
        TestBase.getObject("Search_button").click();
        TestBase.getObject("Location_Id").click();
        Thread.sleep(1000);
        String POI_4=TestBase.getObject("verify_POI").getText();
        TestBase.get_Screenshot("src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C013_Verify_Favourite_and_Recent_Location_is_displaying_in_Application_dashboard_Screen/" + "Saved to favourite toast message for coordinates location_4" + ".jpg");

        for(int i=0;i<=3;i++) {
            TestBase.getObject("Back_button").click();
        }
        MobileElement element=TestBase.driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(resourceId(\"com.galactio.mobile.sg:id/image\"))");

        if(TestBase.getObject("Location_1_text").getText().equals(POI_1)&&TestBase.getObject("Location_2_text").getText().equals(POI_2)){

            System.out.println("Location_1 and Location_2 are successfully seen in dashboard screen as a favourite ");}
        else{
            Assert.assertTrue(1>2, "Wrong Favourites are added in Favourites section");}


        if(TestBase.getObject("Location_3_text").getText().equals(POI_3)&&TestBase.getObject("Location_4_text").getText().equals(POI_4)){

            System.out.println("Location_3 and Location_4 are successfully seen in dashboard screen as a recent ");}
        else{
            Assert.assertTrue(1>2, "Wrong Favourites are added in Favourites section");}


    }



        @AfterTest
        public void quit(){

            if((TestBase.driver)!=null){
                try {
                    Thread.sleep(4000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            TestBase.driver.quit();
            System.out.println("exit");
        }
    }



