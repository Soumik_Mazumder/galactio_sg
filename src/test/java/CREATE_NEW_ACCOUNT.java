import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

/**
 * Created by QI-37 on 13-06-2016.
 */
public class CREATE_NEW_ACCOUNT {

    @Test(priority = 1)
    public void C001__Click_on_CREATE_NEW_ACCOUNT_link_from_Account_page_and_Verify_the_CREATE_NEW_ACCOUNT_page_UI() throws Exception {

    TestBase util = new TestBase();
    util.initialize();

        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Account_Button").click();
        TestBase.driver.navigate().back();

        TestBase.getObject("create_new_acct").click();

        Assert.assertTrue(TestBase.isElementPresent("create_new_acct_page_header"), "create_new_acct_page_header is not there");
        Assert.assertTrue(TestBase.isElementPresent("mapsynq_setup_msg"), "Let's setup your mapSYNQ ACCOUNT is not there");
        Assert.assertTrue(TestBase.isElementPresent("autofill_wid_fb"), "autofill_wid_fb is not there");
        Assert.assertTrue(TestBase.isElementPresent("cna_first_name"), "cna_first_name is not there");
        Assert.assertTrue(TestBase.isElementPresent("cna_last_name"), "cna_last_name is not there");
        Assert.assertTrue(TestBase.isElementPresent("cna_email"), "cna_email is not there");
        Assert.assertTrue(TestBase.isElementPresent("cna_pwd"), "cna_pwd is not there");
        Assert.assertTrue(TestBase.isElementPresent("cna_dob"), "cna_dob is not there");
        Assert.assertTrue(TestBase.isElementPresent("cna_male"), "cna_male is not there");

        Assert.assertTrue(TestBase.isElementPresent("cna_female"), "cna_female is not there");
        Assert.assertTrue(TestBase.isElementPresent("cna_signup"), "cna_signup is not there");
        TestBase.driver.scrollTo("Terms and Conditions");
        Assert.assertTrue(TestBase.isElementPresent("cna_footer1"), "cna_footer1 is not there");
        Assert.assertTrue(TestBase.isElementPresent("cna_footer2"), "cna_footer2 is not there");
        Assert.assertTrue(TestBase.isElementPresent("cna_tnc"), "cna_tnc is not there");


        TestBase.get_Screenshot("src\\test\\resource\\CREATE_NEW_ACCOUNT\\C001__Click_on_CREATE_NEW_ACCOUNT_link_from_Account_page_and_Verify_the_CREATE_NEW_ACCOUNT_page_UI/" + "screen1" + ".png");

    }

    @Test(priority = 2)
    public void C002_Verify_the_error_message_for_blank_field_validation_on_CREATE_NEW_ACCOUNT_page() throws Exception {

        TestBase.getObject("cna_signup").click();
        TestBase.getObject("cna_first_name").click();

        TestBase.getScreenshotForParticularPart("first_name_error", "src\\test\\resource\\CREATE_NEW_ACCOUNT\\C002_Verify_the_error_message_without_populating_any_fields_on_CREATE_NEW_ACCOUNT_and_click_on_SignUp/" + "first_name_error" + ".png");
        String result = TestBase.TextFromImage("src\\test\\resource\\CREATE_NEW_ACCOUNT\\C002_Verify_the_error_message_without_populating_any_fields_on_CREATE_NEW_ACCOUNT_and_click_on_SignUp/" + "first_name_error" + ".png");
        System.out.println(result);
        String exp = "Field cannot be empty";
        if(result.toLowerCase().contains(exp.toLowerCase())){
            System.out.println("passed");
        }else{
            Assert.assertTrue(1>2, "The tool tip error message is not displayed correctly for incorrect password");
        }
        TestBase.get_Screenshot("src\\test\\resource\\CREATE_NEW_ACCOUNT\\C002_Verify_the_error_message_without_populating_any_fields_on_CREATE_NEW_ACCOUNT_and_click_on_SignUp/" + "screen1" + ".png");
    }

    @Test(priority = 3)

    public void C003_Verify_the_field_validations_for_First_name_Last_name_field_on_CREATE_NEW_ACCOUNT_and_click_on_SignUp() throws Exception {

        /*first name validation*/

        TestBase.getObject("cna_first_name").sendKeys(TestBase.TestData.getProperty("first_name"));
        try{
            TestBase.getObject("cna_signup").click();
        }catch(Exception e){
            TestBase.driver.navigate().back();
            TestBase.getObject("cna_signup").click();
        }
        TestBase.getObject("cna_last_name").click();
        TestBase.driver.navigate().back();
        TestBase.getScreenshotForParticularPart("last_name_error", "src\\test\\resource\\CREATE_NEW_ACCOUNT\\C003_Verify_the_field_validations_for_First_name_Last_name_field_on_CREATE_NEW_ACCOUNT_and_click_on_SignUp/" + "last_name_error" + ".png");
        String result = TestBase.TextFromImage("src\\test\\resource\\CREATE_NEW_ACCOUNT\\C003_Verify_the_field_validations_for_First_name_Last_name_field_on_CREATE_NEW_ACCOUNT_and_click_on_SignUp/" + "last_name_error" + ".png");
        System.out.println(result);
        String exp = "Field cannot be empty";
        if(result.toLowerCase().contains(exp.toLowerCase())){
            System.out.println("passed");
        }else{
            Assert.assertTrue(1>2, "The tool tip error message is not displayed correctly for incorrect password");
        }

        TestBase.get_Screenshot("src\\test\\resource\\CREATE_NEW_ACCOUNT\\C003_Verify_the_field_validations_for_First_name_Last_name_field_on_CREATE_NEW_ACCOUNT_and_click_on_SignUp/" + "screen1" + ".png");

        /**********************************************************************************************************************/
        /*last name validation*/


        TestBase.getObject("cna_last_name").sendKeys(TestBase.TestData.getProperty("last_name"));
        Thread.sleep(5000);
        try{
            TestBase.getObject("cna_signup").click();
        }catch(Exception e){
            TestBase.driver.navigate().back();
            TestBase.getObject("cna_signup").click();
        }
        TestBase.getObject("cna_email").click();
        TestBase.driver.navigate().back();
        TestBase.getScreenshotForParticularPart("email_error", "src\\test\\resource\\CREATE_NEW_ACCOUNT\\C003_Verify_the_field_validations_for_First_name_Last_name_field_on_CREATE_NEW_ACCOUNT_and_click_on_SignUp/" + "email_error" + ".png");
        String result1 = TestBase.TextFromImage("src\\test\\resource\\CREATE_NEW_ACCOUNT\\C003_Verify_the_field_validations_for_First_name_Last_name_field_on_CREATE_NEW_ACCOUNT_and_click_on_SignUp/" + "email_error" + ".png");
        System.out.println(result1);
        String exp1 = "Please enter a valid email address";
        if(result1.toLowerCase().contains(exp1.toLowerCase())){
            System.out.println("passed");
        }else{
            Assert.assertTrue(1>2, "The tool tip error message is not displayed correctly for incorrect password");
        }

        TestBase.get_Screenshot("src\\test\\resource\\CREATE_NEW_ACCOUNT\\C003_Verify_the_field_validations_for_First_name_Last_name_field_on_CREATE_NEW_ACCOUNT_and_click_on_SignUp/" + "screen2" + ".png");



    }


    @Test(priority = 4)
    public void C004_Verify_the_Email_field_Validation_on_CREATE_NEW_ACCOUNT_by_entering_invalid_email() throws Exception {

        TestBase.getObject("email_id").sendKeys(TestBase.TestData.getProperty("CNA_invalid_email"));
        Thread.sleep(3000);
        try{
            TestBase.getObject("cna_signup").click();
        }catch(Exception e){
            TestBase.driver.navigate().back();
            TestBase.getObject("cna_signup").click();
        }

        TestBase.getObject("email_id").click();
        TestBase.driver.navigate().back();
        Thread.sleep(5000);
        TestBase.getScreenshotForParticularPart("email_error", "src\\test\\resource\\CREATE_NEW_ACCOUNT\\C004_Verify_the_Email_field_Validation_on_CREATE_NEW_ACCOUNT/" + "email_error" + ".png");
        String result1 = TestBase.TextFromImage("src\\test\\resource\\CREATE_NEW_ACCOUNT\\C004_Verify_the_Email_field_Validation_on_CREATE_NEW_ACCOUNT/" + "email_error" + ".png");
        System.out.println(result1);
        String exp1 = "Please enter a valid email address";
        if(result1.toLowerCase().contains(exp1.toLowerCase())){
            System.out.println("passed");
        }else{
            Assert.assertTrue(1>2, "The tool tip error message is not displayed correctly for incorrect password");
        }

        TestBase.get_Screenshot("src\\test\\resource\\CREATE_NEW_ACCOUNT\\C004_Verify_the_Email_field_Validation_on_CREATE_NEW_ACCOUNT/" + "screen1" + ".png");

    }

    @Test(priority = 5)
    public void C005_Verify_the_Email_field_Validation_on_CREATE_NEW_ACCOUNT_by_keeping_Email_field_as_blank() throws Exception {

        TestBase.getObject("email_id").clear();


        try{
            TestBase.getObject("cna_signup").click();
        }catch(Exception e){
            TestBase.driver.navigate().back();
            TestBase.getObject("cna_signup").click();
        }

        TestBase.getObject("email_id").click();
        TestBase.driver.navigate().back();
        TestBase.getScreenshotForParticularPart("email_error", "src\\test\\resource\\CREATE_NEW_ACCOUNT\\C005_Verify_the_Email_field_Validation_on_CREATE_NEW_ACCOUNT_by_keeping_Email_field_as_blank/" + "email_error" + ".png");
        String result = TestBase.TextFromImage("src\\test\\resource\\CREATE_NEW_ACCOUNT\\C005_Verify_the_Email_field_Validation_on_CREATE_NEW_ACCOUNT_by_keeping_Email_field_as_blank/" + "email_error" + ".png");
        System.out.println(result);
        String exp = "Please enter a valid email address";
        if(result.toLowerCase().contains(exp.toLowerCase())){
            System.out.println("passed");
        }else{
            Assert.assertTrue(1>2, "The tool tip error message is not displayed correctly for incorrect password");
        }

        TestBase.get_Screenshot("src\\test\\resource\\CREATE_NEW_ACCOUNT\\C005_Verify_the_Email_field_Validation_on_CREATE_NEW_ACCOUNT_by_keeping_Email_field_as_blank/" + "screen1" + ".png");
    }


        @Test(priority = 6)
        public void C006_Verify_the_error_message_when_no_password_is_entered_on_CREATE_NEW_ACCOUNT() throws Exception {

            TestBase.getObject("email_id").sendKeys(TestBase.TestData.getProperty("CNA_valid_email"));

            try{
                TestBase.getObject("cna_signup").click();
            }catch(Exception e){
                TestBase.driver.navigate().back();
                TestBase.getObject("cna_signup").click();
            }

            TestBase.getObject("cna_pwd").click();
            TestBase.driver.navigate().back();
            TestBase.getScreenshotForParticularPart("pwd_error", "src\\test\\resource\\CREATE_NEW_ACCOUNT\\C006_Verify_the_error_message_when_no_password_is_entered_on_CREATE_NEW_ACCOUNT/" + "pwd_error" + ".png");
            String result = TestBase.TextFromImage("src\\test\\resource\\CREATE_NEW_ACCOUNT\\C006_Verify_the_error_message_when_no_password_is_entered_on_CREATE_NEW_ACCOUNT/" + "pwd_error" + ".png");
            System.out.println(result);
            String exp = "Field cannot be empty";
            if(result.toLowerCase().contains(exp.toLowerCase())){
                System.out.println("passed");
            }else{
                Assert.assertTrue(1>2, "The tool tip error message is not displayed correctly for incorrect password");
            }

            TestBase.get_Screenshot("src\\test\\resource\\CREATE_NEW_ACCOUNT\\C006_Verify_the_error_message_when_no_password_is_entered_on_CREATE_NEW_ACCOUNT/" + "screen1" + ".png");

        }

    /*@Test(priority = 7)
    public void C007_Verify_the_error_message_when_password_less_than_8_Characters_entered_on_CREATE_NEW_ACCOUNT() throws Exception {

        TestBase.getObject("cna_pwd").sendKeys(TestBase.TestData.getProperty("CNA_invalid_pwd"));

        try{
            TestBase.getObject("cna_signup").click();
        }catch(Exception e){
            TestBase.driver.navigate().back();
            TestBase.getObject("cna_signup").click();
        }

        TestBase.getObject("cna_dob").click();
        TestBase.driver.navigate().back();

        TestBase.getScreenshotForParticularPart("pwd_error", "src\\test\\resource\\CREATE_NEW_ACCOUNT\\C007_Verify_the_error_message_when_password_less_than_8_Characters_entered_on_CREATE_NEW_ACCOUNT/" + "pwd_error" + ".png");
        String result = TestBase.TextFromImage("src\\test\\resource\\CREATE_NEW_ACCOUNT\\C007_Verify_the_error_message_when_password_less_than_8_Characters_entered_on_CREATE_NEW_ACCOUNT/" + "pwd_error" + ".png");
        System.out.println(result);
        String exp = "Field cannot be empty";
        if(result.toLowerCase().contains(exp.toLowerCase())){
            System.out.println("passed");
        }else{
            Assert.assertTrue(1>2, "The tool tip error message is not displayed correctly for incorrect password");
        }

        TestBase.get_Screenshot("src\\test\\resource\\CREATE_NEW_ACCOUNT\\C007_Verify_the_error_message_when_password_less_than_8_Characters_entered_on_CREATE_NEW_ACCOUNT/" + "screen1" + ".png");
    }

    @Test(priority = 8)
    public void C008_Verify_the_error_message_when_no_DOB_is_entered_on_CREATE_NEW_ACCOUNT() throws Exception {

        TestBase.getObject("cna_pwd").clear();
        TestBase.getObject("cna_pwd").sendKeys(TestBase.TestData.getProperty("CNA_valid_pwd"));


        try{
            TestBase.getObject("cna_signup").click();
        }catch(Exception e){
            TestBase.driver.navigate().back();
            TestBase.getObject("cna_signup").click();
        }

        TestBase.getObject("cna_dob").click();
        TestBase.driver.navigate().back();
        TestBase.getScreenshotForParticularPart("pwd_error", "src\\test\\resource\\CREATE_NEW_ACCOUNT\\C008_Verify_the_error_message_when_no_DOB_is_entered_on_CREATE_NEW_ACCOUNT/" + "pwd_error" + ".png");
        String result = TestBase.TextFromImage("src\\test\\resource\\CREATE_NEW_ACCOUNT\\C007_Verify_the_error_message_when_password_less_than_8_Characters_entered_on_CREATE_NEW_ACCOUNT/" + "pwd_error" + ".png");
        System.out.println(result);
        String exp = "Field cannot be empty";
        if(result.toLowerCase().contains(exp.toLowerCase())){
            System.out.println("passed");
        }else{
            Assert.assertTrue(1>2, "The tool tip error message is not displayed correctly for incorrect password");
        }

        TestBase.get_Screenshot("src\\test\\resource\\CREATE_NEW_ACCOUNT\\C008_Verify_the_error_message_when_no_DOB_is_entered_on_CREATE_NEW_ACCOUNT/" + "screen1" + ".png");
    }
*/
    @Test(priority = 9)
    public void C009_Verify_the_error_message_when_no_Gender_is_entered_on_CREATE_NEW_ACCOUNT() throws Exception {


        /*TestBase.getObject("cna_dob").click();
        TestBase.getObject("select_DOB").click();

        TestBase.getObject("DOB_ok").click();
*/

        TestBase.getObject("cna_pwd").sendKeys(TestBase.TestData.getProperty("CNA_valid_pwd"));
        TestBase.driver.navigate().back();
        TestBase.getObject("cna_dob").click();
        TestBase.getObject("done").click();
        TestBase.getObject("cna_signup").click();
        TestBase.getObject("gender_error").click();

        TestBase.getScreenshotForParticularPart("gender_error_display", "src\\test\\resource\\CREATE_NEW_ACCOUNT\\C009_Verify_the_error_message_when_no_Gender_is_entered_on_CREATE_NEW_ACCOUNT/" + "gender_error_display" + ".png");
        String result = TestBase.TextFromImage("src\\test\\resource\\CREATE_NEW_ACCOUNT\\C009_Verify_the_error_message_when_no_Gender_is_entered_on_CREATE_NEW_ACCOUNT/" + "gender_error_display" + ".png");
        System.out.println(result);
        String exp = "Field cannot be empty";
        if(result.toLowerCase().contains(exp.toLowerCase())){
            System.out.println("passed");
        }else{
            Assert.assertTrue(1>2, "The tool tip error message is not displayed correctly for incorrect password");
        }

        TestBase.get_Screenshot("src\\test\\resource\\CREATE_NEW_ACCOUNT\\C009_Verify_the_error_message_when_no_Gender_is_entered_on_CREATE_NEW_ACCOUNT/" + "screen1" + ".png");
    }

    @Test(priority = 10)
    public void C10_Verify_successfull_SignUp_from_CREATE_NEW_ACCOUNT_screen() throws Exception {

        TestBase.getObject("new_first_name").clear();
        TestBase.getObject("new_first_name").sendKeys(TestBase.TestData.getProperty("first_name"));

        TestBase.getObject("new_last_name").clear();
        TestBase.getObject("new_last_name").sendKeys(TestBase.TestData.getProperty("last_name"));

        TestBase.getObject("unused_email_id").clear();
        TestBase.getObject("unused_email_id").sendKeys(TestBase.TestData.getProperty("CNA_valid_email"));

        TestBase.getObject("new_pwd").clear();
        TestBase.getObject("new_pwd").sendKeys(TestBase.TestData.getProperty("CNA_valid_pwd"));


        try {
            TestBase.getObject("new_dob").click();
        }catch (Exception e){
            TestBase.driver.navigate().back();
            TestBase.getObject("new_dob").click();
        }
      /*  TestBase.getObject("select_DOB").click();
        TestBase.getObject("DOB_ok").click();
*/

        TestBase.getObject("done").click();

        TestBase.getObject("gender").click();

        TestBase.get_Screenshot("src\\test\\resource\\CREATE_NEW_ACCOUNT\\C10_Verify_successfull_SignUp_from_CREATE_NEW_ACCOUNT_screen/" + "screen1" + ".png");

        TestBase.getObject("cna_signup").click();

        TestBase.get_Screenshot("src\\test\\resource\\CREATE_NEW_ACCOUNT\\C10_Verify_successfull_SignUp_from_CREATE_NEW_ACCOUNT_screen/" + "screen2" + ".png");


    }
}