import io.appium.java_client.TouchAction;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

/**
 * Created by Soumik on 5/13/2016.
 */
public class WEATHER_FEATURE {

    @Test(priority = 1)
    public void C001_Verify_the_Weather_Feature_from_Options() throws Exception {

        TestBase utill = new TestBase();
        utill.initialize();
        Thread.sleep(5000);

        // Click on Option Button
        TestBase.getObject("Option_Button").click();

        // Click on Weather Button
        TestBase.getObject("Verify_Weather_Button").click();
        Thread.sleep(3000);

        //Screenshot for City_Weather_Screen
        TestBase.get_Screenshot("src\\test\\resource\\WEATHER_FEATURE\\C001_Verify_the_Weather_Feature_from_Options/" + "City_Weather_Screen" + ".jpg");
    }

    @Test(priority = 2)
    public void C002_Verify_multiple_City_Weather() throws Exception {

        //Click on City DropDown List
        TestBase.getObject("Drop_Down").click();

        //Take a screen shots and verify the City drop down
        TestBase.get_Screenshot("src\\test\\resource\\WEATHER_FEATURE\\C002_Verify_multiple_City_Weather/" + "Multiple_City_Name" + ".jpg");

        //Select any City and observe the screen
        TestBase.getObject("North_Singapore").click();
        TestBase.get_Screenshot("src\\test\\resource\\WEATHER_FEATURE\\C002_Verify_multiple_City_Weather/" + "North_Singapore_Weather" + ".jpg");
        TestBase.getObject("North_Singapore").click();

        TestBase.getObject("South_Singapore").click();
        TestBase.get_Screenshot("src\\test\\resource\\WEATHER_FEATURE\\C002_Verify_multiple_City_Weather/" + "South_Singapore_Weather" + ".jpg");
        TestBase.getObject("South_Singapore").click();

        TestBase.getObject("East_Singapore").click();
        TestBase.get_Screenshot("src\\test\\resource\\WEATHER_FEATURE\\C002_Verify_multiple_City_Weather/" + "East_Singapore_Weather" + ".jpg");
        TestBase.getObject("East_Singapore").click();

        TestBase.getObject("West_Singapore").click();
        TestBase.get_Screenshot("src\\test\\resource\\WEATHER_FEATURE\\C002_Verify_multiple_City_Weather/" + "West_Singapore_Weather" + ".jpg");
        TestBase.getObject("West_Singapore").click();

        TestBase.getObject("Central_Singapore").click();
        TestBase.get_Screenshot("src\\test\\resource\\WEATHER_FEATURE\\C002_Verify_multiple_City_Weather/" + "Central_Singapore_Weather" + ".jpg");
    }

    @Test(priority = 3)
    public void C003_Select_Favourite_any_location_as_Favourite() throws Exception {
        TestBase.getObject("Central_Singapore").click();
        TestBase.getObject("West_Singapore").click();
        //Tap Favourite Button
        TestBase.getObject("Fav_Button").click();
        TestBase.get_Screenshot("src\\test\\resource\\WEATHER_FEATURE\\C003_Select_Favourite_any_location_as_Favourite/" + "Fav_Button" + ".jpg");

    }

    @Test(priority = 4)
    public void C004_Verify_user_is_able_to_remove_a_region_from_Favourites() throws Exception {
        //Go to Favourites Tab
        TestBase.getObject("Favourite_Tab").click();
        TestBase.get_Screenshot("src\\test\\resource\\WEATHER_FEATURE\\C004_Verify_user_is_able_to_remove_a_region_from_Favourites/" + "Favourite_Tab_Screen" + ".jpg");

        //Long Press On selected Favourite
        TouchAction action = new TouchAction(TestBase.driver);
        action.longPress(TestBase.getObject("Long_Press")).perform();
        Thread.sleep(5000);

        TestBase.get_Screenshot("src\\test\\resource\\WEATHER_FEATURE\\C004_Checking_the_Favourite_tab/" + "Delete_Fav_Popup" + ".jpg");
        TestBase.getObject("OK_Button").click();
        TestBase.get_Screenshot("src\\test\\resource\\WEATHER_FEATURE\\C004_Checking_the_Favourite_tab/" + "After_Delete_the_Fav" + ".jpg");
    }

    @Test(priority = 5)
    public void C005_Verify_the_back_Functionality_in_weather_page() throws Exception {
        //Click on Back(<) button and return to the dashboard
        TestBase.getObject("Back_Button").click();

    }


    @Test(priority = 6)
    public void C006_Verify_the_Weather_Screen_default_UI_Check() throws Exception {

        // Click on Option Button
        TestBase.getObject("Option_Button").click();

        // Click on Weather Button
        TestBase.getObject("Verify_Weather_Button").click();
        Thread.sleep(3000);


        //Verify the Weather page UI
        Assert.assertTrue(TestBase.isElementPresent("Weather_Text"), "Weather_Text  is not present");
        Assert.assertTrue(TestBase.isElementPresent("City_tab_text"), "City_tab_text  is not present");
        Assert.assertTrue(TestBase.isElementPresent("Favourite_tab_text"), "Favourite_tab_text  is not present");

        //UI checking
        TestBase.get_Screenshot("src\\test\\resource\\WEATHER_FEATURE\\C006_Verify_the_Weather_Screen_default_UI_Check/" + "Weather_Screen_UI" + ".jpg");

    }


    @Test(priority = 7)
    public void C007_Verify_Weather_Favourite_Screen_When_NoData_Available() throws Exception {
        //Click on Favourite tab and verify the
        TestBase.getObject("Favourite_tab_text").click();

        //Verify the No Data available
        Assert.assertTrue(TestBase.isElementPresent("No_fav_Yet_text"), "No_fav_Yet_text  is not present");
    }

    @Test(priority = 8)
    public void C008_Verify_Weather_Default_Message_for_a_region_without_data_availablity() throws Exception {

        //Go to City tab
        TestBase.getObject("City_tab_text").click();

        //Open the location where no data available
        TestBase.getObject("Drop_Down").click();
        TestBase.getObject("Central_Singapore").click();
        Thread.sleep(1000);

        Assert.assertTrue(TestBase.isElementPresent("No_weather_data"), "No_weather_data available text is not present");

    }

    @Test(priority = 9)
    public void C009_Weather_Verify_the_user_is_able_to_toggle_Region_and_predictions_under_Favourites() throws Exception {

        System.out.println("This Test cases need to be tested in Manual Testing");

    }

    @Test(priority = 10)
    public void C010_Weather_Verify_the_user_is_able_to_toggle_Region_and_predictions_under_Favourites() throws Exception {

        //Select more than two location as Favourite
        TestBase.getObject("Fav_Button").click();
        Thread.sleep(1000);
        TestBase.getObject("Central_Singapore").click();
        TestBase.getObject("West_Singapore").click();
        Thread.sleep(1000);

        //Go to Favourites page and delete one record and verify the No data



    }





    @AfterTest
    public void quit() throws InterruptedException {

        if (TestBase.driver != null)
            Thread.sleep(4000);
        TestBase.driver.quit();
        System.out.println("exit");



    }

}
