import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

/**
 * Created by Soumik on 5/12/2016.
 */
public class FUEL_Option {

    @Test(priority = 1)
    public void C001_Verify_The_Office_PopUp_Functionality() throws Exception {
        TestBase utill = new TestBase();
        utill.initialize();
        Thread.sleep(5000);

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }

        //Click on Option Button and verify the Fuel section
        TestBase.getObject("Option_Button").click();

        Assert.assertTrue(TestBase.isElementPresent("Fuel_Option"), "C001: Fuel_Option is not present in Options");
        TestBase.getObject("Fuel_Option").click();
        Thread.sleep(5000);
        }

    @Test(priority = 2)
    public void C002_Verify_the_Fuel_Screen_UI_Check() throws Exception {

        //Take a Screenshot For "TYPE" tab
        TestBase.get_Screenshot("src\\test\\resource\\FUEL_Option\\C002_Verify_the_Fuel_Screen_UI_Check/" + "Fuel_UI1" + ".jpg");

        //Click on "Brand" tab and verify the UI
        TestBase.getObject("BRAND_Tab").click();
        TestBase.get_Screenshot("src\\test\\resource\\FUEL_Option\\C002_Verify_the_Fuel_Screen_UI_Check/" + "Fuel_UI2" + ".jpg");

    }

    @Test(priority = 3)
    public void C003_Verify_the_TYPE_tab_Function() throws Exception {

        //Click on "TYPE" tab
        TestBase.getObject("TYPE_Tab").click();


        //Verify the Type of fuel present in this screen
        Assert.assertTrue(TestBase.isElementPresent("Fuel_Text"), "C003: Fuel_Text is not present in the screen header");


        Assert.assertTrue(TestBase.isElementPresent("Diesel_Text"), "C003: Diesel_Text is not present in the screen");
        TestBase.getObject("Diesel_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\FUEL_Option\\C003_Verify_the_TYPE_tab_Function/" + "Diesel_Text_Category" + ".jpg");
        TestBase.getObject("Diesel_Text").click();

        Assert.assertTrue(TestBase.isElementPresent("G92_Text"), "C003: G92_Text is not present in the screen");
        TestBase.getObject("G92_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\FUEL_Option\\C003_Verify_the_TYPE_tab_Function/" + "G92_Text_Category" + ".jpg");
        TestBase.getObject("G92_Text").click();

        Assert.assertTrue(TestBase.isElementPresent("G95_Text"), "C003: G95_Text is not present in the screen");
        TestBase.getObject("G95_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\FUEL_Option\\C003_Verify_the_TYPE_tab_Function/" + "G95_Text_Category" + ".jpg");
        TestBase.getObject("G95_Text").click();

        Assert.assertTrue(TestBase.isElementPresent("G98_Text"), "C003: G98_Text is not present in the screen");
        TestBase.getObject("G98_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\FUEL_Option\\C003_Verify_the_TYPE_tab_Function/" + "G98_Text_Category" + ".jpg");
        TestBase.getObject("G98_Text").click();

        Assert.assertTrue(TestBase.isElementPresent("Premium_Text"), "C003: Premium_Text is not present in the screen");
        TestBase.getObject("Premium_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\FUEL_Option\\C003_Verify_the_TYPE_tab_Function/" + "Premium_Text_Category" + ".jpg");
        TestBase.getObject("Premium_Text").click();
    }

    @Test(priority = 4)
    public void C004_Verify_the_BRAND_tab_Function() throws Exception {

        //Click on "Brand" tab and verify
        TestBase.getObject("BRAND_Tab").click();

        //Verify the Type of fuel Brand present in this screen

        TestBase.getObject("Caltex_Text").click();
        Assert.assertTrue(TestBase.isElementPresent("Caltex_Text"), "C004: Caltex_Brand is not present in the screen");
        TestBase.get_Screenshot("src\\test\\resource\\FUEL_Option\\C004_Verify_the_BRAND_tab_Function/" + "Caltex_Brand" + ".jpg");
        TestBase.getObject("Caltex_Text").click();

        TestBase.getObject("Esso_Text").click();
        Assert.assertTrue(TestBase.isElementPresent("Esso_Text"), "C004: Esso_Text is not present in the screen");
        TestBase.get_Screenshot("src\\test\\resource\\FUEL_Option\\C004_Verify_the_BRAND_tab_Function/" + "Esso_Brand" + ".jpg");
        TestBase.getObject("Esso_Text").click();

        TestBase.getObject("SPC_Text").click();
        Assert.assertTrue(TestBase.isElementPresent("SPC_Text"), "C004: SPC_Text is not present in the screen");
        TestBase.get_Screenshot("src\\test\\resource\\FUEL_Option\\C004_Verify_the_BRAND_tab_Function/" + "SPC_Brand" + ".jpg");
        TestBase.getObject("SPC_Text").click();

        TestBase.getObject("Shell_Text").click();
        Assert.assertTrue(TestBase.isElementPresent("Shell_Text"), "C004: Shell_Text is not present in the screen");
        TestBase.get_Screenshot("src\\test\\resource\\FUEL_Option\\C004_Verify_the_BRAND_tab_Function/" + "Shell_Brand" + ".jpg");
        TestBase.getObject("Shell_Text").click();

    }

    @Test(priority = 5)
    public void C005_Verify_the_Refresh_Functionality_in_Fuel_Page() throws Exception {

        Assert.assertTrue(TestBase.isElementPresent("Refresh_Button_Fuel"), "C005: Refresh button is not present in the 'BRAND' tab");
        TestBase.getObject("Refresh_Button_Fuel").click();
        TestBase.get_Screenshot("src\\test\\resource\\FUEL_Option\\C005_Verify_the_Refresh_Functionality_in_Fuel_Page/" + "Refresh_BRAND_Page" + ".jpg");

        TestBase.getObject("TYPE_Tab").click();
        Assert.assertTrue(TestBase.isElementPresent("Refresh_Button_Fuel"), "C005: Refresh button is not present in the 'TYPE' tab");
        TestBase.getObject("Refresh_Button_Fuel").click();
        TestBase.get_Screenshot("src\\test\\resource\\FUEL_Option\\C005_Verify_the_Refresh_Functionality_in_Fuel_Page/" + "Refresh_TYPE_Page" + ".jpg");

        TestBase.getObject("Back_Button").click();

    }

    @AfterTest
    public void quit() throws InterruptedException {

        if (TestBase.driver != null)
            Thread.sleep(4000);
        TestBase.driver.quit();
        System.out.println("exit");
    }

}
