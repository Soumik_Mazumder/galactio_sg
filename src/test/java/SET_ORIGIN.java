import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

/**
 * Created by Soumik on 5/16/2016.
 */
public class SET_ORIGIN {

    @Test(priority = 1)
    public void C001_Accessing_Set_Origin_Option() throws Exception {

        TestBase utill = new TestBase();
        utill.initialize();
        Thread.sleep(2000);

        // Click on Option Button
        TestBase.getObject("Option_Button").click();

        //Scroll for Set Origin Option and click on it
        TestBase.driver.scrollTo("Set Origin");
        TestBase.getObject("Set_Origin").click();

        //Screen shot for Set origin screen
        TestBase.get_Screenshot("src\\test\\resource\\SET_ORIGIN\\C001_Accessing_Set_Origin_Option/" + "Set_Origin_Page" + ".jpg");
        TestBase.getObject("Back_Button").click();

       }

    @Test(priority = 2)
    public void C002_Checking_the_functionality_when_Origin_not_set() throws Exception {

        //Open any location and start simulation to verify Origin is not set
        TestBase.getObject("Search_Button").click();
        TestBase.getObject("POI_Options").click();
        TestBase.getObject("Emergency").click();
        TestBase.getObject("All_Emergency").click();
        Thread.sleep(5000);
        TestBase.getObject("Location_Name").click();
        Thread.sleep(2000);
        TestBase.getObject("Simulate_Button").click();
        Thread.sleep(2000);

        //Verify simulation start
        TestBase.get_Screenshot("src\\test\\resource\\SET_ORIGIN\\C002_Checking_the_functionality_when_Origin_not_set/" + "Simulation_Origin_Not_Set" + ".jpg");

        TestBase.getObject("NavigationScreen_Back").click();

        //Clear all the data Functionality Flow:
        TestBase.clear_data();
        TestBase.getObject("Back_Button").click();
    }

    @Test(priority = 3)
    public void C003_Checking_the_Set_Origin_When_Recent_is_Not_Present() throws Exception {

        // Click on Option Button
        TestBase.getObject("Option_Button").click();

        //Scroll for Set Origin Option and click on it
        TestBase.driver.scrollTo("Set Origin");
        TestBase.getObject("Set_Origin").click();

        //Click on Recent when no Recent is present
        TestBase.getObject("Recent_Options").click();

        //Verify the recent page
        TestBase.get_Screenshot("src\\test\\resource\\SET_ORIGIN\\C003_Checking_the_Set_Origin_When_Recent_is_Not_Present/" + "No_Recent_Yet_From_SetOrigin" + ".jpg");
        TestBase.getObject("Back_Button").click();
        TestBase.getObject("Back_Button").click();
    }

    @Test(priority = 4)
    public void C004_Checking_the_Set_Origin_When_Recent_is_Present() throws Exception {
        //Open any location for Recent
        TestBase.getObject("Search_Button").click();
        TestBase.getObject("POI_Options").click();
        TestBase.getObject("Emergency").click();
        TestBase.getObject("All_Emergency").click();
        Thread.sleep(5000);
        TestBase.getObject("Location_Name").click();
        Thread.sleep(2000);

        //  Creating Loop to back from the screen
        for (int i = 0; i < 5; i++) {
            TestBase.getObject("Back_Button").click();
        }

        //Again go to set origin screen
        // Click on Option Button
        TestBase.getObject("Option_Button").click();

        //Scroll for Set Origin Option and click on it
        TestBase.driver.scrollTo("Set Origin");
        TestBase.getObject("Set_Origin").click();

        //Open Recent and open the location in map view
        TestBase.getObject("Recent_Options").click();

        //Open the location in map view and verify the screen and select as Origin
        TestBase.getObject("Location_Name").click();

        TestBase.get_Screenshot("src\\test\\resource\\SET_ORIGIN\\C004_Checking_the_Set_Origin_When_Recent_is_Present/" + "Origin_Flag_SetOrigin_Button" + ".jpg");
        Thread.sleep(2000);
        TestBase.getObject("Set_Origin_Close").click();

        //Take a screen shots for successfully origin set
        TestBase.get_Screenshot("src\\test\\resource\\SET_ORIGIN\\C004_Checking_the_Set_Origin_When_Recent_is_Present/" + "Origin_Set_Successfully" + ".jpg");
    }

    @Test(priority = 5)
    public void C005_Checking_the_Set_Origin_When_Favourites_are_Not_Present() throws Exception {

        // Click on Option Button
        TestBase.getObject("Option_Button").click();

        //Scroll for Set Origin Option and click on it
        TestBase.driver.scrollTo("Set Origin");
        TestBase.getObject("Set_Origin").click();
        TestBase.getObject("Favourite_Options").click();

        //Take a screen shot No Favourite yet
        TestBase.get_Screenshot("src\\test\\resource\\SET_ORIGIN\\C005_Checking_the_Set_Origin_When_Favourites_are_Not_Present/" + "No_Fav_Yet" + ".jpg");
        TestBase.getObject("Back_Button").click();
        TestBase.getObject("Back_Button").click();

    }

    @Test(priority = 6)
    public void C006_Checking_the_Set_Origin_When_Favourites_are_Present() throws Exception {

        //Open any Location in Map View and Select Favourite for Recent & Favourite Page
        TestBase.getObject("Search_Button").click();
        TestBase.getObject("POI_Options").click();
        TestBase.getObject("Emergency").click();
        TestBase.getObject("All_Emergency").click();
        Thread.sleep(5000);
        TestBase.getObject("Location_Name").click();
        Thread.sleep(2000);
        //Select favourite
        TestBase.getObject("Favourite_Star_Button").click();
        TestBase.getObject("Ok_Button").click();
        Thread.sleep(2000);

        //  Creating Loop to back from the screen
        for (int i = 0; i < 5; i++) {
            TestBase.getObject("Back_Button").click();
        }

        //Again go to set origin screen
        // Click on Option Button
        TestBase.getObject("Option_Button").click();

        //Scroll for Set Origin Option and click on it
        TestBase.driver.scrollTo("Set Origin");
        TestBase.getObject("Set_Origin").click();

        //Go to Favourite screen and open in map view
        TestBase.getObject("Favourite_Options").click();
        TestBase.getObject("Location_Name").click();
        Thread.sleep(1000);

        TestBase.get_Screenshot("src\\test\\resource\\SET_ORIGIN\\C006_Checking_the_Set_Origin_When_Favourites_are_Present/" + "Origin_Flag_SetOrigin_Button" + ".jpg");
        Thread.sleep(1000);
        TestBase.getObject("Set_Origin_Close").click();

        //take a screen shots for Set origin from Favourite screen
        TestBase.get_Screenshot("src\\test\\resource\\SET_ORIGIN\\C006_Checking_the_Set_Origin_When_Favourites_are_Present/" + "Origin_Set_Successfully" + ".jpg");
    }

    @Test(priority = 7)
    public void C007_Checking_the_Set_Origin_From_POI() throws Exception {

        // Click on Option Button
        TestBase.getObject("Option_Button").click();

        //Scroll for Set Origin Option and click on it
        TestBase.driver.scrollTo("Set Origin");
        TestBase.getObject("Set_Origin").click();

        // Click on POI and select any location as Origin
        TestBase.getObject("POI_Options").click();
        TestBase.getObject("Emergency").click();
        TestBase.getObject("All_Emergency").click();
        Thread.sleep(5000);
        TestBase.getObject("Location_Name").click();
        Thread.sleep(2000);

        TestBase.get_Screenshot("src\\test\\resource\\SET_ORIGIN\\C007_Checking_the_Set_Origin_From_POI/" + "Origin_Flag_SetOrigin_Button" + ".jpg");
        TestBase.getObject("Set_Origin_Close").click();

        //Verify Origin set successfully
        TestBase.get_Screenshot("src\\test\\resource\\SET_ORIGIN\\C007_Checking_the_Set_Origin_From_POI/" + "Origin_Set_Successfully" + ".jpg");
    }

    @Test(priority = 8)
    public void C008_Checking_the_Set_Origin_From_Co_Ordinate() throws Exception {

        // Click on Option Button
        TestBase.getObject("Option_Button").click();

        //Scroll for Set Origin Option and click on it
        TestBase.driver.scrollTo("Set Origin");
        TestBase.getObject("Set_Origin").click();

        //Click on "Co-Ordinate" options
        TestBase.getObject("Co-Ordinate_Options").click();

        //Enter Latitude and Logitude for search Location
        TestBase.getObject("Latitude_TextBox").sendKeys(TestBase.TestData.getProperty("Latitude"));
        Thread.sleep(2000);
        TestBase.getObject("Logitide_TextBox").sendKeys(TestBase.TestData.getProperty("Logitude"));

        //Tap on Ok button
        TestBase.getObject("Ok_Button").click();

        //Verify the Origin Flag and Save Origin button
        TestBase.get_Screenshot("src\\test\\resource\\SET_ORIGIN\\C008_Checking_the_Set_Origin_From_Co_Ordinate/" + "Origin_Flag_SetOrigin_Button" + ".jpg");
        Thread.sleep(1000);

        TestBase.getObject("Set_Origin_Close").click();

        TestBase.get_Screenshot("src\\test\\resource\\SET_ORIGIN\\C008_Checking_the_Set_Origin_From_Co_Ordinate/" + "Origin_Set_Successfully" + ".jpg");

    }

    @Test(priority = 9)
    public void C009_Checking_the_Set_Origin_From_Search_Origin() throws Exception {

        // Click on Option Button
        TestBase.getObject("Option_Button").click();

        //Scroll for Set Origin Option and click on it
        TestBase.driver.scrollTo("Set Origin");
        TestBase.getObject("Set_Origin").click();

        //Search Origin
        TestBase.getObject("Search_Origin").sendKeys(TestBase.TestData.getProperty("Origin1"));
        TestBase.getObject("Search_button").click();

        //Select the location and open in map view
        TestBase.getObject("Origin_Location").click();
        //Take a screen shot and verify
        TestBase.get_Screenshot("src\\test\\resource\\SET_ORIGIN\\C009_Checking_the_Set_Origin_From_Search_Origin/" + "Origin_Flag_SetOrigin_Button" + ".jpg");
        Thread.sleep(1000);

        TestBase.getObject("Set_Origin_Close").click();

        TestBase.get_Screenshot("src\\test\\resource\\SET_ORIGIN\\C009_Checking_the_Set_Origin_From_Search_Origin/" + "Origin_Set_Successfully" + ".jpg");
    }

    @Test(priority = 10)
    public void C010_Verify_In_Simulation_Screen_Origin_is_Properly_Set() throws Exception {
        //Go to Recent and Start the simulation
        TestBase.getObject("Search_Button").click();
        TestBase.getObject("Recent_Options").click();
        TestBase.getObject("Origin_Location").click();

        //Start the simulation
        TestBase.getObject("Simulate_Button").click();

        //Verify the Set Origin select popup
        Assert.assertTrue(TestBase.isElementPresent("Set_Origin_Set_Text"), "C010: Custom Origin is set text is not present in the select origin popup");
        Assert.assertTrue(TestBase.isElementPresent("From_Current_Location"), "C010: From_Current_Location text is not present in the select origin popup");
        Assert.assertTrue(TestBase.isElementPresent("From_Custom_Origin"), "C010: From_Custom_Origin text is not present in the select origin popup");

        TestBase.get_Screenshot("src\\test\\resource\\SET_ORIGIN\\C010_Verify_In_Simulation_Screen_Origin_is_Properly_Set/" + "Pop_Up_Simulation_Origin" + ".jpg");

        //Click on "Custom Origin"
        TestBase.getObject("From_Custom_Origin").click();

        //Simulation start successfully using Custom Origin

        TestBase.get_Screenshot("src\\test\\resource\\SET_ORIGIN\\C010_Verify_In_Simulation_Screen_Origin_is_Properly_Set/" + "Simulation_Success_From_Custom_Origin" + ".jpg");
        TestBase.getObject("NavigationScreen_Back").click();
        Thread.sleep(1000);
    }

    @Test(priority = 11)
    public void C011_Verify_The_Set_Origin_From_Preview() throws Exception {
        TestBase.driver.scrollTo("Home");

        //Go to any location for checking 'Preview'
        TestBase.getObject("Search_Button").click();
        TestBase.getObject("Recent_Options").click();
        TestBase.getObject("Origin_Location").click();

        //Verify the Set Origin Preview screen
        TestBase.getObject("Preview_Button").click();

        TestBase.get_Screenshot("src\\test\\resource\\SET_ORIGIN\\C011_Verify_The_Set_Origin_From_Preview/" + "Pop_Up_Preview_Origin" + ".jpg");

        //Verify the Set Origin select popup
        Assert.assertTrue(TestBase.isElementPresent("Set_Origin_Set_Text"), "C011: Custom Origin is set text is not present in the select origin popup");
        Assert.assertTrue(TestBase.isElementPresent("From_Current_Location"), "C011: From_Current_Location text is not present in the select origin popup");
        Assert.assertTrue(TestBase.isElementPresent("From_Custom_Origin"), "C011: From_Custom_Origin text is not present in the select origin popup");

        //Click on "Custom Origin"
        TestBase.getObject("From_Custom_Origin").click();

        TestBase.get_Screenshot("src\\test\\resource\\SET_ORIGIN\\C011_Verify_The_Set_Origin_From_Preview/" + "Preview_Success_From_Custom_Origin" + ".jpg");


        //  Creating Loop to back from the screen
        for (int i = 0; i < 4; i++) {
            TestBase.getObject("Back_Button").click();
        }

        //Clear all the data Functionality Flow:
        TestBase.clear_data();
        TestBase.getObject("Back_Button").click();


    }

        @AfterTest
        public void quit() throws InterruptedException {

            if (TestBase.driver != null)
                Thread.sleep(4000);
            TestBase.driver.quit();
            System.out.println("exit");



    }













    }
