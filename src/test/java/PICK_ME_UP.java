import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

/**
 * Created by QI 33 on 24-06-2016.
 */
public class PICK_ME_UP {


    @Test(priority = 1)
    public void C001_Verify_the_Share_Menu_Icon_After_Open_First_location_In_Map_View_Screen() throws Exception {
        TestBase utill = new TestBase();
        utill.initialize();

        //Select any location and open in map view
        TestBase.getObject("Search_Button").click();
        TestBase.getObject("POI_Options").click();
        TestBase.getObject("Emergency").click();
        TestBase.getObject("All_Emergency").click();
        Thread.sleep(5000);
        TestBase.getObject("Location_Name").click();
        Thread.sleep(2000);

        //Verify the share button and click on it
        Assert.assertTrue(TestBase.isElementPresent("Share_Icon_Menu_Button"), "Share_Icon_Menu_Button is not present in the map view screen");
        TestBase.getObject("Share_Icon_Menu_Button").click();

        TestBase.get_Screenshot("src\\test\\resource\\PICK_ME_UP\\C001_Verify_the_Share_Menu_Icon_After_Open_Any_POI_location_In_Map_View_Screen/" + "Verify_share_Menu_Icon" + ".jpg");

        //Verify all the options present in share popup
        Assert.assertTrue(TestBase.isElementPresent("Pick_Me_Up_Button"), "Pick_Me_Up_Button is not present in the screen");
        Assert.assertTrue(TestBase.isElementPresent("Send_Location_Button"), "Send_Location_Button is not present in the screen");
        Assert.assertTrue(TestBase.isElementPresent("Push2Car_Button"), "Push2Car_Button is not present in the map view screen");
    }

    @Test(priority = 2)
    public void C002_Click_on_Pick_Me_Up_button_and_verify_the_Page() throws Exception {

        Assert.assertTrue(TestBase.isElementPresent("Pick_Me_Up_Button"), "Pick_Me_Up__text is not present in screen");
        TestBase.getObject("Pick_Me_Up_Button").click();
        TestBase.get_Screenshot("src\\test\\resource\\PICK_ME_UP\\C002_Click_on_Pick_Me_Up_button_and_verify_the_Page/" + "Pick_Me_Up_Page_UI" + ".jpg");

        //Verify all the text and options
        Assert.assertTrue(TestBase.isElementPresent("Facebook_Option"), "Facebook_Option  is not present");
        Assert.assertTrue(TestBase.isElementPresent("Email_Options"), "Email_Options  is not present");
        Assert.assertTrue(TestBase.isElementPresent("Copy_Link"), "Copy_Link  is not present");
        Assert.assertTrue(TestBase.isElementPresent("More_Options"), "More_Options  is not present");
        Assert.assertTrue(TestBase.isElementPresent("Done_Button"), "Done_Button  is not present");

        Assert.assertTrue(TestBase.isElementPresent("Ready_Share_Text"), "Ready_Share_Text text is not present");
        Assert.assertTrue(TestBase.isElementPresent("Choose_recipients_text"), "Choose_recipients_text  is not present");

    }

    @Test(priority = 3)
    public void C003_Verify_Pick_Me_Up_functionality_using_Facebook() throws Exception {

        //Click on Facebook Option and make sure Facebook app should be present and setup into the device
        TestBase.getObject("Facebook_Option").click();
        Thread.sleep(5000);

        //Take a screen shots to verify the facebook Pick Me Up options
        TestBase.get_Screenshot("src\\test\\resource\\PICK_ME_UP\\C003_Verify_Pick_Me_Up_functionality_using_Facebook/" + "Facebook_Share_popup" + ".jpg");

        //Click on Post button to post in facebook
        TestBase.getObject("POST_Button_For_Facebook").click();
        Thread.sleep(3000);

        TestBase.get_Screenshot("src\\test\\resource\\PICK_ME_UP\\C003_Verify_Pick_Me_Up_functionality_using_Facebook/" + "toast_message" + ".jpg");
        Thread.sleep(1000);
    }


    @Test(priority = 4)
    public void C004_Verify_Pick_Me_Up_functionality_using_Email() throws Exception {

        //Verify email option
        TestBase.getObject("Email_Options").click();
        Thread.sleep(1000);

        //Verify the email content and details
        TestBase.getObject("Share_email").click();
        try {
            TestBase.getObject("r_gmail").click();
        } catch (Exception e) {

            Thread.sleep(2000);
            if ((TestBase.getObject("PickMeUp_Mail_Body").getText().contains("Pick me up.")) && (TestBase.getObject("PickMeUp_Mail_Body").getText().contains("I'm at http://")) && (TestBase.getObject("PickMeUp_Mail_Body").getText().contains("www.mapsynq.com/sg/"))) {

            } else {
                Assert.assertTrue(1 > 2, "mail body shows wrong content");
            }

            if (TestBase.getObject("Mail_Subject_PickMeUP").getText().contains("Galactio - Pick me up request")) {
                System.out.println("passed");
            } else {
                Assert.assertTrue(1 > 2, "mail subject shows wrong content");
            }

            try {
                Thread.sleep(3000);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }

            TestBase.get_Screenshot("src\\test\\resource\\PICK_ME_UP\\C004_Verify_Pick_Me_Up_functionality_using_Email/" + "screen1" + ".jpg");
        }

        //Enter the valid email id and password and send the email successfully
        TestBase.getObject("To_email_Section").sendKeys("soumik.mazumder@quantuminventions.com");
        TestBase.getObject("Send_button_Email").click();


        //Take a screenshots for successful send message
        TestBase.get_Screenshot("src\\test\\resource\\PICK_ME_UP\\C004_Verify_Pick_Me_Up_functionality_using_Email/" + "Send_message_successful" + ".jpg");

    }

    @Test(priority = 5)
    public void C005_Verify_Pick_Me_Up_functionality_using_Copy_Link() throws Exception {

        //Click on Copy Link
        TestBase.getObject("copy_link").click();
        Thread.sleep(1000);

        TestBase.get_Screenshot("src\\test\\resource\\PICK_ME_UP\\C005_Verify_Pick_Me_Up_functionality_using_Copy_Link/" + "Copy_Link_Verify" + ".jpg");

    }

    @Test(priority = 6)
    public void C006_Verify_Pick_Me_Up_functionality_using_More_Option_verification() throws Exception {
        TestBase.getObject("more").click();
        Thread.sleep(1000);

        TestBase.get_Screenshot("src\\test\\resource\\PICK_ME_UP\\C006_Verify_Pick_Me_Up_functionality_using_More_Option_verification/" + "More_Option" + ".jpg");
        Thread.sleep(2000);
        TestBase.driver.navigate().back();
    }

    @Test(priority = 7)
    public void C007_Click_on_Done_button_and_verify () throws Exception {

        //Click on Done Button
        TestBase.getObject("Done_Button").click();
        Assert.assertFalse(TestBase.isElementPresent("Done_Button"), "Done_Button  is still present");

    }

    @Test(priority = 8)
    public void C008_Verify_Back_Functionality() throws Exception {

        //  Creating Loop to back from the screen
        for (int i = 0; i < 5; i++){
            TestBase.getObject("Back_Button").click();
        }


        //Clear all the data Functionality Flow:
        TestBase.clear_data();
        TestBase.getObject("Back_Button").click();

    }

    @AfterTest
    public void quit() throws InterruptedException {

        if (TestBase.driver != null)
            Thread.sleep(4000);
        TestBase.driver.quit();
        System.out.println("exit");
    }

}



