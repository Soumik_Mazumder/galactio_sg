import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

/**
 * Created by QI 33 on 23-06-2016.
 */
public class POI_FUNCTION {

    @Test(priority = 1)
    public void C001_POI_Search_for_First_Location() throws Exception {

        TestBase utill = new TestBase();
        utill.initialize();

        TestBase.getObject("Search_option").click();

        //Click on POI and POI's search field for Location_1
        TestBase.getObject("POI").click();
        TestBase.getObject("Search_POI_field").click();
        TestBase.getObject("Search_POI_field").sendKeys(TestBase.TestData.getProperty("POI_Location_1"));
        TestBase.getObject("Search_button").click();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C001_POI_Search/" + "POI_Location_1_Search_Results " + ".jpg");
        //Click on first location
        TestBase.getObject("Location_Id").click();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C001_POI_Search/" + "POI_Location_1_MapView_Screen " + ".jpg");
        Assert.assertTrue(TestBase.isElementPresent("Map_View_text"), "C001:Map_View_text is not present in the header of Location_1 map view screen");

        //Clicking zoom out and zoom in button
        for (int i = 0; i < 2; i++) {
            TestBase.getObject("Verify_Zoom_In_button").click();
        }

        for (int i = 0; i <= 9; i++) {
            TestBase.getObject("Verify_Zoom_Out_button").click();
        }

        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        //Click on cross button for clearing the location_1 data
        TestBase.getObject("Clear_Data_button").click();
        TestBase.getObject("Back_button").click();
    }

    @Test(priority = 2)
    public void C002_POI_Search_for_Second_Location() throws Exception {

        //Click on POI search field for accessing the Location_2
        TestBase.getObject("Search_POI_field").sendKeys(TestBase.TestData.getProperty("POI_Location_2"));
        TestBase.getObject("Search_button").click();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C001_POI_Search/" + "POI_Location_1_Search_Results " + ".jpg");
        TestBase.getObject("Location_Id").click();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C001_POI_Search/" + "POI_Location_1_MapView_Screen " + ".jpg");
        Assert.assertTrue(TestBase.isElementPresent("Map_View_text"), "C002:Map_View_text is not present in the header of Location_2 map view screen");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        //Click on cross button for clearing the location_2 data
        TestBase.getObject("Clear_Data_button").click();

        //Click on POI search field for accessing the Location_3
        TestBase.getObject("Search_POI_field").sendKeys(TestBase.TestData.getProperty("POI_Location_3"));
        TestBase.getObject("Search_button").click();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C001_POI_Search/" + "POI_Location_3_Search_Results " + ".jpg");
        TestBase.getObject("Location_Id").click();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C001_POI_Search/" + "POI_Location_3_MapView_Screen " + ".jpg");
        Assert.assertTrue(TestBase.isElementPresent("Map_View_text"), "C003:Map_View_text is not present in the header of Location_3 map view screen");
        //Clicking zoom out and zoom in button
        for (int i = 0; i < 2; i++) {
            TestBase.getObject("Verify_Zoom_In_button").click();
        }
        for (int i = 0; i <= 9; i++) {
            TestBase.getObject("Verify_Zoom_Out_button").click();
        }
        TestBase.getObject("Back_button").click();

    }


    @Test(priority = 3)
    public void C003_Verify_Search_Filter_Button_From_POI_Search() throws Exception {

        TestBase.getObject("Search_Filter_Button").click();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C002_POI_Search/" + "POI_Location_3_Search_filter_Screen " + ".jpg");
        TestBase.getObject("Search_Filter_Close_Button").click();
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
    }

    //Taking screenshot for POI categories list
    @Test(priority = 4)
    public void C004_Verify_the_POI_Categories_ICON() throws Exception {
        TestBase.getObject("POI").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C004_Verify_the_POI_Categories_ICON/" + "POI category List icons for first page " + ".jpg");
        TestBase.driver.scrollTo("Others/Uncategorized");
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C004_Verify_the_POI_Categories_ICON/" + "POI category List icons for Second page " + ".jpg");
        TestBase.driver.scrollTo("Emergency");
    }


    @Test(priority = 5)
    public void C005_Verify_the_POI_Emergency_category_and_Subcategories_List() throws Exception {


        Assert.assertTrue(TestBase.isElementPresent("Emergency_Text"), "C004:Emergency_Text is not present in the POI category screen");
        TestBase.getObject("Emergency_Text").click();
        Assert.assertTrue(TestBase.isElementPresent("All_Emergency_Text"), "C004:All_Emergency_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Hospital"), "C004:Hospital_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Emergency_Police_Station_Text"), "C004:Emergency_Police_Station_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Fire_Station_Text"), "C004:Fire_Station_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("RestRoom_Text"), "C004:RestRoom_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("24_Hrs_Towing_Service_Text"), "C004:24_Hrs_Towing_Service_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Car_Insurance_Company_Text"), "C004:Car_Insurance_Company_Text is not present in the POI subcategory screen");
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C005_Verify_the_POI_Emergency_category_and_Subcategories_List/" + "Emergency Sub_Category List icons" + ".jpg");
    }


    @Test(priority = 6)
    public void C006_Verify_the_Single_location_Map_View_Screen_for_POI_Emergency_SubCategory_List() throws Exception {

        //Verify POI_Emergency first Sub_category option and take screenshot for map view screen
        TestBase.getObject("All_Emergency_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C005_Verify_the_Single_location_Map_View_Screen_for_POI_Emergency_SubCategory_List/" + "All Emergency Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C005_Verify_the_Single_location_Map_View_Screen_for_POI_Emergency_SubCategory_List/" + "All Emergency Category Map view screen" + ".jpg");
        Assert.assertTrue(TestBase.isElementPresent("Map_View_text"), "C001:Map_View_text is not present in the header of POI All_Emergency map view screen");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();


        //Verify POI_Emergency Second Sub_category option and take screenshot for map view screen
        TestBase.getObject("Hospital").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C005_Verify_the_Single_location_Map_View_Screen_for_POI_Emergency_SubCategory_List/" + "Hospital Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C005_Verify_the_Single_location_Map_View_Screen_for_POI_Emergency_SubCategory_List/" + "Hospital Category Map view screen" + ".jpg");
        Assert.assertTrue(TestBase.isElementPresent("Map_View_text"), "C006:Map_View_text is not present in the header of Hospital map view screen");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();


        //Verify POI_Emergency third Sub_category option and take screenshot for map view screen
        TestBase.getObject("Emergency_Police_Station_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C005_Verify_the_Single_location_Map_View_Screen_for_POI_Emergency_SubCategory_List/" + "Emergency_Police_Station_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C005_Verify_the_Single_location_Map_View_Screen_for_POI_Emergency_SubCategory_List/" + "Emergency_Police_Station_Text Category Map view screen" + ".jpg");
        Assert.assertTrue(TestBase.isElementPresent("Map_View_text"), "C006:Map_View_text is not present in the header of Emergency_Police_Station map view screen");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();


        //Verify POI_Emergency fourth Sub_category option and take screenshot for map view screen
        TestBase.getObject("Fire_Station_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C005_Verify_the_Single_location_Map_View_Screen_for_POI_Emergency_SubCategory_List/" + "Fire_Station_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C005_Verify_the_Single_location_Map_View_Screen_for_POI_Emergency_SubCategory_List/" + "Fire_Station_Text Category Map view screen" + ".jpg");
        Assert.assertTrue(TestBase.isElementPresent("Map_View_text"), "C006:Map_View_text is not present in the header of Emergency_Police_Station map view screen");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();


        //Verify POI_Emergency fifth Sub_category option and take screenshot for map view screen
        TestBase.getObject("RestRoom_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C005_Verify_the_Single_location_Map_View_Screen_for_POI_Emergency_SubCategory_List/" + "RestRoom_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C005_Verify_the_Single_location_Map_View_Screen_for_POI_Emergency_SubCategory_List/" + "RestRoom_Text Category Map view screen" + ".jpg");
        Assert.assertTrue(TestBase.isElementPresent("Map_View_text"), "C006:Map_View_text is not present in the header of RestRoom map view screen");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        //Verify POI_Emergency sixth Sub_category option and take screenshot for map view screen
        TestBase.getObject("24_Hrs_Towing_Service_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C005_Verify_the_Single_location_Map_View_Screen_for_POI_Emergency_SubCategory_List/" + "24_Hrs_Towing_Service_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C005_Verify_the_Single_location_Map_View_Screen_for_POI_Emergency_SubCategory_List/" + "24_Hrs_Towing_Service_TextCategory Map view screen" + ".jpg");
        Assert.assertTrue(TestBase.isElementPresent("Map_View_text"), "C006:Map_View_text is not present in the header of 24_Hrs_Towing_Service map view screen");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        //Verify POI_Emergency seventh Sub_category option and take screenshot for map view screen
        TestBase.getObject("Car_Insurance_Company_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C005_Verify_the_Single_location_Map_View_Screen_for_POI_Emergency_SubCategory_List/" + "Car_Insurance_Company_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C005_Verify_the_Single_location_Map_View_Screen_for_POI_Emergency_SubCategory_List/" + "Car_Insurance_Company_Text Category Map view screen" + ".jpg");
        Assert.assertTrue(TestBase.isElementPresent("Map_View_text"), "C006:Map_View_text is not present in the header of Car_Insurance_Company map view screen");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
    }


    @Test(priority = 7)
    public void C007_Verify_the_POI_Restaurant_Category_and_its_Subcategory_List() throws Exception {

        Assert.assertTrue(TestBase.isElementPresent("Restaurant_Text"), "C001:Restaurant_Text is not present in the POI Category List");
        TestBase.getObject("Restaurant_Text").click();
        Assert.assertTrue(TestBase.isElementPresent("All_Restaurant_Text"), "C001:All_Restaurant_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Fast_Food_Text"), "C001:Fast_Food_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Chinese_Text"), "C001:Chinese_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Malay_Text"), "C001:Malay_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Indian_Text"), "C001:Indian_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Indonesian_Text"), "C001:Indonesian_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Vietnamese_Text"), "C001:Thai_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Thai_Text"), "C001:Thai_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Seafood_Text"), "C001:Seafood_Text is not present in the POI subcategory screen");
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C006_Verify_the_POI_Restaurant_Category_and_its_Subcategory_List/" + "Restaurant_sub_category_list_ICONS_1" + ".jpg");
        TestBase.driver.scrollTo("Others");
        Assert.assertTrue(TestBase.isElementPresent("Vegetarian_Text"), "C001:Vegetarian_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Italian_Text"), "C001:Italian_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Cafe_Text"), "C001:Cafe_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Japanese_Text"), "C001:Japanese_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Korean_Text"), "C001:Korean_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("German_Text"), "C001:German_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Restaurant_Text"), "C001:Restaurant_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Restaurant_Others_Text"), "C001:Restaurant_Others_Text is not present in the POI subcategory screen");
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C006_Verify_the_POI_Restaurant_Category_and_its_Subcategory_List/" + "Restaurant_sub_category_list_ICONS_2" + ".jpg");
        TestBase.driver.scrollTo("All Restaurant");
    }


    @Test(priority = 8)
    public void C008_Verify_the_Single_location_Map_View_Screen_for_POI_Restaurant_SubCategory_List() throws Exception {

        //Verify POI_Emergency first Sub_category option and take screenshot for map view screen
        TestBase.getObject("All_Restaurant_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C008_Verify_the_Single_location_Map_View_Screen_for_POI_Restaurant_SubCategory_List/" + "All_Restaurant Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C008_Verify_the_Single_location_Map_View_Screen_for_POI_Restaurant_SubCategory_List/" + "All_Restaurant Category Map view screen" + ".jpg");
        Assert.assertTrue(TestBase.isElementPresent("Map_View_text"), "C008:Map_View_text is not present in the header of All_Restaurant map view screen");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        //Verify POI_Emergency first Sub_category option and take screenshot for map view screen
        TestBase.getObject("Fast_Food_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C008_Verify_the_Single_location_Map_View_Screen_for_POI_Restaurant_SubCategory_List/" + "Fast_Food_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C008_Verify_the_Single_location_Map_View_Screen_for_POI_Restaurant_SubCategory_List/" + "Fast_Food_Text Category Map view screen" + ".jpg");
        Assert.assertTrue(TestBase.isElementPresent("Map_View_text"), "C008:Map_View_text is not present in the header of Fast_Food map view screen");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        //Verify POI_Emergency first Sub_category option and take screenshot for map view screen
        TestBase.getObject("Chinese_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C008_Verify_the_Single_location_Map_View_Screen_for_POI_Restaurant_SubCategory_List/" + "Chinese_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C008_Verify_the_Single_location_Map_View_Screen_for_POI_Restaurant_SubCategory_List/" + "Chinese_Text Category Map view screen" + ".jpg");
        Assert.assertTrue(TestBase.isElementPresent("Map_View_text"), "C008:Map_View_text is not present in the header of Chinese map view screen");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        //Verify POI_Emergency first Sub_category option and take screenshot for map view screen
        TestBase.getObject("Malay_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C008_Verify_the_Single_location_Map_View_Screen_for_POI_Restaurant_SubCategory_List/" + "Malay_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C008_Verify_the_Single_location_Map_View_Screen_for_POI_Restaurant_SubCategory_List/" + "Malay_Text Category Map view screen" + ".jpg");
        Assert.assertTrue(TestBase.isElementPresent("Map_View_text"), "C008:Map_View_text is not present in the header of Malay map view screen");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        //Verify POI_Emergency first Sub_category option and take screenshot for map view screen
        TestBase.getObject("Indian_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C008_Verify_the_Single_location_Map_View_Screen_for_POI_Restaurant_SubCategory_List/" + "Indian_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C008_Verify_the_Single_location_Map_View_Screen_for_POI_Restaurant_SubCategory_List/" + "Indian_Text Category Map view screen" + ".jpg");
        Assert.assertTrue(TestBase.isElementPresent("Map_View_text"), "C008:Map_View_text is not present in the header of Indian map view screen");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        //Verify POI_Emergency first Sub_category option and take screenshot for map view screen
        TestBase.getObject("Indonesian_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C008_Verify_the_Single_location_Map_View_Screen_for_POI_Restaurant_SubCategory_List/" + "Indonesian_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C008_Verify_the_Single_location_Map_View_Screen_for_POI_Restaurant_SubCategory_List/" + "Indonesian_Text Category Map view screen" + ".jpg");
        Assert.assertTrue(TestBase.isElementPresent("Map_View_text"), "C008:Map_View_text is not present in the header of Indonesian map view screen");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        //Verify POI_Emergency first Sub_category option and take screenshot for map view screen
        TestBase.getObject("Vietnamese_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C008_Verify_the_Single_location_Map_View_Screen_for_POI_Restaurant_SubCategory_List/" + "Vietnamese_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C008_Verify_the_Single_location_Map_View_Screen_for_POI_Restaurant_SubCategory_List/" + "Vietnamese_Text Category Map view screen" + ".jpg");
        Assert.assertTrue(TestBase.isElementPresent("Map_View_text"), "C008:Map_View_text is not present in the header of Vietnamese map view screen");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        //Verify POI_Emergency first Sub_category option and take screenshot for map view screen
        TestBase.getObject("Thai_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C008_Verify_the_Single_location_Map_View_Screen_for_POI_Restaurant_SubCategory_List/" + "Thai_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C008_Verify_the_Single_location_Map_View_Screen_for_POI_Restaurant_SubCategory_List/" + "Thai_Text Category Map view screen" + ".jpg");
        Assert.assertTrue(TestBase.isElementPresent("Map_View_text"), "C008:Map_View_text is not present in the header of Thai map view screen");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        //Verify POI_Emergency first Sub_category option and take screenshot for map view screen
        TestBase.getObject("Seafood_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C008_Verify_the_Single_location_Map_View_Screen_for_POI_Restaurant_SubCategory_List/" + "Seafood_TextCategory Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C008_Verify_the_Single_location_Map_View_Screen_for_POI_Restaurant_SubCategory_List/" + "Seafood_Text Category Map view screen" + ".jpg");
        Assert.assertTrue(TestBase.isElementPresent("Map_View_text"), "C008:Map_View_text is not present in the header of Seafood map view screen");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.driver.scrollTo("Others");

        //Verify POI_Emergency first Sub_category option and take screenshot for map view screen
        TestBase.getObject("Vegetarian_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C008_Verify_the_Single_location_Map_View_Screen_for_POI_Restaurant_SubCategory_List/" + "Vegetarian_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C008_Verify_the_Single_location_Map_View_Screen_for_POI_Restaurant_SubCategory_List/" + "Vegetarian_Text Category Map view screen" + ".jpg");
        Assert.assertTrue(TestBase.isElementPresent("Map_View_text"), "C008:Map_View_text is not present in the header of Vegetarian map view screen");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        //Verify POI_Emergency first Sub_category option and take screenshot for map view screen
        TestBase.getObject("Italian_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C008_Verify_the_Single_location_Map_View_Screen_for_POI_Restaurant_SubCategory_List/" + "Italian_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C008_Verify_the_Single_location_Map_View_Screen_for_POI_Restaurant_SubCategory_List/" + "Italian_Text Category Map view screen" + ".jpg");
        Assert.assertTrue(TestBase.isElementPresent("Map_View_text"), "C008:Map_View_text is not present in the header of Italian map view screen");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        //Verify POI_Emergency first Sub_category option and take screenshot for map view screen
        TestBase.getObject("Cafe_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C008_Verify_the_Single_location_Map_View_Screen_for_POI_Restaurant_SubCategory_List/" + "Cafe_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C008_Verify_the_Single_location_Map_View_Screen_for_POI_Restaurant_SubCategory_List/" + "Cafe_Text Category Map view screen" + ".jpg");
        Assert.assertTrue(TestBase.isElementPresent("Map_View_text"), "C008:Map_View_text is not present in the header of Cafe map view screen");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        //Verify POI_Emergency first Sub_category option and take screenshot for map view screen
        TestBase.getObject("Japanese_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C008_Verify_the_Single_location_Map_View_Screen_for_POI_Restaurant_SubCategory_List/" + "Japanese_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C008_Verify_the_Single_location_Map_View_Screen_for_POI_Restaurant_SubCategory_List/" + "Japanese_Text Category Map view screen" + ".jpg");
        Assert.assertTrue(TestBase.isElementPresent("Map_View_text"), "C008:Map_View_text is not present in the header of Japanese map view screen");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        //Verify POI_Emergency first Sub_category option and take screenshot for map view screen
        TestBase.getObject("Korean_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C008_Verify_the_Single_location_Map_View_Screen_for_POI_Restaurant_SubCategory_List/" + "Korean_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C008_Verify_the_Single_location_Map_View_Screen_for_POI_Restaurant_SubCategory_List/" + "Korean_Text Category Map view screen" + ".jpg");
        Assert.assertTrue(TestBase.isElementPresent("Map_View_text"), "C008:Map_View_text is not present in the header of Korean map view screen");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        //Verify POI_Emergency first Sub_category option and take screenshot for map view screen
        TestBase.getObject("German_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C008_Verify_the_Single_location_Map_View_Screen_for_POI_Restaurant_SubCategory_List/" + "German_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C008_Verify_the_Single_location_Map_View_Screen_for_POI_Restaurant_SubCategory_List/" + "German_Text Category Map view screen" + ".jpg");
        Assert.assertTrue(TestBase.isElementPresent("Map_View_text"), "C008:Map_View_text is not present in the header of German map view screen");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        //Verify POI_Emergency first Sub_category option and take screenshot for map view screen
        TestBase.getObject("Restaurant_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C008_Verify_the_Single_location_Map_View_Screen_for_POI_Restaurant_SubCategory_List/" + "Restaurant_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C008_Verify_the_Single_location_Map_View_Screen_for_POI_Restaurant_SubCategory_List/" + "Restaurant_Text Category Map view screen" + ".jpg");
        Assert.assertTrue(TestBase.isElementPresent("Map_View_text"), "C008:Map_View_text is not present in the header of Restaurant map view screen");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        //Verify POI_Emergency first Sub_category option and take screenshot for map view screen
        TestBase.getObject("Restaurant_Others_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C008_Verify_the_Single_location_Map_View_Screen_for_POI_Restaurant_SubCategory_List/" + "Restaurant_Others_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C008_Verify_the_Single_location_Map_View_Screen_for_POI_Restaurant_SubCategory_List/" + "Restaurant_Others_Text Map view screen" + ".jpg");
        Assert.assertTrue(TestBase.isElementPresent("Map_View_text"), "C008:Map_View_text is not present in the header of Restaurant map view screen");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
    }


    @Test(priority = 9)
    public void C009_Verify_the_POI_Fuel_Category_and_its_Subcategory_List() throws Exception {


        Assert.assertTrue(TestBase.isElementPresent("Fuel_Text"), "C001:Fuel_Text is not present in the POI Category List");
        TestBase.getObject("Fuel_Text").click();
        Assert.assertTrue(TestBase.isElementPresent("All_Fuel_Text"), "C001:All_Fuel_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Esso_Text"), "C001:Esso_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Shell_Text"), "C001:Shell_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Caltex_Text"), "C001:Caltex_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("SPC_Text"), "C001:SPC_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("CNG_Station_Text"), "C001:CNG_Station_Text is not present in the POI subcategory screen");
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C008_Verify_the_POI_Category_and_its_Subcategory_List/" + "Fuel_Golf_sub_category_list" + ".jpg");
    }


    @Test(priority = 10)
    public void C010_Verify_the_Single_location_Map_View_Screen_for_POI_Fuel_SubCategory_List() throws Exception {

        TestBase.getObject("All_Fuel_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C008_Verify_the_Single_location_Map_View_Screen_for_POI_Restaurant_SubCategory_List/" + "All_Fuel_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C008_Verify_the_Single_location_Map_View_Screen_for_POI_Restaurant_SubCategory_List/" + "All_Fuel_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Esso_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C008_Verify_the_Single_location_Map_View_Screen_for_POI_Restaurant_SubCategory_List/" + "Esso_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C008_Verify_the_Single_location_Map_View_Screen_for_POI_Restaurant_SubCategory_List/" + "Esso_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Shell_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C008_Verify_the_Single_location_Map_View_Screen_for_POI_Restaurant_SubCategory_List/" + "Shell_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C008_Verify_the_Single_location_Map_View_Screen_for_POI_Restaurant_SubCategory_List/" + "Shell_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Caltex_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C008_Verify_the_Single_location_Map_View_Screen_for_POI_Restaurant_SubCategory_List/" + "Caltex_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C008_Verify_the_Single_location_Map_View_Screen_for_POI_Restaurant_SubCategory_List/" + "Caltex_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("SPC_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C008_Verify_the_Single_location_Map_View_Screen_for_POI_Restaurant_SubCategory_List/" + "SPC_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C008_Verify_the_Single_location_Map_View_Screen_for_POI_Restaurant_SubCategory_List/" + "SPC_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("CNG_Station_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C008_Verify_the_Single_location_Map_View_Screen_for_POI_Restaurant_SubCategory_List/" + "CNG_Station_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C008_Verify_the_Single_location_Map_View_Screen_for_POI_Restaurant_SubCategory_List/" + "CNG_Station_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
    }

    @Test(priority = 11)
    public void C011_Verify_the_POI_Medical_Category_and_its_Subcategory_List() throws Exception {

        Assert.assertTrue(TestBase.isElementPresent("Medical_Text"), "C001:Medical_Text is not present in the POI subcategory screen");
        TestBase.getObject("Medical_Text").click();
        Assert.assertTrue(TestBase.isElementPresent("All_Medical_Text"), "C001:All_Medical_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Hospital_Text"), "C001:Hospital_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Clinic_Text"), "C001:Clinic_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Pharmacy_Text"), "C001:Pharmacy_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Polyclinic_Text"), "C001:Polyclinic_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Vet_Text"), "C001:Vet_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Hospice_Text"), "C001:Hospice_Text is not present in the POI subcategory screen");
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C010_Verify_the_POI_Medical_Category_and_its_Subcategory_List/" + "Medical_Golf_sub_category_list" + ".jpg");
    }


    @Test(priority = 12)
    public void C012_Verify_the_Single_location_Map_View_Screen_for_POI_Fuel_SubCategory_List() throws Exception {

        TestBase.getObject("All_Medical_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C011_Verify_the_Single_location_Map_View_Screen_for_POI_Fuel_SubCategory_List/" + "All_Medical_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C011_Verify_the_Single_location_Map_View_Screen_for_POI_Fuel_SubCategory_List/" + "All_Medical_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Hospital_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C011_Verify_the_Single_location_Map_View_Screen_for_POI_Fuel_SubCategory_List/" + "Hospital_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C011_Verify_the_Single_location_Map_View_Screen_for_POI_Fuel_SubCategory_List/" + "Hospital_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Clinic_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C011_Verify_the_Single_location_Map_View_Screen_for_POI_Fuel_SubCategory_List/" + "Clinic_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C011_Verify_the_Single_location_Map_View_Screen_for_POI_Fuel_SubCategory_List/" + "Clinic_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Pharmacy_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C011_Verify_the_Single_location_Map_View_Screen_for_POI_Fuel_SubCategory_List/" + "Pharmacy_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C011_Verify_the_Single_location_Map_View_Screen_for_POI_Fuel_SubCategory_List/" + "Pharmacy_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Polyclinic_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C011_Verify_the_Single_location_Map_View_Screen_for_POI_Fuel_SubCategory_List/" + "Polyclinic_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C011_Verify_the_Single_location_Map_View_Screen_for_POI_Fuel_SubCategory_List/" + "Polyclinic_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Vet_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C011_Verify_the_Single_location_Map_View_Screen_for_POI_Fuel_SubCategory_List/" + "Vet_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C011_Verify_the_Single_location_Map_View_Screen_for_POI_Fuel_SubCategory_List/" + "Vet_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Hospice_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C011_Verify_the_Single_location_Map_View_Screen_for_POI_Fuel_SubCategory_List/" + "Hospice_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C011_Verify_the_Single_location_Map_View_Screen_for_POI_Fuel_SubCategory_List/" + "Hospice_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
    }


    @Test(priority = 13)
    public void C013_Verify_the_POI_Transport_Category_and_its_Subcategory_List() throws Exception {

        Assert.assertTrue(TestBase.isElementPresent("Transport_Text"), "C001:Transport_Text is not present in the POI category screen");
        TestBase.getObject("Transport_Text").click();
        Assert.assertTrue(TestBase.isElementPresent("All_Transport_Text"), "C001:All_Transport_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Taxi_Stand_Text"), "C001:Taxi_Stand_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("MRT_Station_Text"), "C001:MRT_Station_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Ferry_Terminal_Text"), "C001:Ferry_Terminal_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Airport_Text"), "C001:Airport_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Bus_Station_Text"), "C001:Bus_Station_Text is not present in the POI subcategory screen");
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C012_Verify_the_POI_Transport_Category_and_its_Subcategory_List/" + "Transport_Sub_Category_list ICONS" + ".jpg");
    }

    @Test(priority = 14)
    public void C014_Verify_the_Single_location_Map_View_Screen_for_POI_Transport_SubCategory_List() throws Exception {

        TestBase.getObject("All_Transport_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C013_Verify_the_Single_location_Map_View_Screen_for_POI_Transport_SubCategory_List/" + "All_Transport_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C013_Verify_the_Single_location_Map_View_Screen_for_POI_Transport_SubCategory_List/" + "All_Transport_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Taxi_Stand_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C013_Verify_the_Single_location_Map_View_Screen_for_POI_Transport_SubCategory_List/" + "Taxi_Stand_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C013_Verify_the_Single_location_Map_View_Screen_for_POI_Transport_SubCategory_List/" + "Taxi_Stand_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();


        TestBase.getObject("MRT_Station_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C013_Verify_the_Single_location_Map_View_Screen_for_POI_Transport_SubCategory_List/" + "MRT_Station_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C013_Verify_the_Single_location_Map_View_Screen_for_POI_Transport_SubCategory_List/" + "MRT_Station_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Ferry_Terminal_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C013_Verify_the_Single_location_Map_View_Screen_for_POI_Transport_SubCategory_List/" + "Ferry_Terminal_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C013_Verify_the_Single_location_Map_View_Screen_for_POI_Transport_SubCategory_List/" + "Ferry_Terminal_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Airport_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C013_Verify_the_Single_location_Map_View_Screen_for_POI_Transport_SubCategory_List/" + "Airport_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C013_Verify_the_Single_location_Map_View_Screen_for_POI_Transport_SubCategory_List/" + "Airport_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Bus_Station_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C013_Verify_the_Single_location_Map_View_Screen_for_POI_Transport_SubCategory_List/" + "Bus_Station_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C013_Verify_the_Single_location_Map_View_Screen_for_POI_Transport_SubCategory_List/" + "Bus_Station_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
    }

    @Test(priority = 15)
    public void C015_Verify_the_POI_Transport_Category_and_its_Subcategory_List() throws Exception {

        Assert.assertTrue(TestBase.isElementPresent("Hotel_Text"), "C001:All_Hotel_Text is not present in the POI subcategory screen");
        TestBase.getObject("Hotel_Text").click();
        Assert.assertTrue(TestBase.isElementPresent("All_Hotel_Text"), "C001:All_Hotel_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Hotel"), "C001:Hotel_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Chalet_Text"), "C001:Chalet_Text is not present in the POI subcategory screen");
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C003_Verify_the_POI_Category/" + "Hotel_sub_category_list" + ".jpg");
    }

    @Test(priority = 16)
    public void C016_Verify_the_Single_location_Map_View_Screen_for_POI_Hotel_SubCategory_List() throws Exception {

        TestBase.getObject("All_Hotel_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C015_Verify_the_Single_location_Map_View_Screen_for_POI_Hotel_SubCategory_List/" + "All_Hotel_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C015_Verify_the_Single_location_Map_View_Screen_for_POI_Hotel_SubCategory_List/" + "All_Hotel_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();


        TestBase.getObject("Hotel").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C015_Verify_the_Single_location_Map_View_Screen_for_POI_Hotel_SubCategory_List/" + "Hotel Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C015_Verify_the_Single_location_Map_View_Screen_for_POI_Hotel_SubCategory_List/" + "Hotel Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Chalet_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C015_Verify_the_Single_location_Map_View_Screen_for_POI_Hotel_SubCategory_List/" + "Chalet_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C015_Verify_the_Single_location_Map_View_Screen_for_POI_Hotel_SubCategory_List/" + "Chalet_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
    }

    @Test(priority = 17)
    public void C017_Verify_the_POI_Shopping_Category_and_its_Subcategory_List() throws Exception {

        Assert.assertTrue(TestBase.isElementPresent("Shopping_Text"), "C001:Shopping_Text is not present in the POI subcategory screen");
        TestBase.getObject("Shopping_Text").click();
        Assert.assertTrue(TestBase.isElementPresent("All_Shopping_Text"), "C001:All_ShoppingText is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Shopping_Centre_Text"), "C001:Shopping_Centre_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Shop_Text"), "C001:Shop_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Hypermart_Text"), "C001:Hypermart_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Convenience_Store_Text"), "C001:Convenience_Store_Text is not present in the POI subcategory screen");
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C003_Verify_the_POI_Category/" + "Shopping_sub_category_list" + ".jpg");
    }

    @Test(priority = 18)
    public void C018_Verify_the_Single_location_Map_View_Screen_for_POI_Shopping_SubCategory_List() throws Exception {

        TestBase.getObject("All_Shopping_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C018_Verify_the_Single_location_Map_View_Screen_for_POI_Shopping_SubCategory_List/" + "All_Shopping_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C018_Verify_the_Single_location_Map_View_Screen_for_POI_Shopping_SubCategory_List/" + "Chalet_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Shopping_Centre_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C018_Verify_the_Single_location_Map_View_Screen_for_POI_Shopping_SubCategory_List/" + "Shopping_Centre_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C018_Verify_the_Single_location_Map_View_Screen_for_POI_Shopping_SubCategory_List/" + "Shopping_Centre_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Shop_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C018_Verify_the_Single_location_Map_View_Screen_for_POI_Shopping_SubCategory_List/" + "Shop_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C018_Verify_the_Single_location_Map_View_Screen_for_POI_Shopping_SubCategory_List/" + "Shop_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Hypermart_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C018_Verify_the_Single_location_Map_View_Screen_for_POI_Shopping_SubCategory_List/" + "Hypermart_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C018_Verify_the_Single_location_Map_View_Screen_for_POI_Shopping_SubCategory_List/" + "Hypermart_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Convenience_Store_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C018_Verify_the_Single_location_Map_View_Screen_for_POI_Shopping_SubCategory_List/" + "Convenience_Store_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C018_Verify_the_Single_location_Map_View_Screen_for_POI_Shopping_SubCategory_List/" + "Convenience_Store_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
    }


    @Test(priority = 19)
    public void C019_Verify_the_POI_Financial_Category_and_its_Subcategory_List() throws Exception {


        Assert.assertTrue(TestBase.isElementPresent("Financial_Text"), "C001:Financial_Text is not present in the POI subcategory screen");
        TestBase.getObject("Financial_Text").click();
        Assert.assertTrue(TestBase.isElementPresent("All_Financial_Text"), "C001:All_Financial_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Bank_Text"), "C001:Bank_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("ATM_Text"), "C001:AXS_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("AXS_Text"), "C001:AXS_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("SAM_Text"), "C001:SAM_Text is not present in the POI subcategory screen");
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C018_Verify_the_POI_Shopping_Category_and_its_Subcategory_List/" + "Financial_sub_category_list" + ".jpg");
    }

    @Test(priority = 20)
    public void C020_Verify_the_Single_location_Map_View_Screen_for_POI_Financial_SubCategory_List() throws Exception {


        TestBase.getObject("All_Financial_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C019_Verify_the_Single_location_Map_View_Screen_for_POI_Financial_SubCategory_List/" + "All_Financial_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C019_Verify_the_Single_location_Map_View_Screen_for_POI_Financial_SubCategory_List/" + "All_Financial_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Bank_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C019_Verify_the_Single_location_Map_View_Screen_for_POI_Financial_SubCategory_List/" + "Bank_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C019_Verify_the_Single_location_Map_View_Screen_for_POI_Financial_SubCategory_List/" + "Bank_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("ATM_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C019_Verify_the_Single_location_Map_View_Screen_for_POI_Financial_SubCategory_List/" + "ATM_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C019_Verify_the_Single_location_Map_View_Screen_for_POI_Financial_SubCategory_List/" + "All_Financial_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("AXS_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C019_Verify_the_Single_location_Map_View_Screen_for_POI_Financial_SubCategory_List/" + "AXS_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C019_Verify_the_Single_location_Map_View_Screen_for_POI_Financial_SubCategory_List/" + "AXS_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("SAM_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C019_Verify_the_Single_location_Map_View_Screen_for_POI_Financial_SubCategory_List/" + "SAM_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C019_Verify_the_Single_location_Map_View_Screen_for_POI_Financial_SubCategory_List/" + "SAM_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
    }


    @Test(priority = 21)
    public void C021_Verify_the_POI_Parking_Category_and_its_Subcategory_List() throws Exception {


        Assert.assertTrue(TestBase.isElementPresent("Parking_Text"), "C001:Parking_Text is not present in the POI subcategory screen");
        TestBase.getObject("Parking_Text").click();
        Assert.assertTrue(TestBase.isElementPresent("All_Parking_Text"), "C001:All_Parking_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Ope__Car_Park_Text"), "C001:Ope__Car_Park_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Multi_Storey_Car_Park_Text"), "C001:Multi_Storey_Car_Park_Text is not present in the POI subcategory screen");
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C020_Verify_the_POI_Financial_Category_and_its_Subcategory_List/" + "Parking_sub_category_list" + ".jpg");
    }

    @Test(priority = 22)
    public void C022_Verify_the_Single_location_Map_View_Screen_for_POI_Parking_SubCategory_List() throws Exception {

        TestBase.getObject("All_Parking_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C021_Verify_the_Single_location_Map_View_Screen_for_POI_Parking_SubCategory_List/" + "All_Parking_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C021_Verify_the_Single_location_Map_View_Screen_for_POI_Parking_SubCategory_List/" + "SAM_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Ope__Car_Park_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C021_Verify_the_Single_location_Map_View_Screen_for_POI_Parking_SubCategory_List/" + "Ope__Car_Park_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C021_Verify_the_Single_location_Map_View_Screen_for_POI_Parking_SubCategory_List/" + "Ope__Car_Park_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Multi_Storey_Car_Park_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C021_Verify_the_Single_location_Map_View_Screen_for_POI_Parking_SubCategory_List/" + "Multi_Storey_Car_Park_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C021_Verify_the_Single_location_Map_View_Screen_for_POI_Parking_SubCategory_List/" + "Multi_Storey_Car_Park_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
        TestBase.driver.scrollTo("Others/Uncategorized");
    }


    @Test(priority = 23)
    public void C023_Verify_the_POI_Entertainment_Category_and_its_Subcategory_List() throws Exception {

        Assert.assertTrue(TestBase.isElementPresent("Entertainment_Text"), "C001:Entertainment_Text is not present in the POI subcategory screen");
        TestBase.getObject("Entertainment_Text").click();
        Assert.assertTrue(TestBase.isElementPresent("All_Entertainment_Text"), "C001:All_Entertainment_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Bar/Pub_Text"), "C001:Bar/Pub_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Cinema_Text"), "C001:Cinema_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Karaoke_Text"), "C001:Karaoke_Text is not present in the POI subcategory screen");
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C023_Verify_the_POI_Entertainment_Category_and_its_Subcategory_List/" + "Entertainment_sub_category_list" + ".jpg");
    }

    @Test(priority = 24)
    public void C024_Verify_the_Single_location_Map_View_Screen_for_POI_Entertainment_SubCategory_List() throws Exception {

        TestBase.getObject("All_Entertainment_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C024_Verify_the_Single_location_Map_View_Screen_for_POI_Entertainment_SubCategory_List/" + "All_Parking_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C024_Verify_the_Single_location_Map_View_Screen_for_POI_Entertainment_SubCategory_List/" + "SAM_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Bar/Pub_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C024_Verify_the_Single_location_Map_View_Screen_for_POI_Entertainment_SubCategory_List/" + "Bar/Pub_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C024_Verify_the_Single_location_Map_View_Screen_for_POI_Entertainment_SubCategory_List/" + "Bar/Pub_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Cinema_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C024_Verify_the_Single_location_Map_View_Screen_for_POI_Entertainment_SubCategory_List/" + "Cinema_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C024_Verify_the_Single_location_Map_View_Screen_for_POI_Entertainment_SubCategory_List/" + "Cinema_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Karaoke_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C024_Verify_the_Single_location_Map_View_Screen_for_POI_Entertainment_SubCategory_List/" + "Karaoke_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C024_Verify_the_Single_location_Map_View_Screen_for_POI_Entertainment_SubCategory_List/" + "Karaoke_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
    }

    @Test(priority = 25)
    public void C025_Verify_the_POI_Sports_Golf_Category_and_its_Subcategory_List() throws Exception {

        Assert.assertTrue(TestBase.isElementPresent("Sports_Golf_Text"), "C001:All_Sports_Golf_Text is not present in the POI subcategory screen");
        TestBase.getObject("Sports_Golf_Text").click();
        Assert.assertTrue(TestBase.isElementPresent("All_Sports_Golf_Text"), "C001:All_Sports_Golf_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Golf_Text"), "C001:Golf_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Park_Text"), "C001:Park_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Swimming_Pool_Text"), "C001:Swimming_Pool_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Fitness_Centre_Text"), "C001:Fitness_Centre_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Arena/Track_Text"), "C001:Arena/Track_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Amusement/Theme_Park_Text"), "C001:Amusement/Theme_Park_Text is not present in the POI subcategory screen");
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C025_Verify_the_POI_Sports_Golf_Category_and_its_Subcategory_List/" + "Sports&Golf_sub_category_list" + ".jpg");

    }

    @Test(priority = 26)
    public void C026_Verify_the_Single_location_Map_View_Screen_for_POI_Entertainment_SubCategory_List() throws Exception {

        TestBase.getObject("All_Sports_Golf_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C026_Verify_the_Single_location_Map_View_Screen_for_POI_Entertainment_SubCategory_List/" + "All_Sports_Golf_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C026_Verify_the_Single_location_Map_View_Screen_for_POI_Entertainment_SubCategory_List/" + "All_Sports_Golf_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();


        TestBase.getObject("Golf_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C026_Verify_the_Single_location_Map_View_Screen_for_POI_Entertainment_SubCategory_List/" + "Golf_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C026_Verify_the_Single_location_Map_View_Screen_for_POI_Entertainment_SubCategory_List/" + "Golf_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();


        TestBase.getObject("Park_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C026_Verify_the_Single_location_Map_View_Screen_for_POI_Entertainment_SubCategory_List/" + "Park_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C026_Verify_the_Single_location_Map_View_Screen_for_POI_Entertainment_SubCategory_List/" + "Park_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();


        TestBase.getObject("Swimming_Pool_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C026_Verify_the_Single_location_Map_View_Screen_for_POI_Entertainment_SubCategory_List/" + "Swimming_Pool_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C026_Verify_the_Single_location_Map_View_Screen_for_POI_Entertainment_SubCategory_List/" + "Swimming_Pool_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();


        TestBase.getObject("Fitness_Centre_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C026_Verify_the_Single_location_Map_View_Screen_for_POI_Entertainment_SubCategory_List/" + "Fitness_Centre_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C026_Verify_the_Single_location_Map_View_Screen_for_POI_Entertainment_SubCategory_List/" + "Fitness_Centre_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();


        TestBase.getObject("Arena/Track_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C026_Verify_the_Single_location_Map_View_Screen_for_POI_Entertainment_SubCategory_List/" + "Arena/Track_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C026_Verify_the_Single_location_Map_View_Screen_for_POI_Entertainment_SubCategory_List/" + "Arena/Track_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();


        TestBase.getObject("Amusement/Theme_Park_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C026_Verify_the_Single_location_Map_View_Screen_for_POI_Entertainment_SubCategory_List/" + "Amusement/Theme_Park_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C026_Verify_the_Single_location_Map_View_Screen_for_POI_Entertainment_SubCategory_List/" + "Amusement/Theme_Park_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

    }

    @Test(priority = 27)
    public void C027_Verify_the_POI_Attraction_Category_and_its_Subcategory_List() throws Exception {

        Assert.assertTrue(TestBase.isElementPresent("Attractions_Text"), "C001:Attractions_Text is not present in the POI category screen");
        TestBase.getObject("Attractions_Text").click();
        Assert.assertTrue(TestBase.isElementPresent("All_Attractions_Text"), "C001:All_Attractions_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Hall/Auditorium_Text"), "C001:Hall/Auditorium_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Zoo_Text"), "C001:Zoo_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Museum_Text"), "C001:Museum_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Landmark_Text"), "C001:Landmark_Text is not present in the POI subcategory screen");
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C027_Verify_the_POI_Attraction_Category_and_its_Subcategory_List/" + "Attractions_sub_category_list" + ".jpg");
    }

    @Test(priority = 28)
    public void C028_Verify_the_Single_location_Map_View_Screen_for_POI_Attraction_SubCategory_List() throws Exception {

        TestBase.getObject("All_Attractions_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C028_Verify_the_Single_location_Map_View_Screen_for_POI_Attraction_SubCategory_List/" + "All_Attractions_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C028_Verify_the_Single_location_Map_View_Screen_for_POI_Attraction_SubCategory_List/" + "All_Attractions_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Hall/Auditorium_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C028_Verify_the_Single_location_Map_View_Screen_for_POI_Attraction_SubCategory_List/" + "Hall/Auditorium_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C028_Verify_the_Single_location_Map_View_Screen_for_POI_Attraction_SubCategory_List/" + "Hall/Auditorium_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Zoo_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C028_Verify_the_Single_location_Map_View_Screen_for_POI_Attraction_SubCategory_List/" + "Zoo_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C028_Verify_the_Single_location_Map_View_Screen_for_POI_Attraction_SubCategory_List/" + "Zoo_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Museum_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C028_Verify_the_Single_location_Map_View_Screen_for_POI_Attraction_SubCategory_List/" + "Museum_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C028_Verify_the_Single_location_Map_View_Screen_for_POI_Attraction_SubCategory_List/" + "Museum_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Landmark_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C028_Verify_the_Single_location_Map_View_Screen_for_POI_Attraction_SubCategory_List/" + "Landmark_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C028_Verify_the_Single_location_Map_View_Screen_for_POI_Attraction_SubCategory_List/" + "Landmark_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
    }


    @Test(priority = 29)
    public void C029_Verify_the_POI_Business_Category_and_its_Subcategory_List() throws Exception {


        Assert.assertTrue(TestBase.isElementPresent("Business_Text"), "C001:Business_Text is not present in the POI subcategory screen");
        TestBase.getObject("Business_Text").click();
        Assert.assertTrue(TestBase.isElementPresent("All_Business_Text"), "C001:All_Business_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Commercial_Building_Text"), "C001:Commercial_Building_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Business_Company_Text"), "C001:Business_Company_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Convention_Centre_Text"), "C001:Convention_Centre_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Exhibition_CentreText"), "C001:Exhibition_CentreText is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Industrial_Building_Text"), "C001:Industrial_Building_Text is not present in the POI subcategory screen");
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C028_Verify_the_POI_Business_Category_and_its_Subcategory_List/" + "Car_Services_Category_sub_category_list" + ".jpg");

    }


    @Test(priority = 30)
    public void C030_Verify_the_Single_location_Map_View_Screen_for_POI_Business_SubCategory_List() throws Exception {

        TestBase.getObject("All_Business_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C030_Verify_the_Single_location_Map_View_Screen_for_POI_Business_SubCategory_List/" + "All_Business_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C030_Verify_the_Single_location_Map_View_Screen_for_POI_Business_SubCategory_List/" + "All_Business_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Commercial_Building_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C030_Verify_the_Single_location_Map_View_Screen_for_POI_Business_SubCategory_List/" + "Commercial_Building_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C030_Verify_the_Single_location_Map_View_Screen_for_POI_Business_SubCategory_List/" + "Commercial_Building_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Business_Company_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C030_Verify_the_Single_location_Map_View_Screen_for_POI_Business_SubCategory_List/" + "Business_Company_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C030_Verify_the_Single_location_Map_View_Screen_for_POI_Business_SubCategory_List/" + "Business_Company_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Convention_Centre_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C030_Verify_the_Single_location_Map_View_Screen_for_POI_Business_SubCategory_List/" + "Convention_Centre_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C030_Verify_the_Single_location_Map_View_Screen_for_POI_Business_SubCategory_List/" + "Convention_Centre_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Exhibition_CentreText").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C030_Verify_the_Single_location_Map_View_Screen_for_POI_Business_SubCategory_List/" + "Exhibition_CentreText Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C030_Verify_the_Single_location_Map_View_Screen_for_POI_Business_SubCategory_List/" + "Exhibition_CentreText Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Industrial_Building_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C030_Verify_the_Single_location_Map_View_Screen_for_POI_Business_SubCategory_List/" + "Industrial_Building_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C030_Verify_the_Single_location_Map_View_Screen_for_POI_Business_SubCategory_List/" + "Industrial_Building_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

    }

    @Test(priority = 31)
    public void C031_Verify_the_POI_Community_Category_and_its_Subcategory_List() throws Exception {

        Assert.assertTrue(TestBase.isElementPresent("Community_Text"), "C001:Community_Text is not present in the POI category screen");
        TestBase.getObject("Community_Text").click();
        Assert.assertTrue(TestBase.isElementPresent("Government_Office_Text"), "C001:Government_Office_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Library_Text"), "C001:Library_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("School_Text"), "C001:School_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Embassy_Text"), "C001:Embassy_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Post_Office_Text"), "C001:Post_Office_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Utility_Text"), "C001:Utility_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Courthouse_Text"), "C001:Courthouse_Text is not present in the POI subcategory screen");
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C031_Verify_the_POI_Community_Category_and_its_Subcategory_List/" + "Community_Category_sub_category_list_1 " + ".jpg");
        TestBase.driver.scrollTo("Fire Department");
        Assert.assertTrue(TestBase.isElementPresent("Community_Police_Station_Text"), "C001:Community_Police_Station_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Fire_Department_Text"), "C001:Fire_Department_Text is not present in the POI subcategory screen");
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C031_Verify_the_POI_Community_Category_and_its_Subcategory_List/" + "Community_Category_sub_category_list_2 " + ".jpg");
    }

    @Test(priority = 32)
    public void C032_Verify_the_Single_location_Map_View_Screen_for_POI_Community_SubCategory_List() throws Exception {

        TestBase.getObject("Government_Office_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C032_Verify_the_Single_location_Map_View_Screen_for_POI_Community_SubCategory_List/" + "Government_Office_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C032_Verify_the_Single_location_Map_View_Screen_for_POI_Community_SubCategory_List/" + "Government_Office_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Library_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C032_Verify_the_Single_location_Map_View_Screen_for_POI_Community_SubCategory_List/" + "Library_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C032_Verify_the_Single_location_Map_View_Screen_for_POI_Community_SubCategory_List/" + "Library_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("School_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C032_Verify_the_Single_location_Map_View_Screen_for_POI_Community_SubCategory_List/" + "School_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C032_Verify_the_Single_location_Map_View_Screen_for_POI_Community_SubCategory_List/" + "School_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Embassy_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C032_Verify_the_Single_location_Map_View_Screen_for_POI_Community_SubCategory_List/" + "Embassy_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C032_Verify_the_Single_location_Map_View_Screen_for_POI_Community_SubCategory_List/" + "Embassy_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Post_Office_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C032_Verify_the_Single_location_Map_View_Screen_for_POI_Community_SubCategory_List/" + "Post_Office_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C032_Verify_the_Single_location_Map_View_Screen_for_POI_Community_SubCategory_List/" + "Post_Office_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Utility_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C032_Verify_the_Single_location_Map_View_Screen_for_POI_Community_SubCategory_List/" + "Utility_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C032_Verify_the_Single_location_Map_View_Screen_for_POI_Community_SubCategory_List/" + "Utility_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Courthouse_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C032_Verify_the_Single_location_Map_View_Screen_for_POI_Community_SubCategory_List/" + "Courthouse_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C032_Verify_the_Single_location_Map_View_Screen_for_POI_Community_SubCategory_List/" + "Courthouse_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.driver.scrollTo("Fire Department");

        TestBase.getObject("Community_Police_Station_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C032_Verify_the_Single_location_Map_View_Screen_for_POI_Community_SubCategory_List/" + "Community_Police_Station_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C032_Verify_the_Single_location_Map_View_Screen_for_POI_Community_SubCategory_List/" + "Community_Police_Station_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();


        TestBase.getObject("Fire_Department_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C032_Verify_the_Single_location_Map_View_Screen_for_POI_Community_SubCategory_List/" + "Fire_Department_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C032_Verify_the_Single_location_Map_View_Screen_for_POI_Community_SubCategory_List/" + "Fire_Department_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

    }

    @Test(priority = 33)
    public void C033_Verify_the_POI_Car_Services_Category_and_its_Subcategory_List() throws Exception {

        Assert.assertTrue(TestBase.isElementPresent("Car_Services_Text"), "C001:All_Car_Services_Text is not present in the POI category screen");
        TestBase.getObject("Car_Services_Text").click();
        Assert.assertTrue(TestBase.isElementPresent("All_Car_Services_Text"), "C001:All_Car_Services_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Car_Dealer_Text"), "C001:Car_Dealer_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Car_Repair_Facility_Text"), "C001:Car_Repair_Facility_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Tyres/Batteries_Services/Accessories_Text"), "C001:Tyres/Batteries_Services/Accessories_Text is not present in the POI subcategory screen");
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C033_Verify_the_POI_Car_Services_Category_and_its_Subcategory_List/" + "Car_Services_Category_sub_category_list " + ".jpg");
    }

    @Test(priority = 34)
    public void C034_Verify_the_Single_location_Map_View_Screen_for_POI_Car_Services_SubCategory_List() throws Exception {


        TestBase.getObject("All_Car_Services_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C034_Verify_the_Single_location_Map_View_Screen_for_POI_Car_Services_SubCategory_List/" + "All_Car_Services_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C034_Verify_the_Single_location_Map_View_Screen_for_POI_Car_Services_SubCategory_List/" + "All_Car_Services_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Car_Dealer_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C034_Verify_the_Single_location_Map_View_Screen_for_POI_Car_Services_SubCategory_List/" + "Car_Dealer_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C034_Verify_the_Single_location_Map_View_Screen_for_POI_Car_Services_SubCategory_List/" + "Car_Dealer_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Car_Repair_Facility_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C034_Verify_the_Single_location_Map_View_Screen_for_POI_Car_Services_SubCategory_List/" + "Car_Repair_Facility_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C034_Verify_the_Single_location_Map_View_Screen_for_POI_Car_Services_SubCategory_List/" + "Car_Repair_Facility_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Tyres/Batteries_Services/Accessories_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C034_Verify_the_Single_location_Map_View_Screen_for_POI_Car_Services_SubCategory_List/" + "Tyres/Batteries_Services/Accessories_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C034_Verify_the_Single_location_Map_View_Screen_for_POI_Car_Services_SubCategory_List/" + "Tyres/Batteries_Services/Accessories_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
    }


    @Test(priority = 35)
    public void C035_Verify_the_POI_Car_Services_Category_and_its_Subcategory_List() throws Exception {

        Assert.assertTrue(TestBase.isElementPresent("Cities_Text"), "C001:Cities_Text is not present in the POI category screen");
        TestBase.getObject("Cities_Text").click();
        Assert.assertTrue(TestBase.isElementPresent("All_Cities_Text"), "C001:All_Cities_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Major_Cities_Text"), "C001:Major_Cities_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Town_Text"), "C001:Town_Text is not present in the POI subcategory screen");
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C035_Verify_the_POI_Car_Services_Category_and_its_Subcategory_List/" + "Cities_Category_sub_category_list " + ".jpg");
    }

    @Test(priority = 36)
    public void C036_Verify_the_Single_location_Map_View_Screen_for_POI_Car_Services_SubCategory_List() throws Exception {


        TestBase.getObject("All_Cities_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C036_Verify_the_Single_location_Map_View_Screen_for_POI_Car_Services_SubCategory_List/" + "All_Cities_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C036_Verify_the_Single_location_Map_View_Screen_for_POI_Car_Services_SubCategory_List/" + "All_Cities_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Major_Cities_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C036_Verify_the_Single_location_Map_View_Screen_for_POI_Car_Services_SubCategory_List/" + "Major_Cities_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C036_Verify_the_Single_location_Map_View_Screen_for_POI_Car_Services_SubCategory_List/" + "Major_Cities_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Town_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C036_Verify_the_Single_location_Map_View_Screen_for_POI_Car_Services_SubCategory_List/" + "Town_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C036_Verify_the_Single_location_Map_View_Screen_for_POI_Car_Services_SubCategory_List/" + "Town_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
    }

    @Test(priority = 37)
    public void C037_Verify_the_POI_Religion_Category_and_its_Subcategory_List() throws Exception {


        Assert.assertTrue(TestBase.isElementPresent("Religion_Text"), "C001:Religion_Text is not present in the POI subcategory screen");
        TestBase.getObject("Religion_Text").click();
        Assert.assertTrue(TestBase.isElementPresent("All_Religion_Text"), "C001:All_Religion_Tex is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Shrine_Text"), "C001:Shrine_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Church_Text"), "C001:Church_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Mosque_Text"), "C001:Mosque_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Temple_Text"), "C001:Temple_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Sikhism_Text"), "C001:Sikhism_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Religion_Others_Text"), "C001:Religion_Others_Text is not present in the POI subcategory screen");
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C037_Verify_the_POI_Religion_Category_and_its_Subcategory_List/" + "Religion_Category_sub_category_list " + ".jpg");
    }

    @Test(priority = 38)
    public void C038_Verify_the_Single_location_Map_View_Screen_for_POI_Religion_SubCategory_List() throws Exception {


        TestBase.getObject("All_Religion_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C038_Verify_the_Single_location_Map_View_Screen_for_POI_Religion_SubCategory_List/" + "All_Religion_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C038_Verify_the_Single_location_Map_View_Screen_for_POI_Religion_SubCategory_List/" + "All_Religion_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Shrine_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C038_Verify_the_Single_location_Map_View_Screen_for_POI_Religion_SubCategory_List/" + "Shrine_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C038_Verify_the_Single_location_Map_View_Screen_for_POI_Religion_SubCategory_List/" + "Shrine_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();


        TestBase.getObject("Church_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C038_Verify_the_Single_location_Map_View_Screen_for_POI_Religion_SubCategory_List/" + "Church_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C038_Verify_the_Single_location_Map_View_Screen_for_POI_Religion_SubCategory_List/" + "Church_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Mosque_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C038_Verify_the_Single_location_Map_View_Screen_for_POI_Religion_SubCategory_List/" + "Mosque_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C038_Verify_the_Single_location_Map_View_Screen_for_POI_Religion_SubCategory_List/" + "Mosque_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Temple_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C038_Verify_the_Single_location_Map_View_Screen_for_POI_Religion_SubCategory_List/" + "Temple_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C038_Verify_the_Single_location_Map_View_Screen_for_POI_Religion_SubCategory_List/" + "Temple_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Sikhism_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C038_Verify_the_Single_location_Map_View_Screen_for_POI_Religion_SubCategory_List/" + "Sikhism_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C038_Verify_the_Single_location_Map_View_Screen_for_POI_Religion_SubCategory_List/" + "Sikhism_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Religion_Others_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C038_Verify_the_Single_location_Map_View_Screen_for_POI_Religion_SubCategory_List/" + "Religion_Others_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C038_Verify_the_Single_location_Map_View_Screen_for_POI_Religion_SubCategory_List/" + "Religion_Others_Text Map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
    }

    @Test(priority = 39)
    public void C039_Verify_the_POI_Others_Uncategorized_Text_Category_and_its_Subcategory_List() throws Exception {


        Assert.assertTrue(TestBase.isElementPresent("Others/Uncategorized_Text"), "C001:Others/Uncategorized_Text is not present in the POI category screen");
        TestBase.getObject("Others_Uncategorized_Text").click();
        Assert.assertTrue(TestBase.isElementPresent("All_Others/Uncategorized_Text"), "C001:All_Others/Uncategorized_Text is not present in the POI subcategory screen");
        Assert.assertTrue(TestBase.isElementPresent("Toto/4D_Text"), "C001:Toto/4D_Text is not present in the POI category screen");
        Assert.assertTrue(TestBase.isElementPresent("Uncategorized_Others_Text"), "C001:Uncategorized_Others_Text is not present in the POI subcategory screen");
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C039_Verify_the_POI_Others_Uncategorized_Text_Category_and_its_Subcategory_List/" + "Others_Category_sub_category_list " + ".jpg");
    }

    @Test(priority = 40)
    public void C040_Verify_the_Single_location_Map_View_Screen_for_POI_Others_Uncategorized_Text_SubCategory_List() throws Exception {

        TestBase.getObject("All_Others/Uncategorized_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C040_Verify_the_Single_location_Map_View_Screen_for_POI_Others_Uncategorized_Text_SubCategory_List/" + "Others/Uncategorized_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();

        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C040_Verify_the_Single_location_Map_View_Screen_for_POI_Others_Uncategorized_Text_SubCategory_List/" + "Others/Uncategorized_Text Map view screen" + ".jpg");
        Assert.assertTrue(TestBase.isElementPresent("Map_View_text"), "C040:Map_View_text is not present in the header of POI Others/Uncategorized map view screen");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
        Thread.sleep(2000);

        TestBase.getObject("Toto/4D_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C040_Verify_the_Single_location_Map_View_Screen_for_POI_Others_Uncategorized_Text_SubCategory_List/" + "Toto/4D_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C040_Verify_the_Single_location_Map_View_Screen_for_POI_Others_Uncategorized_Text_SubCategory_List/" + "Toto/4D_Text Map view screen" + ".jpg");
        Assert.assertTrue(TestBase.isElementPresent("Map_View_text"), "C001:Map_View_text is not present in the header of POI Toto/4D map view screen");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
        Thread.sleep(2000);

        TestBase.getObject("Uncategorized_Others_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C040_Verify_the_Single_location_Map_View_Screen_for_POI_Others_Uncategorized_Text_SubCategory_List/" + "Uncategorized_Others_Text Category Results" + ".jpg");
        TestBase.getObject("Location_Id").click();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\POI\\C040_Verify_the_Single_location_Map_View_Screen_for_POI_Others_Uncategorized_Text_SubCategory_List/" + "Uncategorized_Others_Text Map view screen" + ".jpg");
        Assert.assertTrue(TestBase.isElementPresent("Map_View_text"), "C001:Map_View_text is not present in the header of POI Uncategorized_Others map view screen");
       for(int i=0;i<=4;i++){
           TestBase.getObject("Back_button").click();
       }
        TestBase.clear_data();
    }

    @AfterTest
    public void quit(){

        if((TestBase.driver)!=null){
            try {
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        TestBase.driver.quit();
        System.out.println("exit");
    }

}
