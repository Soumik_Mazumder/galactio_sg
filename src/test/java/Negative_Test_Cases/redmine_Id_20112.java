package Negative_Test_Cases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static Negative_Test_Cases.TestBase.driver;

/**
 * Created by Soumik Mazumder on 9/2/2016.
 */
public class redmine_Id_20112 {


    SoftAssert softAssert = new SoftAssert();

    @Test(priority = 1)
    public void C19160_Dash_sign_is_being_displayed_before_a_HDB_car_park_address_in_the_Parking_screen() throws Exception {
        TestBase utill = new TestBase();
        utill.initialize();

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }

        // Click on Option Button
        TestBase.getObject("Option_Button").click();

        //Scroll to Parking Option
        TestBase.driver.scrollTo("Parking");

        TestBase.getObject("Verify_Parking_Button").click();
        Thread.sleep(5000);

        //Search the location
        TestBase.getObject("Search_Button_Parking").click();
        TestBase.getObject("Search_Text").sendKeys(TestBase.TestData.getProperty("Location_Dash"));

        TestBase.getObject("Search_Button_Parking").click();
        Thread.sleep(2000);

        TestBase.get_Screenshot("src\\test\\resource\\redmine_Id_20112\\C19160_Dash_sign_is_being_displayed_before_a_HDB_car_park_address/" + "UI_Verification" + ".jpg");

    }


    @AfterTest
    public void quit() throws InterruptedException {

        if (driver != null)
            Thread.sleep(4000);
        driver.quit();
        System.out.println("exit");
    }
}
