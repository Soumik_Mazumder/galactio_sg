package Negative_Test_Cases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static Negative_Test_Cases.TestBase.driver;

/**
 * Created by Soumik Mazumder on 9/5/2016.
 */
public class redmine_Id_10384 {

    SoftAssert softAssert = new SoftAssert();

    @Test(priority = 1)
    public void C19174_Displays_No_Results_Found_if_search_suggestion_has_two_Bukit() throws Exception {
        TestBase utill = new TestBase();
        utill.initialize();

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }


        TestBase.getObject("Search_Button").click();
        //Click on search destination field and entering location1
        TestBase.getObject("Search_destination_edit_field").click();
        TestBase.getObject("Search_destination_edit_field").sendKeys(TestBase.TestData.getProperty("Bukit"));
        TestBase.getObject("Search_button").click();

        //Take a screen and verify
        TestBase.get_Screenshot("src\\test\\resource\\redmine_Id_10384\\C19174_Displays_No_Results_Found_if_search_suggestion_has_two_Bukit/" + "Result_pAge_Check" + ".jpg");

    }


    @AfterTest
    public void quit() throws InterruptedException {

        if (driver != null)
            Thread.sleep(4000);
        driver.quit();
        System.out.println("exit");
    }
}
