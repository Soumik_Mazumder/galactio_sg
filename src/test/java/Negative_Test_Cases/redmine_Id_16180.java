package Negative_Test_Cases;

import io.appium.java_client.TouchAction;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static Negative_Test_Cases.TestBase.driver;

/**
 * Created by Soumik Mazumder on 9/5/2016.
 */
public class redmine_Id_16180 {

    SoftAssert softAssert = new SoftAssert();

    @Test(priority = 1)
    public void C19164_Favourites_page_not_updating_properly_after_Delete_or_Edit_the_favourite() throws Exception {
        TestBase utill = new TestBase();
        utill.initialize();

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }

        //Clear all the data Functionality Flow:
        TestBase.clear_data();
        TestBase.getObject("Back_Button").click();

        //Open any Location in Map View and Select Favourite for Recent & Favourite Page
        TestBase.getObject("Search_Button").click();
        TestBase.getObject("POI_Options").click();
        TestBase.getObject("Emergency").click();
        TestBase.getObject("All_Emergency").click();
        Thread.sleep(5000);
        TestBase.getObject("Location_Name").click();
        Thread.sleep(2000);
        //Select favourite
        TestBase.getObject("Favourite_Star_Button").click();
        TestBase.getObject("Ok_Button").click();
        Thread.sleep(2000);
        //Click on Back to dashboard
        TestBase.getObject("Back_Button").click();
        TestBase.getObject("Back_Button").click();
        TestBase.getObject("Back_Button").click();
        TestBase.getObject("Back_Button").click();
        TestBase.getObject("Back_Button").click();

        //Go to favourite screen and verify
        TestBase.getObject("Search_Button").click();
        TestBase.getObject("Favourite_Options").click();


        //Long press on the the Favourite location for Edit and delete
        TouchAction action = new TouchAction(TestBase.driver);
        action.longPress(TestBase.getObject("Fav_Location")).perform();
        Thread.sleep(5000);

        //Edit the Location name
        TestBase.getObject("Edit_Favourites_Button").click();
        TestBase.getObject("Edit_Field").click();
        TestBase.getObject("Edit_Field").clear();
        TestBase.getObject("Edit_Field").sendKeys(TestBase.TestData.getProperty("Edit_Favourite_name1"));
        TestBase.getObject("Ok_Button").click();
        Thread.sleep(1000);

        //Verify the location is edited succesfully
        softAssert.assertTrue(TestBase.isElementPresent("Edit_Favourite_name19"), "Favourite Edition successfully");
        Thread.sleep(1000);



        //Long press on the the Favourite location for Edit and delete
        TouchAction action1 = new TouchAction(TestBase.driver);
        action.longPress(TestBase.getObject("Edit_Favourite_name19")).perform();
        Thread.sleep(5000);

        TestBase.getObject("Verify_Delete_Favourite_Text").click();
        TestBase.getObject("Ok_Button").click();

        softAssert.assertFalse(TestBase.isElementPresent("Edit_Favourite_name19"), "Favourite Not present in the screen");

    }

    @AfterTest
    public void quit() throws InterruptedException {

        if (driver != null)
            Thread.sleep(4000);
        driver.quit();
        System.out.println("exit");
    }
}
