package Negative_Test_Cases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static Negative_Test_Cases.TestBase.driver;

/**
 * Created by Soumik Mazumder on 9/1/2016.
 */
public class redmine_Id_20864 {

    SoftAssert softAssert = new SoftAssert();

    @Test(priority = 1)
    public void C19064_Parking_availability_information_display_box_is_overlapping_with_the_address_of_car_park_in_Parking_View_screen_of_few_Car_Park() throws Exception {
        TestBase utill = new TestBase();
        utill.initialize();

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }

        // Click on Option Button
        TestBase.getObject("Option_Button").click();

        //Scroll to Parking Option
        TestBase.driver.scrollTo("Parking");

        TestBase.getObject("Verify_Parking_Button").click();
        Thread.sleep(5000);

        //Search the location
        TestBase.getObject("Search_Button_Parking").click();
        TestBase.getObject("Search_Text").sendKeys(TestBase.TestData.getProperty("Location1"));

        TestBase.getObject("Search_Button_Parking").click();
        Thread.sleep(2000);
        TestBase.getObject("Position_Parking").click();
        Thread.sleep(3000);

        //Verify the UI overlap with the Parking details
        TestBase.get_Screenshot("src\\test\\resource\\redmine_Id_20864\\PARKING_ISSUE_UI/" + "UI_Check_1" + ".jpg");


        //Verify with the 2nd location
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        //Click on Cross button
        TestBase.getObject("Cross_Button").click();

        //Type the 2nd location Name
        TestBase.getObject("Search_Text").sendKeys(TestBase.TestData.getProperty("Location2"));
        TestBase.getObject("Search_Button_Parking").click();

        Thread.sleep(2000);
        TestBase.getObject("Position_Parking").click();
        Thread.sleep(3000);

        //Verify the UI overlap with the Parking details
        TestBase.get_Screenshot("src\\test\\resource\\redmine_Id_20864\\PARKING_ISSUE_UI/" + "UI_Check_2" + ".jpg");





    }

    @AfterTest
    public void quit() throws InterruptedException {

        if (driver != null)
            Thread.sleep(4000);
        driver.quit();
        System.out.println("exit");
    }
}
