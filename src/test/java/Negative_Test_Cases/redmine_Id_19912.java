package Negative_Test_Cases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static Negative_Test_Cases.TestBase.driver;

/**
 * Created by Soumik Mazumder on 9/5/2016.
 */
public class redmine_Id_19912 {

    SoftAssert softAssert = new SoftAssert();

    @Test(priority = 1)
    public void C19148_Search_result_number_coming_up_to_the_left_of_the_search_field_and_also_not_going_away_on_deleting_the_search_words() throws Exception {
        TestBase utill = new TestBase();
        utill.initialize();

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }

        TestBase.getObject("Search_option").click();

        //Verify the
        TestBase.getObject("Search_destination_edit_field").click();
        TestBase.getObject("Search_destination_edit_field").sendKeys(TestBase.TestData.getProperty("Location9"));
        Thread.sleep(2000);
        softAssert.assertTrue(TestBase.isElementPresent("Count"), "Result Count is not present");

        //Click on cross button and delete the search text
        TestBase.getObject("Cross_Button").click();
        Thread.sleep(2000);
        softAssert.assertFalse(TestBase.isElementPresent("Count"), "Result Count is still present");
    }

    @AfterTest
    public void quit() throws InterruptedException {

        if (driver != null)
            Thread.sleep(4000);
        driver.quit();
        System.out.println("exit");
    }
}
