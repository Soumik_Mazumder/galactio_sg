package Negative_Test_Cases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static Negative_Test_Cases.TestBase.driver;

/**
 * Created by Soumik Mazumder on 9/5/2016.
 */
public class redmine_Id_10385 {

    SoftAssert softAssert = new SoftAssert();

    @Test(priority = 1)
    public void C19172_During_Search_clicking_search_icon_twice_Screen_goes_to_GlobalSearch_Instead_of_SearchLoading_icon() throws Exception {
        TestBase utill = new TestBase();
        utill.initialize();

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }

        TestBase.getObject("Search_Button").click();
        //Click on search destination field andd entering location1
        TestBase.getObject("Search_destination_edit_field").click();
        TestBase.getObject("Search_destination_edit_field").sendKeys(TestBase.TestData.getProperty("Search_Location"));
        TestBase.getObject("Search_button").click();
       // TestBase.getObject("Search_button").click();
        Thread.sleep(3000);
        softAssert.assertTrue(TestBase.isElementPresent("Result_Text"), "Result screen is not present after search");

    }


    @AfterTest
    public void quit() throws InterruptedException {

        if (driver != null)
            Thread.sleep(4000);
        driver.quit();
        System.out.println("exit");
    }
}
