package Negative_Test_Cases;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

/**
 * Created by Soumik Mazumder on 8/23/2016.
 */
public class redmine_Id_31887 {
    @Test(priority = 1)
    public void C18511_Blank_Location_is_being_displayed_under_destination_in_Route_preview_screen_for_a_location_with_no_name() throws Exception {

        SoftAssert softAssert = new SoftAssert();
        TestBase utill = new TestBase();
        utill.initialize();
        Thread.sleep(2000);

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }


        //Open any location in map view
        TestBase.getObject("Search_Button").click();
        TestBase.getObject("POI_Options").click();
        TestBase.getObject("Emergency").click();
        TestBase.getObject("All_Emergency").click();
        Thread.sleep(3000);
        TestBase.getObject("Location_Name").click();
        Thread.sleep(1000);

        //Click on "Share Position" option in map view
        //Verify the share button and click on it
        softAssert.assertTrue(TestBase.isElementPresent("Share_Icon_Menu_Button"), "Share_Icon_Menu_Button not present");
        TestBase.getObject("Share_Icon_Menu_Button").click();
        Thread.sleep(1000);

        //Take a screen shots to verify the popup
        TestBase.get_Screenshot("src\\test\\resource\\redmine_Id_31887\\C18511_Blank_Location_is_being_displayed_under_destination_in_Route_preview_screen_for_a_location_with_no_name/" + "Verify_share_Menu_popup" + ".jpg");

        // Verify all the Share options
        softAssert.assertTrue(TestBase.isElementPresent("Send_Location_Button"), "Send_Location_Button not present");
        softAssert.assertTrue(TestBase.isElementPresent("Pick_Me_Up_Button"), "Pick_Me_Up_Button not present");
        softAssert.assertTrue(TestBase.isElementPresent("Push2Car_Button"), "Push2Car_Button not present");

        //Click on back
        //  Creating Loop to back from the screen
        for (int i = 0; i < 6; i++) {
            TestBase.driver.navigate().back();
        }

        softAssert.assertAll();


    }

    @AfterTest
    public void quit() throws InterruptedException {

        if (TestBase.driver != null)
            Thread.sleep(4000);
        TestBase.driver.quit();
        System.out.println("exit");
    }


    }
