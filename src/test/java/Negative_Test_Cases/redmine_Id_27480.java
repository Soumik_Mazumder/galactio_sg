package Negative_Test_Cases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

/**
 * Created by Soumik Mazumder on 8/24/2016.
 */
public class redmine_Id_27480 {
    SoftAssert softAssert = new SoftAssert();


    @Test(priority = 1)
    public void c18682_Parking_option_Crash_issue() throws Exception {

        TestBase utill = new TestBase();
        utill.initialize();


        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }


        // Click on Option Button
        TestBase.getObject("Option_Button").click();

        //Scroll to Parking Option
        TestBase.driver.scrollTo("Parking");
        TestBase.getObject("Verify_Parking_Button").click();
        Thread.sleep(5000);

        //Go to search and open the location in map view
        TestBase.getObject("Search_Button_Parking").click();
        TestBase.getObject("Search_Text").sendKeys(TestBase.TestData.getProperty("Search_Parking"));
        TestBase.getObject("Search_Button_Parking").click();
        Thread.sleep(5000);


        //Take a screen shots and verify
        TestBase.get_Screenshot("src\\test\\resource\\redmine_Id_27480\\c18682_Parking_option_Crash_issue/" + "Search_Result_No_Crash" + ".jpg");

        softAssert.assertTrue(TestBase.isElementPresent("Search_Result"), "Search_Result is not right");
        softAssert.assertAll();

    }

    @AfterTest
    public void quit() throws InterruptedException {

        if (TestBase.driver != null)
            Thread.sleep(4000);
        TestBase.driver.quit();
        System.out.println("exit");
    }

}
