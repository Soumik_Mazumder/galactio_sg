package Negative_Test_Cases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static Negative_Test_Cases.TestBase.driver;

/**
 * Created by Soumik Mazumder on 9/6/2016.
 */
public class redmine_Id_27080 {

    SoftAssert softAssert = new SoftAssert();

    @Test(priority = 1)
    public void C19194_Time_slot_is_not_displaying_properly_in_Camera_sections() throws Exception {
        TestBase utill = new TestBase();
        utill.initialize();

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }

        //Click on Options button and go to Camera page
        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Camera_Button").click();
        Thread.sleep(3000);

        softAssert.assertTrue(TestBase.isElementPresent("Verify_Camera_Text"), "Verify_Camera_Text header is not present in the screen ");
        softAssert.assertTrue(TestBase.isElementPresent("Location_name_1"), "AYE is not present in the screen ");

        //Click on the first camera location
        TestBase.getObject("Real_Time_Camera").click();
        Thread.sleep(2000);

        //Verify the Time/Date and details section
        softAssert.assertTrue(TestBase.isElementPresent("Date_Time_Details"), "Date_Time_Details is not present in the screen ");
        Thread.sleep(2000);

        //Take a screen shot to verify the UI
        TestBase.get_Screenshot("src\\test\\resource\\redmine_Id_27080\\C19194_Time_slot_is_not_displaying_properly_in_Camera_sections/" + "UI_Check_Camera" + ".jpg");

    }


    @AfterTest
    public void quit() throws InterruptedException {

        if (driver != null)
            Thread.sleep(4000);
        driver.quit();
        System.out.println("exit");
    }
}
