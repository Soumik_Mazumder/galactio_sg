package Negative_Test_Cases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

/**
 * Created by Soumik Mazumder on 8/23/2016.
 */
public class redmine_Id_31838 {

    @Test(priority = 1)
    public void C18519_Details_for_Coordinates_sent_through_Send_Location_option_is_not_accurately_displayed() throws Exception {
        SoftAssert softAssert = new SoftAssert();
        TestBase utill = new TestBase();
        utill.initialize();

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }

        //Click on Option > SendLocation
        TestBase.getObject("Option_Button").click();
        TestBase.driver.scrollTo("Send Location");
        TestBase.getObject("Send_Location_Button").click();
        Thread.sleep(1000);
        TestBase.driver.navigate().back();

        //Click on Co-Ordinate option
        TestBase.driver.scrollTo("Coordinates");
        TestBase.getObject("Co-Ordinate_Options").click();

        //Enter Latitude and Logitude for search Location
        TestBase.getObject("Latitude_TextBox").sendKeys(TestBase.TestData.getProperty("Latitude"));
        Thread.sleep(2000);
        TestBase.getObject("Logitide_TextBox").sendKeys(TestBase.TestData.getProperty("Logitude"));
        TestBase.getObject("Ok_Button").click();
        Thread.sleep(1000);

        //click on Send Location from the popup
        TestBase.getObject("Send_Location_Button").click();

        //Select Email Option and verify
        TestBase.getObject("Email_Text").click();
        Thread.sleep(1000);
        TestBase.getObject("EMail").click();
        Thread.sleep(1000);

        //Verify the Content of the Email
        TestBase.driver.navigate().back();
        softAssert.assertTrue(TestBase.isElementPresent("Verify_Email_Content"), "Email_Content is nor right");


        //Click on back
        Thread.sleep(2000);
        //  Creating Loop to back from the screen
        for (int i = 0; i < 5; i++) {
            TestBase.driver.navigate().back();
        }

        softAssert.assertAll();
    }


    @AfterTest
    public void quit() throws InterruptedException {

        if (TestBase.driver != null)
            Thread.sleep(4000);
        TestBase.driver.quit();
        System.out.println("exit");
    }
}
