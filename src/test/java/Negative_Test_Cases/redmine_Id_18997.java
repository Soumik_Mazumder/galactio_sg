package Negative_Test_Cases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static Negative_Test_Cases.TestBase.driver;

/**
 * Created by Soumik Mazumder on 9/5/2016.
 */
public class redmine_Id_18997 {

    SoftAssert softAssert = new SoftAssert();

    @Test(priority = 1)
    public void C19181_Add_to_favourites_PopUp_msg_box_disappearing_while_keeping_the_POI_name_blank_in_the_edit_textbox_and_pressing_OK_button() throws Exception {
        TestBase utill = new TestBase();
        utill.initialize();

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }

        //Open any Location in Map View and Select Favourite for Recent & Favourite Page
        TestBase.getObject("Search_Button").click();
        TestBase.getObject("POI_Options").click();
        TestBase.getObject("Emergency").click();
        TestBase.getObject("All_Emergency").click();
        Thread.sleep(3000);
        TestBase.getObject("Location_Name").click();
        Thread.sleep(1000);
        //Select favourite
        TestBase.getObject("Favourite_Star_Button").click();
        Thread.sleep(2000);

        //Clear the location name
        TestBase.getObject("Edit_Field").click();
        TestBase.getObject("Edit_Field").clear();
        Thread.sleep(1000);

        TestBase.getObject("Ok_Button").click();
        Thread.sleep(1000);

        //Verify the Favourite edit popup should not be closed
        TestBase.get_Screenshot("src\\test\\resource\\redmine_Id_18997\\C19181_Add_to_favourites_PopUp_msg_box_disappearing_while_keeping_the_POI_name_blank_in_the_edit_textbox_and_pressing_OK_button/" + "Add_Fav_Popup" + ".jpg");
    }


    @AfterTest
    public void quit() throws InterruptedException {

        if (driver != null)
            Thread.sleep(4000);
        driver.quit();
        System.out.println("exit");
    }


}
