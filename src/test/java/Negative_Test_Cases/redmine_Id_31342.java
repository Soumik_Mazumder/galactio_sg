package Negative_Test_Cases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

/**
 * Created by Soumik Mazumder on 8/23/2016.
 */
public class redmine_Id_31342 {
    SoftAssert softAssert = new SoftAssert();
    @Test(priority = 1)
    public void C18680_Issues_in_Co_ordinate_Page() throws Exception {

        TestBase utill = new TestBase();
        utill.initialize();

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }

        TestBase.getObject("Search_option").click();
        TestBase.getObject("Coordinates").click();

        TestBase.getObject("Latitude").click();
        TestBase.getObject("Latitude").sendKeys(TestBase.TestData.getProperty("N_Lat"));
        TestBase.getObject("Longitude").click();
        TestBase.getObject("Longitude").sendKeys(TestBase.TestData.getProperty("N_Long"));


        softAssert.assertTrue(TestBase.isElementPresent("Lat_Valid"), "Lat_Valid is present");
        softAssert.assertTrue(TestBase.isElementPresent("Long_Valid"), "Long_Valid is present");
        softAssert.assertAll();

    }


    @Test(priority = 2)
    public void C18680_N_and_E_is_missing_In_location_name() throws Exception {
        //Click on Back
        TestBase.driver.navigate().back();
        //TestBase.driver.navigate().back();

        //Click on co-ordinate and verify the details
        TestBase.getObject("Coordinates").click();

        //Enter Latitude and Logitude for search Location
        TestBase.getObject("Latitude_TextBox").sendKeys(TestBase.TestData.getProperty("Latitude"));
        Thread.sleep(2000);
        TestBase.getObject("Logitide_TextBox").sendKeys(TestBase.TestData.getProperty("Logitude"));
        TestBase.getObject("Ok_Button").click();
        Thread.sleep(1000);

        softAssert.assertTrue(TestBase.isElementPresent("Valid_Check_CoOrdinate"), "N and E are present");

        softAssert.assertAll();
    }

    @AfterTest
    public void quit() throws InterruptedException {

        if (TestBase.driver != null)
            Thread.sleep(4000);
        TestBase.driver.quit();
        System.out.println("exit");
    }
}
