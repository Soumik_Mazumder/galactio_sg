package Negative_Test_Cases;

import org.openqa.selenium.*;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.HashMap;

import static Negative_Test_Cases.TestBase.driver;

/**
 * Created by Soumik Mazumder on 8/18/2016.
 */
public class redmine_Id_31895 {
    @Test(priority = 1)
    public void C318484The_location_which_is_outside_the_countrymap_should_not_be_marked_with_the_destination_flag_in_mapview() throws Exception {

        SoftAssert softAssert = new SoftAssert();
        TestBase utill = new TestBase();
        utill.initialize();
        Thread.sleep(2000);

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }


        //Open any location in map view
        TestBase.getObject("Search_Button").click();
        TestBase.getObject("POI_Options").click();
        TestBase.getObject("Emergency").click();
        TestBase.getObject("All_Emergency").click();
        Thread.sleep(3000);
        TestBase.getObject("Location_Name").click();
        Thread.sleep(1000);

        //Go to maximum zoom out
        for (int i = 0; i <= 9; i++) {
            TestBase.getObject("Verify_Zoom_Out_button").click();
        }

        //TestBase.getObject("moreIcon_logout").click();

        //Click on the Map view

        WebElement element = driver.findElement(By.id("com.galactio.mobile.sg:id/surface_view_only"));
            //should be switched to name instead of text JavascriptExecutor
        JavascriptExecutor  js = (JavascriptExecutor) driver;
        Point p = element.getLocation();
        Dimension size = element.getSize();
        //double x = p.getX() + size.getWidth() / 2.0, y = p.getY() + size.getHeight() / 2.0;
        HashMap<String , Double> point = new HashMap<String , Double>();
        point.put("x" ,320.0); point.put("y" , 582.0); js.executeScript("mobile: tap", point);
        Thread.sleep(1000);

        //Take a screen shot
        TestBase.get_Screenshot("src\\test\\resource\\REDMINE\\redmine31895/" + "ToastVerification" + ".png");
    }


    @AfterTest
    public void quit() throws InterruptedException {

        if (TestBase.driver != null)
            Thread.sleep(4000);
        TestBase.driver.quit();
        System.out.println("exit");
    }


    }
