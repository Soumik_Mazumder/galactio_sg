package Negative_Test_Cases;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static Negative_Test_Cases.TestBase.driver;

/**
 * Created by Soumik Mazumder on 8/30/2016.
 */
public class redmine_id_26634 {


    SoftAssert softAssert = new SoftAssert();

    @Test(priority = 1)
    public void C19023_Unable_to_register_car_in_Register_Car_section() throws Exception {
        TestBase utill = new TestBase();
        utill.initialize();

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }

        //Click on Options button
        TestBase.getObject("Option_Button").click();
        Thread.sleep(2000);
        //Scroll to CAMERA Option
        TestBase.driver.scrollTo("Cameras");
        TestBase.getObject("Verify_Camera_Button").click();

        // verify the Refresh button
        Assert.assertTrue(TestBase.isElementPresent("Refresh_Button"), "Refresh_Button is not present in the Camera screen ");
        TestBase.getObject("Back_Button").click();

    }

    @AfterTest
    public void quit() throws InterruptedException {

        if (driver != null)
            Thread.sleep(4000);
        driver.quit();
        System.out.println("exit");
    }
}
