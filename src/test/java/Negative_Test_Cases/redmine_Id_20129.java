package Negative_Test_Cases;

import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static Negative_Test_Cases.TestBase.OR;
import static Negative_Test_Cases.TestBase.driver;

/**
 * Created by Soumik Mazumder on 9/1/2016.
 */
public class redmine_Id_20129 {

    /*public MobileElement scrollTo(String text) {
        String uiScrollables = AndroidDriver.UiScrollable("new UiSelector().descriptionContains(\"" + text + "\")") +
                AndroidDriver.UiScrollable("new UiSelector().textContains(\"" + text + "\")");
        return (MobileElement)findElementByAndroidUIAutomator(uiScrollables);
    }*/


    public static void scrollToa(String selector) {
        String selectorString = String.format("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView("+ selector +")");

        TestBase.driver.findElement(MobileBy.AndroidUIAutomator(selectorString));
    }



    SoftAssert softAssert = new SoftAssert();

    @Test(priority = 1)
    public void C19085_Locations_address_overlapping_issue_for_rich_POI() throws Exception {
        TestBase utill = new TestBase();
        utill.initialize();

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }

        TestBase.getObject("Search_Button").click();
        TestBase.getObject("POI_Options").click();
        TestBase.getObject("Restaurant").click();
        TestBase.getObject("Chinese").click();
        Thread.sleep(2000);

        //Scroll the location
        scrollToa(OR.getProperty("Rich_POI_Location"));


        //TestBase.driver.scrollTo("PEACH GARDEN THOMSON PLAZA");
        TestBase.getObject("Rich_POI_Location").click();
        Thread.sleep(3000);

        //Take a screen shot for UI verification for Rich POI
        TestBase.get_Screenshot("src\\test\\resource\\redmine_Id_20129\\C19085_Locations_address_overlapping_issue_for_rich_POI/" + "UI_Rich_POI" + ".jpg");

    }


    @AfterTest
    public void quit() throws InterruptedException {

        if (driver != null)
            Thread.sleep(4000);
        driver.quit();
        System.out.println("exit");
    }
}
