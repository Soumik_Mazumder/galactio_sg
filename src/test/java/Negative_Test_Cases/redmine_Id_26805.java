package Negative_Test_Cases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static Negative_Test_Cases.TestBase.driver;

/**
 * Created by Soumik Mazumder on 8/30/2016.
 */
public class redmine_Id_26805 {

    SoftAssert softAssert = new SoftAssert();

    @Test(priority = 1)
    public void C19023_Unable_to_register_car_in_Register_Car_section() throws Exception {
        TestBase utill = new TestBase();
        utill.initialize();

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }


        TestBase.getObject("Option_Button").click();
        TestBase.driver.scrollTo("Account");

        //LogIn into the MapSYNQ
        TestBase.getObject("Verify_Account_Button").click();
        TestBase.getObject("SharePosition_email_label").click();
        TestBase.getObject("SharePosition_email_label").sendKeys(TestBase.TestData.getProperty("SharePosition_user_name"));

        Thread.sleep(2000);
        TestBase.getObject("share_pwd_field").click();
        TestBase.getObject("share_pwd_field").sendKeys(TestBase.TestData.getProperty("SharePosition_pwd"));

        TestBase.driver.navigate().back();
        TestBase.getObject("share_Log_in_btn").click();
        Thread.sleep(3000);

        //Click on (+)icon for Register Car feature
        TestBase.getObject("Register_Car_Button").click();
        Thread.sleep(1000);
        TestBase.driver.navigate().back();

        //Enter some value in UUID, Car_Name, Car_Model  Section
        TestBase.getObject("UUID_Text").click();
        TestBase.getObject("UUID_Text").sendKeys("4JJJUTEST");
        Thread.sleep(1000);
        TestBase.driver.navigate().back();
        Thread.sleep(1000);

        TestBase.getObject("car_Name_Field").click();
        TestBase.getObject("car_Name_Field").sendKeys("AUDI");
        TestBase.driver.navigate().back();
        Thread.sleep(1000);

        TestBase.getObject("Car_Model").click();
        TestBase.getObject("Car_Model").sendKeys("007");
        TestBase.driver.navigate().back();
        Thread.sleep(1000);

        //Click on Register button and verify
        TestBase.getObject("Register_Button").click();
        Thread.sleep(1000);
        TestBase.get_Screenshot("src\\test\\resource\\redmine_Id_26805\\C19023_Unable_to_register_car_in_Register_Car_section/" + "Registered_Successfully" + ".jpg");

    }


    @AfterTest
    public void quit() throws InterruptedException {

        if (driver != null)
            Thread.sleep(4000);
        driver.quit();
        System.out.println("exit");
    }

}
