package Negative_Test_Cases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

/**
 * Created by Soumik Mazumder on 8/29/2016.
 */
public class redmine_Id_27317 {
    SoftAssert softAssert = new SoftAssert();

    @Test(priority = 1)
    public void C18950_Improper_format_of_text_displayed_in_Saved_Ads_section() throws Exception {

        TestBase utill = new TestBase();
        utill.initialize();

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }

        //Go to option and open the Saved Ad option and verify
        TestBase.getObject("Option_Button").click();
        TestBase.driver.scrollTo("Saved Ads");
        Thread.sleep(1000);
        TestBase.getObject("Saved_Ads").click();
        Thread.sleep(1000);

        softAssert.assertTrue(TestBase.isElementPresent("No_Add_Text"), "No_Add_Yet_Text is not present in the screen ");
        TestBase.getObject("Back_Button").click();
        softAssert.assertAll();
    }


    @AfterTest
    public void quit() throws InterruptedException {

        if (TestBase.driver != null)
            Thread.sleep(4000);
        TestBase.driver.quit();
        System.out.println("exit");
    }



}
