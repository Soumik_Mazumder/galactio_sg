package Negative_Test_Cases;

import com.sun.org.apache.bcel.internal.generic.BREAKPOINT;
import io.appium.java_client.MobileElement;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import javax.swing.text.DefaultEditorKit;

/**
 * Created by Soumik Mazumder on 8/23/2016.
 */
public class redmine_Id_28971 {

    SoftAssert softAssert = new SoftAssert();
    @Test(priority = 1)
    public void C18680_Issues_in_Co_ordinate_Page() throws Exception {

        TestBase utill = new TestBase();
        utill.initialize();

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }

        //Scroll in the application Dashboard for "SUGGESTED FOR YOU" section
        MobileElement element= TestBase.driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(resourceId(\"com.galactio.mobile.sg:id/ignore\"))");

        //Click on the Share button from Dashboard "Suggested For You" section
        while(!((TestBase.getObject("Share_Id").getText()).equalsIgnoreCase("Share"))){
            TestBase.getObject("Suggested_For_You_Text_Button").click();
            Thread.sleep(2000);
        }
        TestBase.getObject("Share_Id").click();
        Thread.sleep(3000);

        //Take a screen shot
        TestBase.get_Screenshot("src\\test\\resource\\redmine_Id_28971\\C18680_Issues_in_Co_ordinate_Page/" + "Share_Page_No_Crash" + ".jpg");
        softAssert.assertAll();
    }

    @AfterTest
    public void quit() throws InterruptedException {

        if (TestBase.driver != null)
            Thread.sleep(4000);
        TestBase.driver.quit();
        System.out.println("exit");
    }


    }


