package Negative_Test_Cases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static Negative_Test_Cases.TestBase.driver;

/**
 * Created by Soumik Mazumder on 8/31/2016.
 */
public class redmine_Id_26444 {

    SoftAssert softAssert = new SoftAssert();


    @Test(priority = 1)
    public void Calendar_feature_is_not_present_in_the_application() throws Exception {
        TestBase utill = new TestBase();
        utill.initialize();

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }

        //Go to the Option and verify the Calender Options
        TestBase.getObject("Option_Button").click();
        TestBase.driver.scrollTo("Calendar");
        Thread.sleep(1000);

        softAssert.assertFalse(TestBase.isElementPresent("CALENDER"), "CALENDER label is not there ");


        System.out.println("Other steps are covered in Positive scenario");




    }


    @AfterTest
    public void quit() throws InterruptedException {

        if (driver != null)
            Thread.sleep(4000);
        driver.quit();
        System.out.println("exit");
    }
    }
