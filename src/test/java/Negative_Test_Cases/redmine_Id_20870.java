package Negative_Test_Cases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static Negative_Test_Cases.TestBase.driver;

/**
 * Created by Soumik Mazumder on 8/31/2016.
 */
public class redmine_Id_20870 {
    SoftAssert softAssert = new SoftAssert();

    @Test(priority = 1)
    public void C19059_Issues_with_POI_Along_Route_screen() throws Exception {
        TestBase utill = new TestBase();
        utill.initialize();
        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }

        //OPen any location and start the simulation
        //Open any Location in Map View and Select Favourite for Recent & Favourite Page
        TestBase.getObject("Search_Button").click();
        TestBase.getObject("POI_Options").click();
        TestBase.getObject("Emergency").click();
        TestBase.getObject("All_Emergency").click();
        Thread.sleep(5000);
        TestBase.getObject("Location_Name").click();
        Thread.sleep(2000);
        TestBase.getObject("Go_Button").click();

        //Click on Back From Navigation screen
        TestBase.getObject("NavigationScreen_Back").click();

        TestBase.getObject("Search_option").click();
        TestBase.getObject("POI_Along_Route").click();

        //Verify the UI
        TestBase.get_Screenshot("src\\test\\resource\\redmine_Id_20870\\C19059_Issues_with_POI_Along_Route_screen/" + "UI_Checking_Issue1" + ".jpg");

        //Issue:2
        Thread.sleep(2000);

        TestBase.getObject("Position").click();
        Thread.sleep(1000);
        TestBase.getObject("Preview_Button").click();
        Thread.sleep(3000);
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        //Verify the UI
        TestBase.get_Screenshot("src\\test\\resource\\redmine_Id_20870\\C19059_Issues_with_POI_Along_Route_screen/" + "UI_Checking_Issue1" + ".jpg");

    }


    @AfterTest
    public void quit() throws InterruptedException {

        if (driver != null)
            Thread.sleep(4000);
        driver.quit();
        System.out.println("exit");
    }
}
