package Negative_Test_Cases;

import io.appium.java_client.MobileElement;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static Negative_Test_Cases.TestBase.driver;

/**
 * Created by Soumik Mazumder on 8/31/2016.
 */
public class redmine_Id_25513 {

    SoftAssert softAssert = new SoftAssert();

    @Test(priority = 1)
    public void C19502_UI_related_issue_in_Suggested_For_You_section() throws Exception {

        TestBase utill = new TestBase();
        utill.initialize();

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }

        //Scroll in the application Dashboard for "SUGGESTED FOR YOU" section
        MobileElement element= TestBase.driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(resourceId(\"com.galactio.mobile.sg:id/ignore\"))");


        //Click on the Share button from Dashboard "Suggested For You" section
        while(!((TestBase.getObject("Share_Id").getText()).equalsIgnoreCase("Report"))){
            TestBase.getObject("Suggested_For_You_Text_Button").click();
            Thread.sleep(2000);
        }

        TestBase.get_Screenshot("src\\test\\resource\\redmine_Id_25513\\C19502_UI_related_issue_in_Suggested_For_You_section/" + "UI_Checking" + ".jpg");

    }

    @AfterTest
    public void quit() throws InterruptedException {

        if (driver != null)
            Thread.sleep(4000);
        driver.quit();
        System.out.println("exit");
    }
}
