package Negative_Test_Cases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static Negative_Test_Cases.TestBase.driver;

/**
 * Created by Soumik Mazumder on 9/5/2016.
 */
public class redmine_Id_19016 {

    SoftAssert softAssert = new SoftAssert();

    @Test(priority = 1)
    public void C19154_The_Favourite_and_the_Share_icons_are_missing_in_the_header_of_the_Parking_View_screen() throws Exception {
        TestBase utill = new TestBase();
        utill.initialize();

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }


        // Click on Option Button
        TestBase.getObject("Option_Button").click();

        //Scroll to Parking Option
        TestBase.driver.scrollTo("Parking");

        TestBase.getObject("Verify_Parking_Button").click();
        Thread.sleep(5000);

        // verify the Alignment of the screen header
        TestBase.get_Screenshot("src\\test\\resource\\redmine_Id_19013\\C19152_Alignment_issue_in_ParkingRate_Popup_when_any_Car_Park_tapped_after_searching/" + "Alignment_Check" + ".jpg");

    }

    @AfterTest
    public void quit() throws InterruptedException {

        if (driver != null)
            Thread.sleep(4000);
        driver.quit();
        System.out.println("exit");
    }


}

