package Negative_Test_Cases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

/**
 * Created by Soumik Mazumder on 8/23/2016.
 */
public class redmine_Id_28967 {

    @Test(priority = 1)
    public void C18681_App_crashed_in_Share_App_Page() throws Exception {
        SoftAssert softAssert = new SoftAssert();
        TestBase utill = new TestBase();
        utill.initialize();

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }

        //Click on the Option and go to About page and verify
        TestBase.getObject("Search_option").click();
        TestBase.driver.scrollTo("About");

        TestBase.getObject("Verify_About_Button").click();
        TestBase.driver.scrollTo("Share App");

        //Click on Share App option and verify
        TestBase.getObject("Verify_Share_App_Text").click();
        Thread.sleep(2000);

        softAssert.assertTrue(TestBase.isElementPresent("Verify_Share_App_Text"), "Verify_Share_App_Text is not right");


        //Verify the page
        softAssert.assertTrue(TestBase.isElementPresent("Verify_Rate_and_Review_Text"), "Verify_Rate_and_Review_Text is not right");
        softAssert.assertTrue(TestBase.isElementPresent("Verify_Welcome_Message_Text"), "Verify_Welcome_Message_Text is not right");
        softAssert.assertTrue(TestBase.isElementPresent("Verify_Contact_Support_Text_1"), "Verify_Contact_Support_Text_1 is not right");
        softAssert.assertTrue(TestBase.isElementPresent("Verify_Contact_Support_Text_2"), "Verify_Contact_Support_Text_2 is not right");

        softAssert.assertAll();

    }


    @AfterTest
    public void quit() throws InterruptedException {

        if (TestBase.driver != null)
            Thread.sleep(4000);
        TestBase.driver.quit();
        System.out.println("exit");
    }
}
