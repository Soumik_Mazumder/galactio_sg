package Negative_Test_Cases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static Negative_Test_Cases.TestBase.driver;

/**
 * Created by Soumik Mazumder on 8/30/2016.
 */
public class redmine_Id_26763 {

    SoftAssert softAssert = new SoftAssert();

    @Test(priority = 1)
    public void C19026_After_Clear_data_when_try_to_select_any_location_which_selected_as_favourite_and_try_Renamed_unable_to_save_as_favourite() throws Exception {
        TestBase utill = new TestBase();
        utill.initialize();

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }

        //POI Search
        TestBase.getObject("Search_Button").click();
        TestBase.getObject("POI_Options").click();

        TestBase.getObject("Search_POI_field").click();
        TestBase.getObject("Search_POI_field").sendKeys(TestBase.TestData.getProperty("POI_Location_1"));
        TestBase.getObject("Search_button").click();
        Thread.sleep(2000);

        //Click on first location
        TestBase.getObject("Location_Id").click();
        Thread.sleep(2000);
        //Select favourite
        TestBase.getObject("Favourite_Star_Button").click();
        TestBase.getObject("Ok_Button").click();

        //Click on Back to dashboard
        TestBase.getObject("Back_Button").click();
        TestBase.getObject("Back_Button").click();
        TestBase.getObject("Back_Button").click();
        TestBase.getObject("Back_Button").click();
       // TestBase.getObject("Back_Button").click();

        //Clear all the data Functionality Flow:
        Thread.sleep(2000);
        TestBase.getObject("Option_Button").click();
        TestBase.clear_data();
        TestBase.getObject("Back_Button").click();


        //Using the same steps to select the FAvourite

        //POI Search

        TestBase.getObject("Search_Button").click();
        TestBase.getObject("POI_Options").click();
        TestBase.getObject("Search_POI_field").click();
        TestBase.getObject("Search_POI_field").sendKeys(TestBase.TestData.getProperty("POI_Location_1"));
        TestBase.getObject("Search_button").click();
        Thread.sleep(2000);

        //Click on first location
        TestBase.getObject("Location_Id").click();
        Thread.sleep(2000);
        //Select favourite
        TestBase.getObject("Favourite_Star_Button").click();
        TestBase.getObject("Ok_Button").click();

        TestBase.get_Screenshot("src\\test\\resource\\redmine_Id_26763\\C19026/" + "Select_Favourite" + ".jpg");
    }


    @AfterTest
    public void quit() throws InterruptedException {

        if (driver != null)
            Thread.sleep(4000);
        driver.quit();
        System.out.println("exit");
    }
}
