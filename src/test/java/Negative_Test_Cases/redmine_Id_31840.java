package Negative_Test_Cases;

import io.appium.java_client.android.AndroidKeyCode;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

/**
 * Created by Soumik Mazumder on 8/23/2016.
 */
public class redmine_Id_31840 {


    @Test(priority = 1)
    public void C18520_Space_is_being_accepted_in_the_UUID_field_during_car_registration() throws Exception {
        SoftAssert softAssert = new SoftAssert();
        TestBase utill = new TestBase();
        utill.initialize();

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }

        TestBase.getObject("Option_Button").click();

        //Go to LogIn page and login into the MapSYNQ

        TestBase.driver.scrollTo("Account");
        TestBase.getObject("Verify_Account_Button").click();


        TestBase.getObject("SharePosition_email_label").click();
        TestBase.getObject("SharePosition_email_label").sendKeys(TestBase.TestData.getProperty("SharePosition_user_name"));

        Thread.sleep(2000);
        TestBase.getObject("share_pwd_field").click();
        TestBase.getObject("share_pwd_field").sendKeys(TestBase.TestData.getProperty("SharePosition_pwd"));

        //TestBase.driver.navigate().back();
        TestBase.getObject("share_Log_in_btn").click();
        Thread.sleep(3000);

        //Click on the "REGISTERED CARS" button
        TestBase.getObject("Register_Car_Icon").click();
        Thread.sleep(1000);
        TestBase.driver.navigate().back();
        Thread.sleep(2000);

        //Give Space in the UUID sections
        TestBase.getObject("UUID_Box").click();
        TestBase.driver.pressKeyCode(AndroidKeyCode.SPACE);
        //TestBase.driver.pressKeyCode(AndroidKeyCode.SPACE);
        //TestBase.driver.pressKeyCode(AndroidKeyCode.SPACE);

        softAssert.assertAll();
    }

    @AfterTest
    public void quit() throws InterruptedException {

        if (TestBase.driver != null)
            Thread.sleep(4000);
        TestBase.driver.quit();
        System.out.println("exit");
    }

}
