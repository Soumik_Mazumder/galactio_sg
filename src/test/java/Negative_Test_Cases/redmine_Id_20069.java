package Negative_Test_Cases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static Negative_Test_Cases.TestBase.driver;

/**
 * Created by Soumik Mazumder on 9/2/2016.
 */
public class redmine_Id_20069 {

    SoftAssert softAssert = new SoftAssert();

    @Test(priority = 1)
    public void C19131_TCAM_data_not_available() throws Exception {
        TestBase utill = new TestBase();
        utill.initialize();

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }

        // Click on Option Button
        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Camera_Button").click();
        Thread.sleep(5000);

        //Verify the Camera page
        TestBase.get_Screenshot("src\\test\\resource\\redmine_Id_20069\\C19131_TCAM_data_not_available/" + "Camera screen" + ".jpg");
        TestBase.getObject("Back_button").click();

    }


    @AfterTest
    public void quit() throws InterruptedException {

        if (driver != null)
            Thread.sleep(4000);
        driver.quit();
        System.out.println("exit");
    }

}
