package Negative_Test_Cases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static Negative_Test_Cases.TestBase.driver;

/**
 * Created by Soumik Mazumder on 9/5/2016.
 */
public class redmine_Id_18976 {
    SoftAssert softAssert = new SoftAssert();

    @Test(priority = 1)
    public void C19157_Duplicate_search_results_are_being_displayed_when_searched_with_some_postal_codes() throws Exception {
        TestBase utill = new TestBase();
        utill.initialize();

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }

        TestBase.getObject("Search_Button").click();

        //Click on search destination field andd entering location1
        TestBase.getObject("Search_destination_edit_field").click();
        TestBase.getObject("Search_destination_edit_field").sendKeys(TestBase.TestData.getProperty("Postal_Code1"));
        TestBase.getObject("Search_button").click();
        Thread.sleep(3000);

        //Verify the Result Page
        TestBase.get_Screenshot("src\\test\\resource\\redmine_Id_18976\\C19157_Duplicate_search_results_are_being_displayed_when_searched_with_some_postal_codes/" + "Duplicate_Entry" + ".jpg");

    }

    @AfterTest
    public void quit() throws InterruptedException {

        if (driver != null)
            Thread.sleep(4000);
        driver.quit();
        System.out.println("exit");
    }
}
