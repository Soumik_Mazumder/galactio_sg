package Negative_Test_Cases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

/**
 * Created by Soumik Mazumder on 8/17/2016.
 */
public class redmineId_31909 {
    @Test(priority = 1)
    public void C18452_Without_tapping_done_button_on_calendar_popup_in_CREATE_NEW_ACCOUNT_PAGE_date_is_being_displayed() throws Exception {

        SoftAssert softAssert = new SoftAssert();
        TestBase utill = new TestBase();
        utill.initialize();
        Thread.sleep(2000);

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }

        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Account_Button").click();
        TestBase.driver.navigate().back();
        Thread.sleep(1000);

        TestBase.getObject("create_new_acct").click();
        /*Thread.sleep(1000);
        TestBase.driver.navigate().back();*/
        Thread.sleep(1000);

        TestBase.getObject("DOB").click();
        Thread.sleep(2000);

        //Click on OK if fist time it's not clicked
        try {
            System.out.println("Click on DOB button");
            TestBase.getObject("DOB").click();}
        catch (Exception e){ System.out.println(" First DOB button click ");
        }

        TestBase.getObject("Class_Name").click();
        softAssert.assertAll();


    }

    @AfterTest
    public void quit() throws InterruptedException {

        if (TestBase.driver != null)
            Thread.sleep(4000);
        TestBase.driver.quit();
        System.out.println("exit");
    }
}
