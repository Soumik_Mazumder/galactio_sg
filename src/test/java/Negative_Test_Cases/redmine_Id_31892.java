package Negative_Test_Cases;

import org.openqa.selenium.*;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.HashMap;

import static Negative_Test_Cases.TestBase.driver;

/**
 * Created by Soumik Mazumder on 8/18/2016.
 */
public class redmine_Id_31892 {

    @Test(priority = 1)
    public void C18511Blank_Location_is_being_displayed_under_destination_in_Route_preview_screen_for_a_location_with_no_name() throws Exception {

        SoftAssert softAssert = new SoftAssert();
        TestBase utill = new TestBase();
        utill.initialize();
        Thread.sleep(2000);

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }


        //Open any location in map view
        TestBase.getObject("Search_Button").click();
        TestBase.getObject("POI_Options").click();

        TestBase.driver.scrollTo("Sports & Golf");
        TestBase.getObject("Sports_Golf_Text").click();
        TestBase.getObject("Golf_Text").click();
        Thread.sleep(1000);
        TestBase.getObject("Position").click();


        //Click on the Map view

        WebElement element = driver.findElement(By.id("com.galactio.mobile.sg:id/surface_view_only"));
        //should be switched to name instead of text JavascriptExecutor
        JavascriptExecutor js = (JavascriptExecutor) driver;
        Point p = element.getLocation();
        Dimension size = element.getSize();
        //double x = p.getX() + size.getWidth() / 2.0, y = p.getY() + size.getHeight() / 2.0;
        HashMap<String , Double> point = new HashMap<String , Double>();
        point.put("x" ,320.0); point.put("y" , 582.0); js.executeScript("mobile: tap", point);
        Thread.sleep(1000);

        softAssert.assertTrue(TestBase.isElementPresent("LocationText"), "LocationText not present");
        softAssert.assertTrue(TestBase.isElementPresent("Location_Co_ordinate"), "Location_Co_ordinate not present");

        //Click on "Set Destination" button
        TestBase.getObject("Set_Destination_Button").click();

        //Click on 'Preview' button
        TestBase.getObject("Preview_Button").click();
        //Verify all the Destination details
        softAssert.assertTrue(TestBase.isElementPresent("Destination_text"), "Destination_text is not present");
        softAssert.assertTrue(TestBase.isElementPresent("Location_Co_ordinate"), "Location_Co_ordinate not present");

    }

    @AfterTest
    public void quit() throws InterruptedException {

        if (TestBase.driver != null)
            Thread.sleep(4000);
        TestBase.driver.quit();
        System.out.println("exit");
    }

}
