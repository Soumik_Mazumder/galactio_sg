package Negative_Test_Cases;

import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.List;

import static Negative_Test_Cases.TestBase.driver;

/**
 * Created by Soumik Mazumder on 8/29/2016.
 */
public class redmine_Id_27223 {
    SoftAssert softAssert = new SoftAssert();

    @Test(priority = 1)
    public void c18962_UI_related_issue_in_the_application() throws Exception {

        TestBase utill = new TestBase();
        utill.initialize();

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }

        TestBase.getObject("navigation_arrow").click();
        Thread.sleep(3000);
        TestBase.getObject("settings_button").click();

        //Verify the volume settings and verify the Ui
        //TestBase.getObject("settings_button").click();

        /*//Verify the "Volume" option verification
        String result = TestBase.TextFromImage("src\\test\\resource\\redmine_Id_27223\\c18962_UI_related_issue_in_the_application/" + "settings" + ".jpg");
        System.out.println(result);
        if((result.contains("Share Drive"))&&(result.contains("Volume"))&&(result.contains("Settings"))){
            System.out.println("passed");
        }else{
            Assert.assertTrue(1>2, "Page is not displayed correctly for incorrect password");
        }*/

        //Verify the Volume (View clasS)
          /* List<MobileElement> li= TestBase.driver.findElementsByClassName("android.view.View");
        System.out.println("BeforeClick ");
        Thread.sleep(3000);
            li.get(2).click();*/

        /*List<MobileElement> pic= TestBase.driver.findElements(By.className("//android.view.View"));
        pic.get(1).click(); // click based on index of image*/




       TestBase.get_Screenshot("src\\test\\resource\\redmine_Id_27223\\c18962_UI_related_issue_in_the_application/" + "screen1" + ".jpg");

        //Go back to application dashboard
        TestBase.driver.navigate().back();
        //TestBase.driver.navigate().back();

        //Go to option and verify the parking page
        Thread.sleep(2000);


        // Click on Option Button
        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Option_Button").click();

        //Scroll to Parking Option
        driver.scrollTo("Parking");

        TestBase.getObject("Verify_Parking_Button").click();
        Thread.sleep(5000);

        TestBase.get_Screenshot("src\\test\\resource\\redmine_Id_27223\\c18962_UI_related_issue_in_the_application/" + "Parking_Page" + ".jpg");

        //Verify the Parking details (i) details button
        TestBase.getObject("Parking_Rates_Button").click();
        Thread.sleep(5000);

        TestBase.get_Screenshot("src\\test\\resource\\redmine_Id_27223\\c18962_UI_related_issue_in_the_application/" + "iParking_Details" + ".jpg");
    }

    @AfterTest
    public void quit() throws InterruptedException {

        if (driver != null)
            Thread.sleep(4000);
        driver.quit();
        System.out.println("exit");
    }

}
