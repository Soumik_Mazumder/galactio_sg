package Negative_Test_Cases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static Negative_Test_Cases.TestBase.driver;

/**
 * Created by Soumik Mazumder on 9/8/2016.
 */
public class redmine_Id_27098 {

    SoftAssert softAssert = new SoftAssert();


    @Test(priority = 1)
    public void c18682_Parking_option_Crash_issue() throws Exception {

        TestBase utill = new TestBase();
        utill.initialize();

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }

        // Click on Option Button
        TestBase.getObject("Option_Button").click();

        //Scroll to Parking Option
        TestBase.driver.scrollTo("Parking");
        TestBase.getObject("Verify_Parking_Button").click();
        Thread.sleep(5000);

        //Click on the location and open in Map view
        TestBase.getObject("Position_Parking").click();
        Thread.sleep(2000);

        //Map_View
        TestBase.getObject("Position_Parking").click();



    }

    @AfterTest
    public void quit() throws InterruptedException {

        if (driver != null)
            Thread.sleep(4000);
        driver.quit();
        System.out.println("exit");
    }

}
