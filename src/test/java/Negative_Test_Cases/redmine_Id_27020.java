package Negative_Test_Cases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

/**
 * Created by Soumik Mazumder on 8/29/2016.
 */
public class redmine_Id_27020 {
    SoftAssert softAssert = new SoftAssert();

    @Test(priority = 1)
    public void C18998_Delete_icon_is_missing_on_selecting_the_registered_car_from_the_register_car_list() throws Exception {

        TestBase utill = new TestBase();
        utill.initialize();

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }

        TestBase.getObject("Option_Button").click();
        TestBase.driver.scrollTo("Account");

        //LogIn into the MapSYNQ
        TestBase.getObject("Verify_Account_Button").click();
        TestBase.getObject("SharePosition_email_label").click();
        TestBase.getObject("SharePosition_email_label").sendKeys(TestBase.TestData.getProperty("SharePosition_user_name"));

        Thread.sleep(2000);
        TestBase.getObject("share_pwd_field").click();
        TestBase.getObject("share_pwd_field").sendKeys(TestBase.TestData.getProperty("SharePosition_pwd"));

        TestBase.driver.navigate().back();
        TestBase.getObject("share_Log_in_btn").click();
        Thread.sleep(3000);

        //Click on the Registered car
        TestBase.getObject("Share_Title").click();
        Thread.sleep(2000);

        softAssert.assertTrue(TestBase.isElementPresent("Delete_Button_Car"), "Delete button is not present in the screen ");
        TestBase.getObject("Delete_Button_Car").click();

        //Verify the Delete text
        softAssert.assertTrue(TestBase.isElementPresent("Delete_popup1"), "Delete popup text is not present in the screen ");
        TestBase.getObject("Cancel_Button").click();

        softAssert.assertAll();
    }

    @AfterTest
    public void quit() throws InterruptedException {

        if (TestBase.driver != null)
            Thread.sleep(4000);
        TestBase.driver.quit();
        System.out.println("exit");
    }

    }
