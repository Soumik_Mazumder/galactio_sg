package Negative_Test_Cases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

/**
 * Created by Soumik Mazumder on 8/26/2016.
 */
public class redmine_Id_27383 {
    SoftAssert softAssert = new SoftAssert();

    @Test(priority = 1)
    public void C18928_Account_logout_pop_up_Checking_the_check_box_for_clearing_Recent_and_Favourite_is_not_working() throws Exception {

        TestBase utill = new TestBase();
        utill.initialize();

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }


        //Open any Location in Map View and Select Favourite for Recent & Favourite Page
        TestBase.getObject("Search_Button").click();
        TestBase.getObject("POI_Options").click();
        TestBase.getObject("Emergency").click();
        TestBase.getObject("All_Emergency").click();
        Thread.sleep(5000);
        TestBase.getObject("Location_Name").click();
        Thread.sleep(2000);
        //Select favourite
        TestBase.getObject("Favourite_Star_Button").click();
        TestBase.getObject("Ok_Button").click();
        Thread.sleep(2000);
        //Click on Back to dashboard
        TestBase.getObject("Back_Button").click();
        TestBase.getObject("Back_Button").click();
        TestBase.getObject("Back_Button").click();
        TestBase.getObject("Back_Button").click();
        TestBase.getObject("Back_Button").click();
        Thread.sleep(2000);


        TestBase.getObject("Option_Button").click();
        TestBase.driver.scrollTo("Account");

        //LogIn into the MapSYNQ
        TestBase.getObject("Verify_Account_Button").click();
        TestBase.getObject("SharePosition_email_label").click();
        TestBase.getObject("SharePosition_email_label").sendKeys(TestBase.TestData.getProperty("SharePosition_user_name"));

        Thread.sleep(2000);
        TestBase.getObject("share_pwd_field").click();
        TestBase.getObject("share_pwd_field").sendKeys(TestBase.TestData.getProperty("SharePosition_pwd"));

        TestBase.driver.navigate().back();
        TestBase.getObject("share_Log_in_btn").click();
        Thread.sleep(3000);


        //Verify user in Loged in page
        softAssert.assertTrue(TestBase.isElementPresent("Logout_Button"), "Logout_Button is not present");
        TestBase.getObject("Logout_Button").click();

        Thread.sleep(2000);

        //Verify the logout popup and click on check box
        TestBase.getObject("Clear_Fav_Recent").click();
        Thread.sleep(1000);
        TestBase.getObject("Logout_Button").click();

        TestBase.getObject("Back_Button").click();
        TestBase.getObject("Back_Button").click();

        TestBase.getObject("Search_Button").click();
        TestBase.getObject("Favourites").click();
        softAssert.assertTrue(TestBase.isElementPresent("no_favourites_msg1"), "No Favourite present in the screen ");

        TestBase.getObject("Back_Button").click();
        Thread.sleep(2000);
        TestBase.getObject("Recent_Options").click();
        softAssert.assertTrue(TestBase.isElementPresent("No_Recent_Text"), "No Recent present in the screen ");

        TestBase.getObject("Back_Button").click();
        TestBase.getObject("Back_Button").click();
        softAssert.assertAll();
    }

    @AfterTest
    public void quit() throws InterruptedException {

        if (TestBase.driver != null)
            Thread.sleep(4000);
        TestBase.driver.quit();
        System.out.println("exit");
    }
}
