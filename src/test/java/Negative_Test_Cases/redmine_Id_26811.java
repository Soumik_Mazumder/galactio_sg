package Negative_Test_Cases;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static Negative_Test_Cases.TestBase.driver;

/**
 * Created by Soumik Mazumder on 8/30/2016.
 */
public class redmine_Id_26811 {

    SoftAssert softAssert = new SoftAssert();

    @Test(priority = 1)
    public void C19005_Wrong_validation_message_in_Register_Car_option() throws Exception {

        TestBase utill = new TestBase();
        utill.initialize();

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }



        TestBase.getObject("Option_Button").click();
        TestBase.driver.scrollTo("Account");

        //LogIn into the MapSYNQ
        TestBase.getObject("Verify_Account_Button").click();
        TestBase.getObject("SharePosition_email_label").click();
        TestBase.getObject("SharePosition_email_label").sendKeys(TestBase.TestData.getProperty("SharePosition_user_name"));

        Thread.sleep(2000);
        TestBase.getObject("share_pwd_field").click();
        TestBase.getObject("share_pwd_field").sendKeys(TestBase.TestData.getProperty("SharePosition_pwd"));

        TestBase.driver.navigate().back();
        TestBase.getObject("share_Log_in_btn").click();
        Thread.sleep(3000);

        //Click on (+)icon for Register Car feature
        TestBase.getObject("Register_Car_Button").click();
        Thread.sleep(1000);
        TestBase.driver.navigate().back();

        //Click on REGISTER button and verify the toast Register_Button
        Thread.sleep(1000);
        TestBase.getObject("Register_Button").click();
        Thread.sleep(2000);

        //Verify the Toast message is correct or wrong
        //Using Tesseract
        TestBase.getScreenshotForParticularPart("Class_Name2", "src\\test\\resource\\redmine_Id_26811\\C19005_Wrong_validation_message_in_Register_Car_option/" + "Register_Car" + ".jpg");
        String result1 = TestBase.TextFromImage("src\\test\\resource\\redmine_Id_26811\\C19005_Wrong_validation_message_in_Register_Car_option/" + "Register_Car" + ".jpg");
        System.out.println("******************");
        System.out.println(result1);
        Thread.sleep(2000);

        if ((result1.contains("Please enter UUID. ")) ||(result1.contains("Can Name Please enter ULHD,"))) {
            System.out.println("Pass ");
        } else {
            Assert.assertTrue(1 > 2, "The toast notification is not displayed correctly for Register Car");
        }

        //Enter some value in UUID Section
        TestBase.getObject("UUID_Text").click();
        TestBase.getObject("UUID_Text").sendKeys("4JJJUTEST");
        Thread.sleep(1000);
        TestBase.driver.navigate().back();
        TestBase.getObject("Register_Button").click();


        TestBase.getObject("car_Name_Field").click();
        TestBase.driver.navigate().back();

        //Verify the Car Name sections message

        //Using Tesseract
        TestBase.getScreenshotForParticularPart("Class_Name3", "src\\test\\resource\\redmine_Id_26811\\C19005_Wrong_validation_message_in_Register_Car_option/" + "Car_Name" + ".jpg");
        String result2 = TestBase.TextFromImage("src\\test\\resource\\redmine_Id_26811\\C19005_Wrong_validation_message_in_Register_Car_option/" + "Car_Name" + ".jpg");
        System.out.println("******************");
        System.out.println(result2);
        Thread.sleep(2000);

        if ((result1.contains("Please enter Car Name. ")) ||(result1.contains("L72” Mode‘ Please enter Car Name."))) {
            System.out.println("Pass ");
        } else {
            Assert.assertTrue(1 > 2, "The toast notification is not displayed correctly for Register Car");
        }

    }

    @AfterTest
    public void quit() throws InterruptedException {

        if (driver != null)
            Thread.sleep(4000);
        driver.quit();
        System.out.println("exit");
    }

    }
