package Negative_Test_Cases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

/**
 * Created by Soumik Mazumder on 8/23/2016.
 */
public class redmine_Id_31874 {
    @Test(priority = 1)
    public void C18515_POI_When_tapped_on_i_button_from_the_MapView_issue_in_the_displayed_information() throws Exception {
        SoftAssert softAssert = new SoftAssert();
        TestBase utill = new TestBase();
        utill.initialize();
        Thread.sleep(2000);

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }


        //Open Specific location in map view
        TestBase.getObject("Search_Button").click();
        TestBase.getObject("POI_Options").click();

        TestBase.getObject("Search_destination_edit_field").click();
        TestBase.getObject("Search_destination_edit_field").sendKeys(TestBase.TestData.getProperty("Rich_POI"));

        TestBase.getObject("Search_button").click();

        //Click on the Location and open in Map_view
        softAssert.assertTrue(TestBase.isElementPresent("Rich_POI_LocationName"), "Rich_POI location not present");
        TestBase.getObject("Rich_POI_LocationName").click();
        Thread.sleep(1000);

        //Click on the Rich POI button
        TestBase.getObject("Rich_POi_Button").click();
        Thread.sleep(1000);

        //Take a screen shots and verify
        TestBase.get_Screenshot("src\\test\\resource\\redmine_Id_31874\\C18515_POI_When_tapped_on_i_button_from_the_MapView_issue_in_the_displayed_information/" + "Rich_POI_Details" + ".jpg");


        //Click on back
        Thread.sleep(2000);
        //  Creating Loop to back from the screen
        for (int i = 0; i < 6; i++) {
            TestBase.driver.navigate().back();
        }

        softAssert.assertAll();
    }

    @AfterTest
    public void quit() throws InterruptedException {

        if (TestBase.driver != null)
            Thread.sleep(4000);
        TestBase.driver.quit();
        System.out.println("exit");
    }

}
