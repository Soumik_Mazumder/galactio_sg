package Negative_Test_Cases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static Negative_Test_Cases.TestBase.driver;

/**
 * Created by Soumik Mazumder on 9/1/2016.
 */
public class redmine_Id_20862 {

    SoftAssert softAssert = new SoftAssert();

    @Test(priority = 1)
    public void C19080_Duplicate_records_are_displayed_under_POI_Religion_Others_Section() throws Exception {
        TestBase utill = new TestBase();
        utill.initialize();

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }

        TestBase.getObject("Search_Button").click();
        TestBase.getObject("POI_Options").click();
        TestBase.driver.scrollTo("Religion");

        TestBase.getObject("Religion_Category").click();
        Thread.sleep(2000);
        TestBase.getObject("Other").click();
        Thread.sleep(1000);


        //Verify the Duplicate entry
        TestBase.get_Screenshot("src\\test\\resource\\redmine_Id_20862\\C19080_Duplicate_records_are_displayed_under_POI_Religion_Others_Section/" + "Duplicate_entry_ShouldNot_Present" + ".jpg");
    }


    @AfterTest
    public void quit() throws InterruptedException {

        if (driver != null)
            Thread.sleep(4000);
        driver.quit();
        System.out.println("exit");
    }
}
