package Negative_Test_Cases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static Negative_Test_Cases.TestBase.driver;

/**
 * Created by Soumik Mazumder on 8/31/2016.
 */
public class redmine_Id_23627 {

    SoftAssert softAssert = new SoftAssert();

    @Test(priority = 1)
    public void C19056_Application_crashed_when_we_use_Dot_in_Longitude_and_Latitude_text_box_in_Co_Ordinate_section() throws Exception {

        TestBase utill = new TestBase();
        utill.initialize();

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }

        TestBase.getObject("Search_option").click();
        TestBase.getObject("Coordinates").click();
        softAssert.assertTrue(TestBase.isElementPresent("Verify_Degrees_Text"), "Verify_Degrees_Text is not present in the screen ");

        //Enter Dot(.) in Latitude and Longitude text box
        TestBase.getObject("Latitude").click();
        TestBase.getObject("Latitude").sendKeys(TestBase.TestData.getProperty("DOT"));
        TestBase.getObject("Longitude").click();
        TestBase.getObject("Longitude").sendKeys(TestBase.TestData.getProperty("DOT"));

        TestBase.getObject("Ok_Button").click();

        //Verify the Invalid Input popup is displaying and application is not crashing
        softAssert.assertTrue(TestBase.isElementPresent("Verify_Invalid_Input_Text_Degree"), "Verify_Invalid_Input_Text_Degree is not present in the screen ");
        softAssert.assertTrue(TestBase.isElementPresent("Verify_Please_Entry_Valid_Coordinates_Text_Degree"), "Text is not present in the screen ");

        TestBase.getObject("Ok_Button").click();
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
    }

    @AfterTest
    public void quit() throws InterruptedException {

        if (driver != null)
            Thread.sleep(4000);
        driver.quit();
        System.out.println("exit");
    }
}
