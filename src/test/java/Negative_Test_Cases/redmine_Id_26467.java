package Negative_Test_Cases;

import io.appium.java_client.MobileElement;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.List;

import static Negative_Test_Cases.TestBase.driver;

/**
 * Created by Soumik Mazumder on 9/5/2016.
 */
public class redmine_Id_26467 {
    SoftAssert softAssert = new SoftAssert();

    @Test(priority = 1)
    public void C19176_ZoomIn_and_ZoomOut_is_not_smooth_when_an_incident_is_opened_in_map_view() throws Exception {
        TestBase utill = new TestBase();
        utill.initialize();

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }


        //Click on Option_Button
        TestBase.getObject("Option_Button").click();
        //Click on Incidents_Button
        TestBase.getObject("Verify_Incidents_Button").click();
        Thread.sleep(2000);

        //Click on first incident
        List<MobileElement> incidents = TestBase.driver.findElementsByAndroidUIAutomator("UiSelector().resourceId(\"com.galactio.mobile.sg:id/incidentDesFrameLayout\")");

        incidents.get(0).click();
        TestBase.get_Screenshot("src\\test\\resource\\redmine_Id_26467\\C19176_ZoomIn_and_ZoomOut_is_not_smooth_when_an_incident_is_opened_in_map_view/" + "Incident_View_Screen_first_location" + ".jpg");

        for(int i=0;i<=1;i++){
            TestBase.getObject("Verify_Zoom_In_button").click();
        }
        TestBase.get_Screenshot("src\\test\\resource\\redmine_Id_26467\\C19176_ZoomIn_and_ZoomOut_is_not_smooth_when_an_incident_is_opened_in_map_view/" + "Max_Zoom_In_Incident_View_Screen_first_location" + ".jpg");

        for(int i=0;i<=9;i++){
            TestBase.getObject("Verify_Zoom_Out_button").click();
        }
        TestBase.get_Screenshot("src\\test\\resource\\redmine_Id_26467\\C19176_ZoomIn_and_ZoomOut_is_not_smooth_when_an_incident_is_opened_in_map_view/" + "Max_Zoom_Out_Incident_View_Screen_first_location" + ".jpg");

    }


    @AfterTest
    public void quit() throws InterruptedException {

        if (driver != null)
            Thread.sleep(4000);
        driver.quit();
        System.out.println("exit");
    }

}
