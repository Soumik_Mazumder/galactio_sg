package Negative_Test_Cases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static Negative_Test_Cases.TestBase.driver;

/**
 * Created by Soumik Mazumder on 8/31/2016.
 */
public class redmine_Id_26567 {
    SoftAssert softAssert = new SoftAssert();


    @Test(priority = 1)
    public void C19047_Resume_button_is_not_functional_in_Route_Overview_Screen() throws Exception {
        TestBase utill = new TestBase();
        utill.initialize();

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }

        //Set any location as Home
        TestBase.getObject("Home_Dashboard").click();
        TestBase.getObject("Yes_Button").click();
        TestBase.getObject("Current_Location").click();
        //Tap on "SAVE AND NAVIGATE" button
        TestBase.getObject("Save_And_Navigate").click();
        Thread.sleep(2000);

        //Click on header on Road instruction
        TestBase.getObject("Road_Instruction").click();
        softAssert.assertTrue(TestBase.isElementPresent("Resume_Button"), "Resume_Button is not present in the screen ");

        TestBase.getObject("Resume_Button").click();
        Thread.sleep(2000);
        softAssert.assertFalse(TestBase.isElementPresent("Resume_Button"), "Resume_Button is still present in the screen ");


    }

    @AfterTest
    public void quit() throws InterruptedException {

        if (driver != null)
            Thread.sleep(4000);
        driver.quit();
        System.out.println("exit");
    }
}
