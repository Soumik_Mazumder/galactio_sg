package Negative_Test_Cases;


import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

/**
 * Created by Soumik Mazumder on 8/17/2016.
 */
public class redmineId_31967 {

    @Test(priority = 1)
    public void c18446_Application_crashed_while_navigating_to_mapview_page_through_POI_Along_Route() throws Exception {

        SoftAssert softAssert= new SoftAssert();
        TestBase utill = new TestBase();
        utill.initialize();
        Thread.sleep(2000);

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }

        //Click on any location and create a route
        TestBase.getObject("Search_Button").click();
        TestBase.getObject("POI_Options").click();
        TestBase.getObject("Emergency").click();
        TestBase.getObject("All_Emergency").click();
        Thread.sleep(3000);
        TestBase.getObject("Location_Name").click();
        Thread.sleep(1000);
        TestBase.getObject("Go_Button").click();
        Thread.sleep(2000);

        //Click on Back From Navigation screen
        TestBase.getObject("NavigationScreen_Back").click();

        //Go to Set Origin
        // Click on Option Button
        TestBase.getObject("Option_Button").click();

        //Scroll for Set Origin Option and click on it
        TestBase.driver.scrollTo("Set Origin");
        TestBase.getObject("Set_Origin").click();

        //Click on POI_Along the Route
        TestBase.getObject("POI_Along_Route").click();
        TestBase.getObject("Suggested_Image").click();

        softAssert.assertTrue(TestBase.isElementPresent("Map_View_Text"), "Map_View_Text not present");

        //Click on Set Origin button
        Thread.sleep(1000);
        TestBase.getObject("Set_Origin_Close").click();

        softAssert.assertAll();

    }

    @AfterTest
    public void quit() throws InterruptedException {

        if (TestBase.driver != null)
            Thread.sleep(4000);
        TestBase.driver.quit();
        System.out.println("exit");
    }


}
