package Negative_Test_Cases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static Negative_Test_Cases.TestBase.driver;

/**
 * Created by Soumik Mazumder on 9/6/2016.
 */
public class redmine_Id_31889 {

    SoftAssert softAssert = new SoftAssert();

    @Test(priority = 1)
    public void C19193_Calendar_Tapping_back_from_results_page_of_calendar_takes_user_to_search_destination_page() throws Exception {
        TestBase utill = new TestBase();
        utill.initialize();

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }

        //User need to logIn into the MapSYNQ and
        //Click on Option button and login into mapSYNQ account
        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Account_Button").click();


        TestBase.getObject("email_field").clear();
        TestBase.getObject("email_label").sendKeys(TestBase.TestData.getProperty("SharePosition_user_name"));

        TestBase.getObject("pwd_field").clear();
        TestBase.getObject("pwd_field").sendKeys(TestBase.TestData.getProperty("SharePosition_pwd"));

        TestBase.driver.navigate().back();
        TestBase.getObject("log_in_btn").click();
        Thread.sleep(5000);
        TestBase.getObject("Back_Button").click();

        //Go to option and Calender page
        TestBase.getObject("Option_Button").click();
        TestBase.getObject("CALENDER").click();
        Thread.sleep(5000);

        //Click on the first event
        TestBase.getObject("Event1").click();

        //Take a screen shot Results screen appeared on the screen
        TestBase.get_Screenshot("src\\test\\resource\\redmine_Id_31889\\C19193_Calendar_Tapping_back_from_results_page_of_calendar_takes_user_to_search_destination_page/" + "Results_page" + ".jpg");

        //Verify after click on event user should present in the Result's screen
        softAssert.assertTrue(TestBase.isElementPresent("Result_Text"), "Result_Text is not present in the screen ");
        Thread.sleep(1000);
        TestBase.getObject("Back_Button").click();

        //Verify after click on back button from Results page application should landed on the Calender page
        softAssert.assertTrue(TestBase.isElementPresent("CALENDER"), "CALENDER header is not present in the screen ");


    }



    @AfterTest
    public void quit() throws InterruptedException {

        if (driver != null)
            Thread.sleep(4000);
        driver.quit();
        System.out.println("exit");
    }
}
