package Negative_Test_Cases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

/**
 * Created by Soumik Mazumder on 8/29/2016.
 */
public class redmine_Id_27004 {
    SoftAssert softAssert = new SoftAssert();

    @Test(priority = 1)
    public void C19000_Incomplete_feature_name_Report_displayed_in_menu_option_in_place_of_Report_incident() throws Exception {

        TestBase utill = new TestBase();
        utill.initialize();

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }

        TestBase.getObject("Option_Button").click();
        TestBase.driver.scrollTo("Report Incident");

        softAssert.assertTrue(TestBase.isElementPresent("Report_Incident"), "Report_Incident option is not present in the screen ");
        //TestBase.getObject("Report_Incident").click();


        softAssert.assertAll();
    }

    @AfterTest
    public void quit() throws InterruptedException {

        if (TestBase.driver != null)
            Thread.sleep(4000);
        TestBase.driver.quit();
        System.out.println("exit");
    }

    }
