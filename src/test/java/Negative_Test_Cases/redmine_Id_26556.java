package Negative_Test_Cases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static Negative_Test_Cases.TestBase.driver;

/**
 * Created by Soumik Mazumder on 8/31/2016.
 */
public class redmine_Id_26556 {


    SoftAssert softAssert = new SoftAssert();

    @Test(priority = 1)
    public void C19044_Refresh_button_is_not_present_in_Fuel_option() throws Exception {
        TestBase utill = new TestBase();
        utill.initialize();

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }

        //Click on Option Button and verify the Fuel section
        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Fuel_Option").click();
        Thread.sleep(5000);

        softAssert.assertTrue(TestBase.isElementPresent("Refresh_Button_Fuel"), "Refresh_Button_Fuel is not present in the screen ");

        //Click on BRAND tab and verify the same
        TestBase.getObject("BRAND_Tab").click();
        softAssert.assertTrue(TestBase.isElementPresent("Refresh_Button_Fuel"), "Refresh_Button_Fuel is not present in the screen ");

        softAssert.assertAll();
    }

    @AfterTest
    public void quit() throws InterruptedException {

        if (driver != null)
            Thread.sleep(4000);
        driver.quit();
        System.out.println("exit");
    }
    }
