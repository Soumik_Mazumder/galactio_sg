package Negative_Test_Cases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static Negative_Test_Cases.TestBase.driver;

/**
 * Created by Soumik Mazumder on 9/6/2016.
 */
public class redmine_Id_26795 {

    @Test(priority = 1)
    public void  C19195_Issue_regarding_de_select_the_Favourites_option() throws Exception {
        SoftAssert softAssert = new SoftAssert();
        TestBase utill = new TestBase();
        utill.initialize();

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }


        //Clear all the data Functionality Flow:
        TestBase.clear_data();
        TestBase.getObject("Back_Button").click();


        //Open any Location in Map View and Select Favourite for Recent & Favourite Page
        TestBase.getObject("Search_Button").click();
        TestBase.getObject("POI_Options").click();
        TestBase.getObject("Emergency").click();
        TestBase.getObject("All_Emergency").click();
        Thread.sleep(3000);
        TestBase.getObject("Location_Name").click();
        Thread.sleep(1000);
        //Select favourite
        TestBase.getObject("Favourite_Star_Button").click();
        Thread.sleep(2000);
        TestBase.getObject("Ok_Button").click();
        Thread.sleep(2000);


        //Verify the Favourite edit popup should not be closed
        TestBase.get_Screenshot("src\\test\\resource\\redmine_Id_26795\\C19195_Issue_regarding_de_select_the_Favourites_option/" + "Yellow_Star_icon" + ".jpg");

        //Click on the same Favourite_(Star) button and verify
        TestBase.getObject("Favourite_Star_Button").click();
        TestBase.get_Screenshot("src\\test\\resource\\redmine_Id_26795\\C19195_Issue_regarding_de_select_the_Favourites_option/" + "Star_icon_Discolor" + ".jpg");


    }


    @AfterTest
    public void quit() throws InterruptedException {

        if (driver != null)
            Thread.sleep(4000);
        driver.quit();
        System.out.println("exit");
    }


}
