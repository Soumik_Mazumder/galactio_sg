package Negative_Test_Cases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static Negative_Test_Cases.TestBase.driver;

/**
 * Created by Soumik Mazumder on 9/5/2016.
 */
public class redmine_Id_19902 {

    SoftAssert softAssert = new SoftAssert();

    @Test(priority = 1)
    public void C19126_Application_crash_Report_For_POI_Location() throws Exception {
        TestBase utill = new TestBase();
        utill.initialize();

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }


        TestBase.getObject("Search_Button").click();
        TestBase.getObject("POI_Options").click();

        TestBase.driver.scrollTo("Cities");
        TestBase.getObject("Cities_Text").click();

        Thread.sleep(5000);

        //Take a screen shot for application not crashing
        TestBase.get_Screenshot("src\\test\\resource\\redmine_Id_19902\\C19126_Application_crash_Report_For_POI_Location/" + "App_Not_Crashing" + ".jpg");
    }

    @AfterTest
    public void quit() throws InterruptedException {

        if (driver != null)
            Thread.sleep(4000);
        driver.quit();
        System.out.println("exit");
    }
}
