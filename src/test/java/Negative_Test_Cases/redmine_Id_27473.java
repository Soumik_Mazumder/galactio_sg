package Negative_Test_Cases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

/**
 * Created by Soumik Mazumder on 8/24/2016.
 */
public class redmine_Id_27473 {
    SoftAssert softAssert = new SoftAssert();

    @Test(priority = 1)
    public void C18721_Crashing_issue_for_Galactio_SG_Negative_Flow() throws Exception {

        TestBase utill = new TestBase();
        utill.initialize();

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }

        //Open any location in map view
        TestBase.getObject("Search_Button").click();
        TestBase.getObject("POI_Options").click();
        TestBase.getObject("Emergency").click();
        TestBase.getObject("All_Emergency").click();
        Thread.sleep(3000);
        TestBase.getObject("Location_Name").click();
        Thread.sleep(1000);

        //Click on Go button
        TestBase.getObject("Go_Button").click();
        Thread.sleep(3000);

        //Click on Back From Navigation screen
        TestBase.getObject("NavigationScreen_Back").click();

        //Tap on search and serach Burger King
        TestBase.getObject("Search_destination_edit_field").sendKeys(TestBase.TestData.getProperty("Burger_King"));
        TestBase.getObject("Search_button").click();
        Thread.sleep(2000);
        TestBase.getObject("Burger_Search_Location").click();
        Thread.sleep(1000);

        TestBase.getObject("Go_Button").click();
        Thread.sleep(3000);

        //Take a screen shots and verify
        TestBase.get_Screenshot("src\\test\\resource\\redmine_Id_27473\\C18721_Crashing_issue_for_Galactio_SG_Negative_Flow/" + "Application_Not_Crashing" + ".jpg");
    }

    @AfterTest
    public void quit() throws InterruptedException {

        if (TestBase.driver != null)
            Thread.sleep(4000);
        TestBase.driver.quit();
        System.out.println("exit");
    }


}
