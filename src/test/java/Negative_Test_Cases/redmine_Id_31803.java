package Negative_Test_Cases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

/**
 * Created by Soumik Mazumder on 8/23/2016.
 */
public class redmine_Id_31803 {

    @Test(priority = 1)
    public void C18521_Application_hanged_after_searching_in_Parking() throws Exception {
        SoftAssert softAssert = new SoftAssert();
        TestBase utill = new TestBase();
        utill.initialize();

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }


        // Click on Option Button
        TestBase.getObject("Option_Button").click();

        //Scroll to Parking Option
        TestBase.driver.scrollTo("Parking");

        TestBase.getObject("Verify_Parking_Button").click();

        TestBase.getObject("Search_Button_Parking").click();

        //Type a Parking locaton name and search the data
        TestBase.getObject("Search_Text").sendKeys(TestBase.TestData.getProperty("Location_Parking"));

        //Click on Search Button for result list
        TestBase.getObject("Search_Button_Parking").click();
        Thread.sleep(3000);

        //Take a screen shots with all the result list
        TestBase.get_Screenshot("src\\test\\resource\\redmine_Id_31803\\C18521_Application_hanged_after_searching_in_Parking/" + "Search_Result" + ".jpg");


        softAssert.assertAll();

    }
    @AfterTest
    public void quit() throws InterruptedException {

        if (TestBase.driver != null)
            Thread.sleep(4000);
        TestBase.driver.quit();
        System.out.println("exit");
    }




}
