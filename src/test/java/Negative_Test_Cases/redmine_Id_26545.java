package Negative_Test_Cases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static Negative_Test_Cases.TestBase.driver;

/**
 * Created by Soumik Mazumder on 9/5/2016.
 */
public class redmine_Id_26545 {
    SoftAssert softAssert = new SoftAssert();

    @Test(priority = 1)
    public void C19177_Weather_forecast_for_next_seven_days_are_not_displayed() throws Exception {
        TestBase utill = new TestBase();
        utill.initialize();

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }

        // Click on Option Button
        TestBase.getObject("Option_Button").click();

        // Click on Weather Button
        TestBase.getObject("Verify_Weather_Button").click();
        Thread.sleep(3000);

        //Screenshot for City_Weather_Screen
        TestBase.get_Screenshot("src\\test\\resource\\redmine_Id_26545\\C19177_Weather_forecast_for_next_seven_days_are_not_displayed/" + "City_Weather_Screen" + ".jpg");

    //Click on City DropDown List
        TestBase.getObject("Drop_Down").click();

        //Select any City and observe the screen
        TestBase.getObject("North_Singapore").click();
        TestBase.get_Screenshot("src\\test\\resource\\redmine_Id_26545\\C19177_Weather_forecast_for_next_seven_days_are_not_displayed/" + "North_Singapore_Weather" + ".jpg");
        TestBase.getObject("North_Singapore").click();

        TestBase.getObject("South_Singapore").click();
        TestBase.get_Screenshot("src\\test\\resource\\redmine_Id_26545\\C19177_Weather_forecast_for_next_seven_days_are_not_displayed/" + "South_Singapore_Weather" + ".jpg");
        TestBase.getObject("South_Singapore").click();

        TestBase.getObject("East_Singapore").click();
        TestBase.get_Screenshot("src\\test\\resource\\redmine_Id_26545\\C19177_Weather_forecast_for_next_seven_days_are_not_displayed/" + "East_Singapore_Weather" + ".jpg");
        TestBase.getObject("East_Singapore").click();

        TestBase.getObject("West_Singapore").click();
        TestBase.get_Screenshot("src\\test\\resource\\redmine_Id_26545\\C19177_Weather_forecast_for_next_seven_days_are_not_displayed/" + "West_Singapore_Weather" + ".jpg");
        TestBase.getObject("West_Singapore").click();

        TestBase.getObject("Central_Singapore").click();
        TestBase.get_Screenshot("src\\test\\resource\\redmine_Id_26545\\C19177_Weather_forecast_for_next_seven_days_are_not_displayed/" + "Central_Singapore_Weather" + ".jpg");
    }


    @AfterTest
    public void quit() throws InterruptedException {

        if (driver != null)
            Thread.sleep(4000);
        driver.quit();
        System.out.println("exit");
    }




}

