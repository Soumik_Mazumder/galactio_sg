package Negative_Test_Cases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

/**
 * Created by Soumik Mazumder on 8/29/2016.
 */
public class redmine_Id_27336 {
    SoftAssert softAssert = new SoftAssert();

    @Test(priority = 1)
    public void C18945_Dashboard_Traffic_data_is_not_getting_loaded_on_Incidents_section() throws Exception {

        TestBase utill = new TestBase();
        utill.initialize();
        Thread.sleep(3000);

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }

        softAssert.assertTrue(TestBase.isElementPresent("Incident_List"), "Incident_List is not present");
        Thread.sleep(2000);
        softAssert.assertTrue(TestBase.isElementPresent("Incident_List"), "Incident_List is not present");
        Thread.sleep(1000);

        //Take a screen shots and verify
        TestBase.get_Screenshot("src\\test\\resource\\redmine_Id_27336\\C18945_Dashboard_Traffic_data_is_not_getting_loaded_on_Incidents_section/" + "Incident" + ".jpg");


    }

    @AfterTest
    public void quit() throws InterruptedException {

        if (TestBase.driver != null)
            Thread.sleep(4000);
        TestBase.driver.quit();
        System.out.println("exit");
    }
}
