package Negative_Test_Cases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static Negative_Test_Cases.TestBase.driver;

/**
 * Created by Soumik Mazumder on 8/31/2016.
 */
public class redmine_Id_26432 {

    SoftAssert softAssert = new SoftAssert();

    @Test(priority = 1)
    public void C19050_Issues_in_verifying_the_checkbox_of_clear_data_pop_up_window() throws Exception {
        TestBase utill = new TestBase();
        utill.initialize();

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }

        //Go to the Options>Clear Data
        TestBase.getObject("Option_Button").click();
        TestBase.driver.scrollTo("Settings");
        TestBase.getObject("Settings_Button").click();
        TestBase.driver.scrollTo("Clear Data");
        TestBase.getObject("ClearData_Button").click();

        TestBase.getObject("Clear_Recent").click();
        TestBase.getObject("Clear_Favourite").click();
        TestBase.getObject("Clear_Home").click();
        TestBase.getObject("Clear_Office").click();

        //Click on Cancel button
        TestBase.getObject("Cancel_Button").click();
        Thread.sleep(1000);

        TestBase.getObject("ClearData_Button").click();
        Thread.sleep(1000);

        TestBase.get_Screenshot("src\\test\\resource\\redmine_Id_26432\\C19050_Issues_in_verifying_the_checkbox_of_clear_data_pop_up_window/" + "Option_Should_not_Select" + ".jpg");
    }

    @AfterTest
    public void quit() throws InterruptedException {

        if (driver != null)
            Thread.sleep(4000);
        driver.quit();
        System.out.println("exit");
    }
}
