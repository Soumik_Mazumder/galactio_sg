package Negative_Test_Cases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static Negative_Test_Cases.TestBase.driver;

/**
 * Created by Soumik Mazumder on 9/5/2016.
 */
public class redmine_Id_20125 {

    SoftAssert softAssert = new SoftAssert();

    @Test(priority = 1)
    public void C19177_Weather_forecast_for_next_seven_days_are_not_displayed() throws Exception {
        TestBase utill = new TestBase();
        utill.initialize();

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }


        // Click on Option Button
        TestBase.getObject("Option_Button").click();
        TestBase.driver.scrollTo("Weather");

        softAssert.assertTrue(TestBase.isElementPresent("Weather_Text"), "Weather_Text Not present in the screen");

        // Click on Weather Button
        TestBase.getObject("Verify_Weather_Button").click();
        Thread.sleep(3000);


        softAssert.assertTrue(TestBase.isElementPresent("Weather_Text"), "Weather_Text Not present in the screen");

        softAssert.assertFalse(TestBase.isElementPresent("No_weather_data"), "No_weather_data is present in the screen");
    }


    @AfterTest
    public void quit() throws InterruptedException {

        if (driver != null)
            Thread.sleep(4000);
        driver.quit();
        System.out.println("exit");
    }
}
