package Negative_Test_Cases;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static Negative_Test_Cases.TestBase.driver;

/**
 * Created by Soumik Mazumder on 9/2/2016.
 */
public class redmine_Id_20075 {

    SoftAssert softAssert = new SoftAssert();

    @Test(priority = 1)
    public void C19126_Application_crash_Report_For_POI_Location() throws Exception {
        TestBase utill = new TestBase();
        utill.initialize();

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }

        //Go to POI Search and search any location:
        TestBase.getObject("Search_Button").click();
        TestBase.getObject("POI_Options").click();

        TestBase.getObject("Search_POI_field").click();
        TestBase.getObject("Search_POI_field").sendKeys(TestBase.TestData.getProperty("Location_Crash"));
        TestBase.getObject("Search_button").click();
        Thread.sleep(3000);

        //Click on first location
        TestBase.getObject("Location_Id").click();
        Thread.sleep(2000);

        TestBase.getObject("Back_button").click();
        Thread.sleep(4000);

        //Verify application not crashing
        softAssert.assertTrue(TestBase.isElementPresent("Result_Text"), "Result_Text is not present in the screen ");
    }



    @AfterTest
    public void quit() throws InterruptedException {

        if (driver != null)
            Thread.sleep(4000);
        driver.quit();
        System.out.println("exit");
    }

}
