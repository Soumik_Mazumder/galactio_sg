import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import java.util.List;

/**
 * Created by Soumik Mazumder on 7/22/2016.
 */
public class Settings_Options_UI_Verification {

    @Test(priority = 1)
    public void C001_Verify_the_Settings_Options_verification() throws Exception {
        TestBase utill = new TestBase();
        utill.initialize();
        Thread.sleep(2000);

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }

        //Go to Option menue and verify_the_Settings options
        //Click on Option Button
        TestBase.getObject("Option_Button").click();
        TestBase.driver.scrollTo("Settings");

        Assert.assertTrue(TestBase.isElementPresent("Setting_Option"), "Setting_Option is not present in the option menue ");
    }

    @Test(priority = 2)
    public void C002_Checking_the_UI_in_Settings_page() throws Exception {

        //Click on Settings option
        TestBase.getObject("Setting_Option").click();

        //Take a Screenshot For UI check
        TestBase.get_Screenshot("src\\test\\resource\\Settings_Options_UI_Verification\\C002_Checking_the_UI_in_Settings_page/" + "Setting_Page_UI1" + ".jpg");
        TestBase.driver.scrollTo("Clear Data");
        TestBase.get_Screenshot("src\\test\\resource\\Settings_Options_UI_Verification\\C002_Checking_the_UI_in_Settings_page/" + "Setting_Page_UI2" + ".jpg");
    }


    @Test(priority = 3)
    public void C003_Verify_all_Category_Subcategory_sections_in_Settings_page() throws Exception {

        //Verify all the options of settings page
        TestBase.driver.scrollTo("Graphics");

        Assert.assertTrue(TestBase.isElementPresent("Graphics_text"), "Graphics_text is not present in the Settings page ");
        Assert.assertTrue(TestBase.isElementPresent("Traffic_Overlay_Button"), "Traffic_Overlay_Button is not present in the Settings page ");
        Assert.assertTrue(TestBase.isElementPresent("Junction_View"), "Junction_View is not present in the Settings page ");
        Assert.assertTrue(TestBase.isElementPresent("Map_Profile"), "Map_Profile is not present in the Settings page");
        Assert.assertTrue(TestBase.isElementPresent("Advanced_option"), "Advanced_option is not present in the Settings page ");
        Assert.assertTrue(TestBase.isElementPresent("General_text"), "General_text is not present in the Settings page ");
        Assert.assertTrue(TestBase.isElementPresent("Voice_Guidance_Button"), "Voice_Guidance_Button is not present in the Settings page ");
        TestBase.driver.scrollTo("Clear Data");
        Assert.assertTrue(TestBase.isElementPresent("Guidance_Frequency"), "Guidance_Frequency is not present in the Settings page ");
        Assert.assertTrue(TestBase.isElementPresent("Route_Type"), "Route_Type is not present in the Settings page ");
        Assert.assertTrue(TestBase.isElementPresent("Display_Language"), "Display_Language is not present in the Settings page ");

        Assert.assertTrue(TestBase.isElementPresent("Voice_Language"), "Voice_Language is not present in the Settings page ");
        Assert.assertTrue(TestBase.isElementPresent("Recet_text"), "Recet_text is not present in the Settings page ");
        Assert.assertTrue(TestBase.isElementPresent("Clear_Data"), "Clear_Data is not present in the Settings page ");
        TestBase.driver.scrollTo("Graphics");
    }

    @Test(priority = 4)
    public void C004_Verify_the_Traffic_Overlay_Button_verification() throws Exception {

        //Verify the check and unchecked functionality

        if(TestBase.getObject("Traffic_Button").getAttribute("checked").equals("true")){
            System.out.println("passed");
        }else{
            Assert.assertTrue(1>2, "unchecked");
        }

        //Click on Traffic overlay button
        TestBase.getObject("Traffic_Overlay_Button").click();

        if(TestBase.getObject("Traffic_Button").getAttribute("checked").equals("true")){
            System.out.println("passed");
        }else{
            System.out.println("Successfully Unchecked");
        }
    }


    @Test(priority = 5)
    public void C005_Verify_the_Map_Profile_option_verification() throws Exception {

        Assert.assertTrue(TestBase.isElementPresent("Auto_Text"), "Auto status is not present in the map profile option ");

        //Click on the map profile
        TestBase.getObject("Map_Profile").click();

        // Verify all the options for Map profile

        Assert.assertTrue(TestBase.isElementPresent("Map_Profile_header"), "Map_Profile_header is not present in map profile popup ");
        Assert.assertTrue(TestBase.isElementPresent("Auto_Text"), "Auto_Text is not present in map profile popup  ");
        Assert.assertTrue(TestBase.isElementPresent("Day_Text"), "Day_Text is not present in map profile popup ");
        Assert.assertTrue(TestBase.isElementPresent("Night_Text"), "Night_Text is not present in map profile popup  ");

        Assert.assertTrue(TestBase.isElementPresent("CANCEL_Button_setting"), "CANCEL_Button is not present in map profile popup  ");
       // Assert.assertTrue(TestBase.isElementPresent("OK_Button_Setting"), "OK_Button is not present in map profile popup  ");

        //Click on "Night" button and verify
        TestBase.getObject("Night_Text").click();
        Thread.sleep(1000);
        Assert.assertTrue(TestBase.isElementPresent("Night_Text"), "Night_Text is not present in map profile popup  ");

        //Cancel Button verification
        TestBase.getObject("Map_Profile").click();
        Thread.sleep(1000);
        TestBase.getObject("CANCEL_Button_setting").click();
    }

    @Test(priority = 6)
    public void C006_Verify_the_Juction_view_settings_options() throws Exception {

        //Verify the Junction view options
        Assert.assertTrue(TestBase.isElementPresent("Junction_view"), "Junction_view is not present in Settings ");
        Assert.assertTrue(TestBase.isElementPresent("Small_JV_View"), "Small_JV_View option is not selected by default  ");
        //Click on JV option
        TestBase.getObject("Junction_view").click();

        //Verify the Junction view option popup
        Assert.assertTrue(TestBase.isElementPresent("JV_text"), "JV_text is not present in JV options popup  ");
        Assert.assertTrue(TestBase.isElementPresent("Small_JV_View"), "Small_JV_View is not present in JV options popup");
        Assert.assertTrue(TestBase.isElementPresent("No_JV_View"), "No_JV_View is not present in JV options popup");

        Assert.assertTrue(TestBase.isElementPresent("Large_JV"), "Large_JV is not present in JV options popup");
        //Assert.assertTrue(TestBase.isElementPresent("OK_Button_Setting"), "OK_Button is not present in JV options popup  ");
        Assert.assertTrue(TestBase.isElementPresent("CANCEL_Button_setting"), "CANCEL_Button_setting is not present in JV options popup  ");

        //Verify the check and unchecked functionality
        TestBase.get_Screenshot("src\\test\\resource\\Settings_Options_UI_Verification\\C006_Verify_the_Juction_view_settings_options/" + "JV_Option_checked" + ".jpg");

        //Click on "Small Junction View" and verify the selection
        TestBase.getObject("Large_JV").click();
        Thread.sleep(1000);
        Assert.assertTrue(TestBase.isElementPresent("Large_JV"), "Large_JV is not present in selected JV options");

    }

    @Test(priority = 7)
    public void C007_Checking_the_Advance_setting_options() throws Exception {

        //Verify the advanced option
        Assert.assertTrue(TestBase.isElementPresent("Advance_option"), "Advance_option is not present in sSettings page");
        TestBase.getObject("Advance_option").click();

        //Verify the header
        Assert.assertTrue(TestBase.isElementPresent("Setting_Option"), "Setting_Option is not present in header");
        Assert.assertTrue(TestBase.isElementPresent("Back_button_S"), "Back_button is not present in header");

        //Verify the all the Advanced options
        Assert.assertTrue(TestBase.isElementPresent("Auto_Zoom"), "Auto_Zoom is not present in Advanced option");
        Assert.assertTrue(TestBase.isElementPresent("Show_POI"), "Show_POI is not present in Advanced option");
        Assert.assertTrue(TestBase.isElementPresent("Show_Block_No"), "Show_Block_No is not present in Advanced option");
        Assert.assertTrue(TestBase.isElementPresent("3D_Landmaark"), "3D_Landmaark is not present in Advanced option");
        Assert.assertTrue(TestBase.isElementPresent("Nort_Face"), "Nort_Face is not present in Advanced option");
        Assert.assertTrue(TestBase.isElementPresent("DRL_Option"), "DRL_Option is not present in Advanced option");
        Assert.assertTrue(TestBase.isElementPresent("Font_Size"), "Font_Size is not present in Advanced option");
        Assert.assertTrue(TestBase.isElementPresent("Medium_Option"), "Medium_Option is not selected by default for Font Size");

        //Checking the checked and unchecked functionality for Advanced options

        //Auto_Zoom
        List<MobileElement> elements = TestBase.driver.findElements(By.className("android.widget.Switch"));
        MobileElement element = elements.get(0);
        element.click();
        Thread.sleep(1000);
        if(TestBase.getObject("CheckBox").getAttribute("checked").equals("false")){
            System.out.println("passed");
        }else{
            System.out.println("Test failed");
        }

        //Show_POI

        MobileElement element1 = elements.get(1);
        element1.click();
        Thread.sleep(1000);
        if(TestBase.getObject("CheckBox").getAttribute("checked").equals("false")){
            System.out.println("passed");
        }else{
            System.out.println("Test failed");
        }


        //Show_Block_No :

        MobileElement element2 = elements.get(2);
        element2.click();
        Thread.sleep(1000);
        if(TestBase.getObject("CheckBox").getAttribute("checked").equals("false")){
            System.out.println("passed");
        }else{
            System.out.println("Test failed");
        }

        //3D_Landmark :

        MobileElement element3 = elements.get(3);
        element3.click();
        Thread.sleep(1000);
        if(TestBase.getObject("CheckBox").getAttribute("checked").equals("false")){
            System.out.println("passed");
        }else{
            System.out.println("Test failed");
        }


        //North_facing :
        MobileElement element4 = elements.get(4);
        element4.click();
        Thread.sleep(2000);
        if(TestBase.getObject("CheckBox").getAttribute("checked").equals("true")){
            System.out.println("passed");
        }else{
            System.out.println("Test failed");
        }

        //Destination_Reference_Line :
        MobileElement element5 = elements.get(5);
        element5.click();
        Thread.sleep(1000);
        if(TestBase.getObject("CheckBox").getAttribute("checked").equals("false")){
            System.out.println("passed");
        }else{
            System.out.println("Test failed");
        }
    }

    @Test(priority = 8)
    public void C008_Verify_the_Font_Size_option_in_Andvance_settings() throws Exception {

        //Verify the Font size
        Assert.assertTrue(TestBase.isElementPresent("Font_Size"), "Font_Size is not present in Advanced option");
        Assert.assertTrue(TestBase.isElementPresent("Medium_Option"), "Medium_Option is not selected by default for Font Size");

        //Click on Font Size option
        TestBase.getObject("Font_Size").click();

        //Verify the Font Size options popup
        Assert.assertTrue(TestBase.isElementPresent("Font_Size"), "Font_Size header is not present in option popup ");
        Assert.assertTrue(TestBase.isElementPresent("Small"), "Small option is not present in option popup ");
        Assert.assertTrue(TestBase.isElementPresent("Medium"), "Medium is not present in option popup ");
        Assert.assertTrue(TestBase.isElementPresent("Large"), "Large header is not present in option popup ");
        Assert.assertTrue(TestBase.isElementPresent("Extra_Large"), "Extra_Large is not present in option popup ");


        if(TestBase.getObject("Medium").getAttribute("checked").equals("true")){
            System.out.println("passed");
        }else{
            Assert.assertTrue(1>2, "unchecked");
        }

        //Click and select any other option and verify the selection
        TestBase.getObject("Large").click();
        Assert.assertTrue(TestBase.isElementPresent("Large"), "Selected option(Large) is not displaying along with the Font_Size Popup");

        //Click on the "CANCEL' optiion and verify
        TestBase.getObject("Font_Size").click();
        TestBase.getObject("CANCEL_Button_setting").click();
        TestBase.getObject("Back_button").click();

    }

    @Test(priority = 9)
    public void C009_Checking_the_Voice_Guidance_button() throws Exception {

        //Voice guidance option verification
        Assert.assertTrue(TestBase.isElementPresent("Voice_Guidance"), "Voice_Guidance option is not present ");

        if(TestBase.getObject("Voice_check").getAttribute("checked").equals("false")){
            System.out.println("passed");
        }else{
            Assert.assertTrue(1>2, "unchecked");
        }

        Thread.sleep(2000);

        TestBase.getObject("Voice_Guidance").click();
        Thread.sleep(1000);

        if(TestBase.getObject("Voice_check").getAttribute("checked").equals("false")){
            System.out.println("passed");
        }else{
            Assert.assertTrue(1>2, "unchecked");
        }

        Thread.sleep(1000);
        TestBase.getObject("Voice_Guidance").click();

    }


    @Test(priority = 10)
    public void C010_Verify_the_Scroll_functionality_in_Settings_page() throws Exception {
        //verify the scroll
        TestBase.driver.scrollTo("Clear Data");
    }

    @Test(priority = 11)
    public void C011_Verify_the_Guidance_Frequency_options() throws Exception {
        Assert.assertTrue(TestBase.isElementPresent("Guidance_Frequency"), "Guidance_Frequency is not present in the Settings page ");

        TestBase.getObject("Guidance_Frequency").click();

        Assert.assertTrue(TestBase.isElementPresent("Expressway"), "Expressway option is not present in the Settings page ");
        Assert.assertTrue(TestBase.isElementPresent("Expressway_Distance"), "Expressway_Distance option is not present in the Settings page ");

        Assert.assertTrue(TestBase.isElementPresent("Other_Roads"), "Other_Roads option is not present in the Settings page ");
        Assert.assertTrue(TestBase.isElementPresent("Other_Roads_Distance"), "Other_Roads_Distance option is not present in the Settings page ");

        TestBase.getObject("Back_button").click();
        TestBase.driver.scrollTo("Clear Data");
    }

    @Test(priority = 12)
    public void C012_Checking_the_Route_Type_option_checking() throws Exception {

        Assert.assertTrue(TestBase.isElementPresent("Route_Type"), "Route_Type is not present in the Settings page ");
        Assert.assertTrue(TestBase.isElementPresent("Fastest_Route"), "Fastest_Route is not present by default selection ");

        TestBase.getObject("Route_Type").click();

        //Verify the route type popup
        Assert.assertTrue(TestBase.isElementPresent("Route_Type"), "Route_Type_header is not present in RouteType_popup ");

        Assert.assertTrue(TestBase.isElementPresent("Traffic_Aware"), "Traffic_Aware is not present in RouteType_popup ");
        Assert.assertTrue(TestBase.isElementPresent("Fastest"), "Fastest is not present in RouteType_popup ");
        Assert.assertTrue(TestBase.isElementPresent("Shortest"), "Shortest is not present in RouteType_popup ");
        Assert.assertTrue(TestBase.isElementPresent("Toll_minimize"), "Toll_minimize is not present in RouteType_popup ");
        Assert.assertTrue(TestBase.isElementPresent("Toll_avoidance"), "Toll_avoidance is not present in RouteType_popup ");
        Assert.assertTrue(TestBase.isElementPresent("Route_Type"), "Route_Type_header is not present in RouteType_popup ");
        Assert.assertTrue(TestBase.isElementPresent("Route_Type"), "Route_Type_header is not present in RouteType_popup ");
        //Assert.assertTrue(TestBase.isElementPresent("OK_Button_Setting"), "OK_Button is not present in RouteType_popup  ");
        Assert.assertTrue(TestBase.isElementPresent("CANCEL_Button_setting"), "CANCEL_Button_setting is not present in RouteType_popup ");

        //Click on any option and verify the options
        TestBase.getObject("Shortest").click();
        Assert.assertTrue(TestBase.isElementPresent("Shortest"), "Shortest is not present in RouteType_popup ");
        TestBase.getObject("Route_Type").click();

        TestBase.getObject("CANCEL_Button_setting").click();

    }

    @Test(priority = 13)
    public void C013_Checking_the_Display_Language_Settings_options() throws Exception {

        Assert.assertTrue(TestBase.isElementPresent("Display_Language"), "Display_Language is not present in the Settings page ");
        Assert.assertTrue(TestBase.isElementPresent("English"), "English is not present in Display language seletion");

        TestBase.getObject("Display_Language").click();

        //Verify the Display Language settings
        Assert.assertTrue(TestBase.isElementPresent("Display_Language"), "Display_Language text is not present in the display language popup");
        Assert.assertTrue(TestBase.isElementPresent("English"), "English is not present in Display language seletion");
        Assert.assertTrue(TestBase.isElementPresent("CANCEL_Button_setting"), "CANCEL_Button_setting is not present in Display language seletion");
        //Assert.assertTrue(TestBase.isElementPresent("OK_Button_Setting"), "OK_Button_Setting is not present in Display language seletion");

        //Click on Cancel
        TestBase.getObject("CANCEL_Button_setting").click();
    }


    @Test(priority = 14)
    public void C014_Checking_the_Voice_Language_Settings_options() throws Exception {

        Assert.assertTrue(TestBase.isElementPresent("Voice_Language"), "Voice_Language is not present in the Settings page ");
        Assert.assertTrue(TestBase.isElementPresent("English"), "English is not present in Display language seletion");

        TestBase.getObject("Voice_Language").click();

        //Verify all the options
        Assert.assertTrue(TestBase.isElementPresent("Voice_Language"), "Voice_Language header is not present in the voice language popup");
        Assert.assertTrue(TestBase.isElementPresent("English"), "English is not present in Voice language popup");
       // Assert.assertTrue(TestBase.isElementPresent("English_TTS"), "English_TTS is not present in Voice language popup");
        Assert.assertTrue(TestBase.isElementPresent("CANCEL_Button_setting"), "CANCEL_Button_setting is not present in Voice language popup");
        //Assert.assertTrue(TestBase.isElementPresent("OK_Button_Setting"), "English_TTS is not present in Voice language popup");

        //Select any other options
        //TestBase.getObject("English_TTS").click();
        //Assert.assertTrue(TestBase.isElementPresent("English_TTS"), "English_TTS is not selected as Voice language ");

        //Verify the CANCEL button
       // TestBase.getObject("Voice_Language").click();
        TestBase.getObject("CANCEL_Button_setting").click();
    }

    @Test(priority = 15)
    public void C015_Verify_the_Clear_Data_Options() throws Exception {
        Assert.assertTrue(TestBase.isElementPresent("Clear_Data"), "Clear_Data is not present in the Settings page ");
        TestBase.getObject("Clear_Data").click();

        //Verify all the options

        Assert.assertTrue(TestBase.isElementPresent("Clear_Recents"), "Clear_Recents is not present in the Clear Data popup ");
        Assert.assertTrue(TestBase.isElementPresent("Clear_Favourites"), "Clear_Favourites is not present in the Clear Data popup ");
        Assert.assertTrue(TestBase.isElementPresent("Clear_Home1"), "Clear_Home1 is not present in the Clear Data popup ");
        Assert.assertTrue(TestBase.isElementPresent("Clear_Office1"), "Clear_Office1 is not present in the Clear Data popup ");
        Assert.assertTrue(TestBase.isElementPresent("CANCEL_Button_setting"), "CANCEL_Button_setting is not present in the Clear Data popup ");
        Assert.assertTrue(TestBase.isElementPresent("OK_Button_Setting"), "OK_Button_Setting is not present in the Clear Data popup ");

        //Select any options
        TestBase.getObject("Clear_Home1").click();
        TestBase.getObject("Clear_Favourites").click();

        TestBase.getObject("OK_Button_Setting").click();

        //Verify the CANCEL option
        TestBase.getObject("Clear_Data").click();
        TestBase.getObject("CANCEL_Button_setting").click();
        TestBase.getObject("Back_button").click();
    }

    @AfterTest
    public void quit() throws InterruptedException {

        if (TestBase.driver != null)
            Thread.sleep(4000);
        TestBase.driver.quit();
        System.out.println("exit");
    }



}



