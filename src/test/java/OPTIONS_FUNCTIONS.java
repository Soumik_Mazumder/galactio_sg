import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

/**
 * Created by QI 33 on 09-05-2016.
 */
public class OPTIONS_FUNCTIONS {

    @Test(priority = 1)

    public void C001_Checking_display_of_Options_menu() throws Exception {

        TestBase utill = new TestBase();
        utill.initialize();
        //Click on dashboard option button
        TestBase.getObject("Option_Button").click();
        Thread.sleep(1000);
        //Screenshot for list of  Option menu
        TestBase.get_Screenshot("src\\test\\resource\\OPTIONS_FUNCTIONS\\C001_Checking_display_of_Options_menu/" + "Option_Menu_Screen1" + ".jpg");
        //Scroll to setting button
        TestBase.driver.scrollTo("Settings");
        //Screenshot for next list of Option menu
        TestBase.get_Screenshot("src\\test\\resource\\OPTIONS_FUNCTIONS\\C001_Checking_display_of_Options_menu/" + "Option_Menu_Screen2" + ".jpg");
        //Scroll to Account button because when option menu is open account button is not displayed so for that we use scroll
        TestBase.driver.scrollTo("Account");

    }

    @Test(priority = 2)

    public void C002_Verify_LogIn_Functionality_from_Option_Account_Link() throws Exception {

        //click on Account button
        TestBase.getObject("Verify_Account_Button").click();
        Thread.sleep(2000);
        //Verify and taking Screenshot for Account page
        TestBase.get_Screenshot("src\\test\\resource\\OPTIONS_FUNCTIONS\\C002_Verify_LogIn_Functionality_from_Option_Account_Link/" + "Account_Page" + ".jpg");
        Assert.assertTrue(TestBase.isElementPresent("Verify_Account_Button"), "C003:Account_Text is not present in the header of Account_Page ");


        //Verify mapSYNQ_Account_text is present in the Account screen
        Assert.assertTrue(TestBase.isElementPresent("Verify_mapSynq_text"), "C004:Your_mapSYNQ_Account_text is not present in Account_Page screen");


        //Verify Create New Account Button and take Screenshot for Create New Account screen
        Assert.assertTrue(TestBase.isElementPresent("Create_New_Account_Button"), "C005:Create_New_Account_text is not present in Account_Page screen");
        TestBase.getObject("Create_New_Account_Button").click();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\OPTIONS_FUNCTIONS\\C002_Verify_LogIn_Functionality_from_Option_Account_Link/" + "Create_New_Account_Page" + ".jpg");

        //Click on back Button twice
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
    }


    @Test(priority = 3)

    public void C003_Verify_Options_Incidents_Screen() throws Exception {

        TestBase.getObject("Option_Button").click();

        //Click on Incidents_Button and take screenshot
        TestBase.getObject("Verify_Incidents_Button").click();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\OPTIONS_FUNCTIONS\\C003_Verify_Options_Incidents_Functionality/" + "Incidents_screen" + ".jpg");

        //Click on User_Reported_Tab
        TestBase.getObject("Verify_User_Reported_Tab").click();

        //Verify the No user reported message text and take screenshot of it
        Assert.assertTrue(TestBase.isElementPresent("No_User_reported_Incidents_text"), "C003:No_User_reported_Incidents_text is not present in the User_Reported_Tab  ");
        TestBase.get_Screenshot("src\\test\\resource\\OPTIONS_FUNCTIONS\\C003_Verify_Options_Incidents_Functionality/" + "User_Reported_Incidents_screen" + ".jpg");
        TestBase.getObject("Back_button").click();
    }


 @Test(priority = 4)

    public void C004_Verify_Options_Calender_Screen() throws Exception {

        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Calendar_Button").click();
        Thread.sleep(2000);
        //Screenshot for Calendar screen before login into map synq account
        TestBase.get_Screenshot("src\\test\\resource\\OPTIONS_FUNCTIONS\\C004_Verify_Options_Calender_Functionality/" + "Pop_up_for_login map synq account" + ".jpg");

        //Click again on Option_Button
        TestBase.getObject("Option_Button").click();
        TestBase.login_function();
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Calendar_Button").click();
        Thread.sleep(3000);
        Assert.assertTrue(TestBase.isElementPresent("You_Dont_Have_Any_Events_Text"), "C004:You_Dont_Have_Any_Events_Text is not present in the calendar screen ");
        //Screenshot for Calendar screen after login into map synq account
        TestBase.get_Screenshot("src\\test\\resource\\OPTIONS_FUNCTIONS\\C004_Verify_Options_Calender_Functionality/" + "Calendar icon" + ".jpg");

        TestBase.getObject("Back_button").click();
        TestBase.getObject("Option_Button").click();
        TestBase.logout_function();
        TestBase.getObject("Back_button").click();
    }


    @Test(priority = 5)

    public void C005_Verify_Options_Camera_Screen() throws Exception {
       // //Verify_Camera_Header
        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Camera_Button").click();
        Assert.assertTrue(TestBase.isElementPresent("Verify_Camera_Text"), "C004:Verify_Camera_Text is not present in the header of the Camera screen ");

        //Verify Groups_camera_options
        Assert.assertTrue(TestBase.isElementPresent("Verify_Groups_Tab"), "C001:Groups_Text is not present in the Camera screen ");
        Thread.sleep(9000);
        TestBase.get_Screenshot("src\\test\\resource\\OPTIONS_FUNCTIONS\\C005_Verify_Options_Camera_Functionality/" + "Groups_Cameras_screen" + ".jpg");

        //Click on Nearby_Camera_Options
        TestBase.getObject("Verify_Nearby_Camera_Tab").click();
        Thread.sleep(9000);
        TestBase.get_Screenshot("src\\test\\resource\\OPTIONS_FUNCTIONS\\C005_Verify_Options_Camera_Functionality/" + "Nearby_cameras_screen" + ".jpg");


        //Verify Favourite_camera_options
        TestBase.getObject("Verify_Favourites_Camera_Tab").click();
        Assert.assertTrue(TestBase.isElementPresent("no_favourites_msg1"), "C004:no_favourites_msg1 is not present in Favourite tab of Camera screen ");
        TestBase.get_Screenshot("src\\test\\resource\\OPTIONS_FUNCTIONS\\C005_Verify_Options_Camera_Functionality/" + "Star_icon_Favourites_Cameras_screen" + ".jpg");
        TestBase.getObject("Back_button").click();
    }

    @Test(priority = 6)

    public void C006_Verify_Options_Parking_Screen() throws Exception {
        //Click on Options button
        TestBase.getObject("Option_Button").click();
        //Click on Parking_Button
        TestBase.getObject("Verify_Parking_Button").click();
        Thread.sleep(5000);
        //Screenshot for Parking_Screen
        TestBase.get_Screenshot("src\\test\\resource\\OPTIONS_FUNCTIONS\\C006_Verify_Options_Parking_Functionality/" + "Parking_Screen" + ".jpg");
        //Click back button
        TestBase.getObject("Back_button").click();
    }

    @Test(priority = 7)

    public void C007_Verify_Options_Fuel_Screen() throws Exception {
        // Click on Fuel_Button
        TestBase.getObject("Option_Button").click();
       // Click on Option Button
        TestBase.getObject("Verify_Fuel_Button").click();
        Thread.sleep(5000);
        //Screenshot for Fuel_Type_screen
        TestBase.get_Screenshot("src\\test\\resource\\OPTIONS_FUNCTIONS\\C007_Verify_Options_Fuel_Functionality/" + "Fuel_Type_screen" + ".jpg");
        // Click on Fuel_Brand_Tab
        TestBase.getObject("Verify_Brand_Text").click();
        //Screenshot for Fuel_Brand_screen
        TestBase.get_Screenshot("src\\test\\resource\\OPTIONS_FUNCTIONS\\C007_Verify_Options_Fuel_Functionality/" + "Fuel_Brand_screen" + ".jpg");
        // Click on Back_button
        TestBase.getObject("Back_button").click();
    }

    @Test(priority = 8)

    public void C008_Verify_Options_Prayer_Time_Screen() throws Exception {
        // Click on Option Button
        TestBase.getObject("Option_Button").click();

        // Click on Prayer_Time_Button
        TestBase.getObject("Prayer_Time_Button").click();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\OPTIONS_FUNCTIONS\\C008_Verify_Options_Prayer_Time_Functionality/" + "City_Prayer_Time_screen" + ".jpg");

        // Click on Favourites
        TestBase.getObject("Favourites").click();
        Assert.assertTrue(TestBase.isElementPresent("no_favourites_msg1"), "C001: No Favourites yet... is not present in the Favourite Tab");
        TestBase.get_Screenshot("src\\test\\resource\\OPTIONS_FUNCTIONS\\C008_Verify_Options_Prayer_Time_Functionality/" + "Favourites_Prayer_Time_screen" + ".jpg");

       // click on Nearby_Mosques
        TestBase.getObject("Verify_Nearby_Mosques").click();
        Thread.sleep(1000);
        TestBase.get_Screenshot("src\\test\\resource\\OPTIONS_FUNCTIONS\\C008_Verify_Options_Prayer_Time_Functionality/" + "Nearby_Mosques_Prayer_Time_screen" + ".jpg");

        //click on Prayer_Time_Alert
        TestBase.getObject("Prayer_Time_Alert").click();
        Thread.sleep(1000);
        TestBase.get_Screenshot("src\\test\\resource\\OPTIONS_FUNCTIONS\\C008_Verify_Options_Prayer_Time_Functionality/" + "Prayer_Time_Alert_screen" + ".jpg");
        TestBase.getObject("Back_button").click();
    }

    @Test(priority = 9)

    public void C009_Verify_Options_Weather_Screen() throws Exception {

        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Weather_Button").click();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\OPTIONS_FUNCTIONS\\C009_Options_Weather_Functionality/" + "City_Weather_Screen" + ".jpg");
        TestBase.getObject("Verify_Favourites_Weather").click();
        Thread.sleep(1000);
        Assert.assertTrue(TestBase.isElementPresent("no_favourites_msg1"), "C009: No Favourites yet... is not present in the Favourite Tab");
        TestBase.get_Screenshot("src\\test\\resource\\OPTIONS_FUNCTIONS\\C009_Options_Weather_Functionality/" + "Favourites_Weather_Screen" + ".jpg");
        TestBase.getObject("Back_button").click();

    }

    @Test(priority = 10)

    public void C010_Verify_Report_Incidents_Screen() throws Exception {
        // Click on Option Button
        TestBase.getObject("Option_Button").click();
        // Click on Reported_Incidents Button
        TestBase.getObject("Verify_Reported_Incidents").click();
        Thread.sleep(2000);
        //Screenshot for Favourites_Weather_Screen
        TestBase.get_Screenshot("src\\test\\resource\\OPTIONS_FUNCTIONS\\C010_Verify_Report_Incidents_Functionality/" + "Pop Up Login map synq account" + ".jpg");//Before login into Map synq account
        // Click on Option Button
        TestBase.getObject("Option_Button").click();
        //Call login_function from TesBase class for login into mapsynq account
        TestBase.login_function();
        // Click on Back_button
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Reported_Incidents").click();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\OPTIONS_FUNCTIONS\\C010_Verify_Report_Incidents_Functionality/" + "Reported_Incidents_Screen1" + ".jpg");
        TestBase.driver.scrollTo("Describe the incident here ...");
        TestBase.get_Screenshot("src\\test\\resource\\OPTIONS_FUNCTIONS\\C010_Verify_Report_Incidents_Functionality/" + "Reported_Incidents_Screen2" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Option_Button").click();
        TestBase.logout_function();
        TestBase.getObject("Back_button").click();



    }

    @Test(priority = 11)

    public void C011_Verify_Popup_for_Share_Position_before_Login_Into_Mapsynq_Account() throws Exception {

        TestBase.getObject("Option_Button").click();
        TestBase.driver.scrollTo("About");
        TestBase.getObject("Share_Position_Button").click();
        TestBase.get_Screenshot("src\\test\\resource\\OPTIONS_FUNCTIONS\\C011_Verify_Popup_for_Share_Position_before_Login_Into_Mapsynq_Account/" + "Tips message for share position before login " + ".jpg");
        TestBase.getObject("Option_Button").click();
        TestBase.driver.scrollTo("Account");
        TestBase.login_function();
        TestBase.getObject("Back_button").click();

    }

        @Test(priority = 12)

        public void C012_Verify_Share_Position_Screen_After_Login_Into_Mapsynq_Account() throws Exception {

        TestBase.getObject("Option_Button").click();
        TestBase.driver.scrollTo("About");
        TestBase.getObject("Share_Position_Button").click();
            TestBase.get_Screenshot("src\\test\\resource\\OPTIONS_FUNCTIONS\\C012_Verify_Share_Position_Screen_After_Login_Into_Mapsynq_Account/" + "Share_poistion screen" + ".jpg");
            TestBase.getObject("Back_button").click();
            TestBase.getObject("Option_Button").click();
            TestBase.driver.scrollTo("Account");
            TestBase.logout_function();
            TestBase.getObject("Back_button").click();
    }

    @Test(priority = 13)

    public void C013_Verify_Send_Location_Screen() throws Exception {


        TestBase.getObject("Option_Button").click();
        TestBase.driver.scrollTo("About");
    TestBase.getObject("Send_Location_Button").click();
    TestBase.driver.navigate().back();
    Assert.assertTrue(TestBase.isElementPresent("Current_Location_Button"), "C002:Current_Location text is not present in the Search_Location_Screen_");
    Assert.assertTrue(TestBase.isElementPresent("Recent_button"), "C002:Recent text is not present in the Search_Location_Screen_");
    Assert.assertTrue(TestBase.isElementPresent("POI"), "C003: POI text is not present in the Search_Location_Screen_");
    Assert.assertTrue(TestBase.isElementPresent("Favourites"), "C004: Favourites text is not present in the Search_Location_Screen_");
    Assert.assertTrue(TestBase.isElementPresent("Coordinates"), "C005: Coordinates text is not present in the Search_Location_Screen_");
    Assert.assertTrue(TestBase.isElementPresent("My_Home_text_1"), "C006: My_Home text is not present in the Search_Location_Screen_");
    Assert.assertTrue(TestBase.isElementPresent("My_Home_text_2"), "C006: Not set text is not present in the Search_Location_Screen_");
    Assert.assertTrue(TestBase.isElementPresent("My_Office_text_1"), "C006: My_Office_text is not present in the Search_Location_Screen_");
    Assert.assertTrue(TestBase.isElementPresent("My_Office_text_2"), "C007: Not set text is not present in the Search_Location_Screen_");
    TestBase.get_Screenshot("src\\test\\resource\\SEND_LOCATION_FUNCTION\\C013_Verify_Send_Location_Screen/" + "Verification_Send location ICON" + ".jpg");
        TestBase.getObject("Back_button").click();
}



    @Test(priority = 14)

    public void C014_Verify_Saved_Ads_Screen() throws Exception {


        TestBase.getObject("Option_Button").click();
        Assert.assertTrue(TestBase.isElementPresent("Saved_Ads_Button"), "C002:Saved_Ads_Button is not present in the Options menu");
        TestBase.getObject("Saved_Ads_Button").click();
        Assert.assertTrue(TestBase.isElementPresent("Saved_Ads_Text"), "C002:Saved_Ads_Text is not present in the header of the Saved_Ads screen");
        Assert.assertTrue(TestBase.isElementPresent("No_Saved_Ads_Yet_Text"), "C002:No_Saved_Ads_Yet_Text is not present in the Saved_Ads screen");
        TestBase.get_Screenshot("src\\test\\resource\\SEND_LOCATION_FUNCTION\\C014_Verify_Saved_Ads_Screen/" + "Verification_Saved_Ads_All UI" + ".jpg");
        TestBase.getObject("Back_button").click();
    }

    @Test(priority = 15)

    public void C015_Settings_Screen_Screen() throws Exception {

        TestBase.getObject("Option_Button").click();
        Assert.assertTrue(TestBase.isElementPresent("Settings_Button"), "C0015:Saved_Ads_Button is not present in the Options menu");
        TestBase.getObject("Settings_Button").click();
        TestBase.get_Screenshot("src\\test\\resource\\CAMERA_FUNCTION\\C015_Settings_Screen_Screen/" + "Settings_Screen_UI" + ".jpg");
        Assert.assertTrue(TestBase.isElementPresent("Settings_Text"), "C0015:Saved_Ads_Button is not present in the header of the Settings screen");
        Assert.assertTrue(TestBase.isElementPresent("Graphics_Text"), "C0015:Graphics_Text is not present in the Settings screen");
        Assert.assertTrue(TestBase.isElementPresent("Map_Profile_Text"), "C0015:Map_Profile_Text is not present in the Settings screen");
        try {
            Assert.assertTrue(TestBase.isElementPresent("Auto_Map_Profile"));
        }
        catch(AssertionError e){
            System.out.println("Auto Profile is not set now");
        }
        try {
            Assert.assertTrue(TestBase.isElementPresent("Day_Map_Profile"));
        }
        catch(AssertionError e){
            System.out.println("Day_Map_Profile is not set now");
        }
        try {
            Assert.assertTrue(TestBase.isElementPresent("Night_Map_Profile"));
        }
        catch(AssertionError e){
            System.out.println("Night_Map_Profile is not set now");
        }

        //Junction view
        Assert.assertTrue(TestBase.isElementPresent("Junction_View_Text"), "C0015:Junction_View_Text is not present in the Settings screen");
        try {
            Assert.assertTrue(TestBase.isElementPresent("Large_Junction_View"));
        }
        catch(AssertionError e){
            System.out.println("Large_Junction_View is not set now");
        }
        try {
            Assert.assertTrue(TestBase.isElementPresent("Small_Junction_View"));
        }
        catch(AssertionError e){
            System.out.println("Small_Junction_View is not set now");
        }
        try {
            Assert.assertTrue(TestBase.isElementPresent("No_Junction_View"));
        }
        catch(AssertionError e){
            System.out.println("No_Junction_View is not set now");
        }

        Assert.assertTrue(TestBase.isElementPresent("Advanced_Text"), "C0015:Advanced_Text is not present in the Settings screen");
        Assert.assertTrue(TestBase.isElementPresent("General_Text"), "C0015:General_Text is not present in the Settings screen");
        Assert.assertTrue(TestBase.isElementPresent("Voice_Guidance_Text"), "C0015:Voice_Guidance_Text is not present in the Settings screen");
        Assert.assertTrue(TestBase.isElementPresent("Guidance_Frequency_Text"), "C0015:Guidance_Frequency_Text is not present in the Settings screen");

        Assert.assertTrue(TestBase.isElementPresent("Route_Type_Text"), "C0015:Route_Type_Text is not present in the Settings screen");
        try {
            Assert.assertTrue(TestBase.isElementPresent("Fastest_Route_Text"));
        }
        catch(AssertionError e){
            System.out.println("Fastest_Route_Text is not set now");
        }
        try {
            Assert.assertTrue(TestBase.isElementPresent("Shortest_Route_Text"));
        }
        catch(AssertionError e){
            System.out.println("Shortest_Route_Text is not set now");
        }
        try {
            Assert.assertTrue(TestBase.isElementPresent("Toll_Minimized_Route_Text"));
        }
        catch(AssertionError e){
            System.out.println("Toll_Minimized_Route_Text is not set now");
        }
        try {
            Assert.assertTrue(TestBase.isElementPresent("Toll_Avoidance_Route_Text"));
        }
        catch(AssertionError e){
            System.out.println("Toll_Avoidance_Route_Text is not set now");
        }

        TestBase.driver.scrollTo("Clear Data");

        Assert.assertTrue(TestBase.isElementPresent("Display_Language_Text"), "C0015:Display_Language_Text is not present in the Settings screen");
        Assert.assertTrue(TestBase.isElementPresent("Display_English_text"), "C0015:Display_English_text is not present in the Settings screen");
        Assert.assertTrue(TestBase.isElementPresent("Reset_Text"), "C0015:Reset_Text is not present in the Settings screen");
        Assert.assertTrue(TestBase.isElementPresent("Clear_Data_Text"), "C0015:Clear_Data_Text is not present in the Settings screen");
        TestBase.get_Screenshot("src\\test\\resource\\CAMERA_FUNCTION\\C015_Settings_Screen_Screen/" + "Settings_Screen_UI" + ".jpg");
        TestBase.getObject("Back_button").click();
    }

    @Test(priority = 16)

    public void C016_Verify_Set_Origin_Screen() throws Exception {

        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Set_Origin_Button").click();
        Assert.assertTrue(TestBase.isElementPresent("Search_Origin_text"), "C0015:Search_Origin_text is not present in the Search Origin screen");
        Assert.assertTrue(TestBase.isElementPresent("Recent_Text"), "C0015:Recent_Text is not present in the Search Origin screen");
        Assert.assertTrue(TestBase.isElementPresent("POI_Text"), "C0015:POI_Text is not present in the Search Origin screen");
        Assert.assertTrue(TestBase.isElementPresent("Favourite_Text"), "C0015:Favourite_Text is not present in the Search Origin screen");
        Assert.assertTrue(TestBase.isElementPresent("Coordinates_Text"), "C0015:Coordinates_Text is not present in the Search Origin screen");
        TestBase.get_Screenshot("src\\test\\resource\\CAMERA_FUNCTION\\C016_Verify_Set_Origin_Screen/" + "Search_Origin_Screen_UI" + ".jpg");
        TestBase.getObject("Back_button").click();
    }


    @Test(priority = 17)

    public void C017_Verify_Options_About_Screen() throws Exception {
        //Click on dashboard option button
        TestBase.getObject("Option_Button").click();
        Thread.sleep(1000);
        //Screenshot for list of  Option menu
        TestBase.get_Screenshot("src\\test\\resource\\OPTIONS_FUNCTIONS\\C001_Checking_display_of_Options_menu/" + "Option_Menu_Screen1" + ".jpg");
        //Scroll to setting button
        TestBase.driver.scrollTo("Settings");

        //TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_About_Button").click();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\OPTIONS_FUNCTIONS\\C016_Verify_Options_About_Screen/" + "About_Screen_UI" + ".jpg");

        String Galactio_Beta_Text_Verification= TestBase.getObject("Galactio_Beta_Text").getText();
        String SW_Version_Verification=TestBase.getObject("Galactio_SW_Version").getText();
        String Map_Version_Verification=TestBase.getObject("Galactio_MAP_Version").getText();
        System.out.println(Galactio_Beta_Text_Verification);
        System.out.println(SW_Version_Verification);
        System.out.println(Map_Version_Verification);

        Assert.assertTrue(TestBase.isElementPresent("Verify_Rate_and_Review_Text"), "C002:Rate_and_Review_Text is not present in about menu screen");
        TestBase.getObject("Verify_Rate_and_Review_Text").click();
        Thread.sleep(3000);
        TestBase.get_Screenshot("src\\test\\resource\\OPTIONS_FUNCTIONS\\C016_Verify_Options_About_Screen/" + "Galactio_Play_Store_Version" + ".jpg");
        TestBase.driver.navigate().back();

        //Verify Welcome_Message_Text is present or not in About page
        Assert.assertTrue(TestBase.isElementPresent("Verify_Welcome_Message_Text"), "C003:Welcome_Message_Text is not present in about menu screen");
        TestBase.getObject("Verify_Welcome_Message_Text").click();
        Thread.sleep(4000);
        TestBase.get_Screenshot("src\\test\\resource\\OPTIONS_FUNCTIONS\\C016_Verify_Options_About_Screen/" + "Welcome message screen" + ".jpg");
        TestBase.getObject("Back_button").click();


        //Verify Terms_and_Conditions_Text is present or not in About page
        Assert.assertTrue(TestBase.isElementPresent("Verify_Terms_and_Conditions_Text"), "C004:Terms_and_Conditions_Text is not present in about menu screen");
        TestBase.getObject("Verify_Terms_and_Conditions_Text").click();
        Thread.sleep(3000);
        TestBase.get_Screenshot("src\\test\\resource\\OPTIONS_FUNCTIONS\\C016_Verify_Options_About_Screen/" + "Terms_and_Conditions page" + ".jpg");
        TestBase.driver.navigate().back();
        Thread.sleep(2000);


        //Verify Contact_Support_Text is present
        Assert.assertTrue(TestBase.isElementPresent("Verify_Contact_Support_Text_1"), "C005:Contact_Support_Text is not present in about menu screen");
        Assert.assertTrue(TestBase.isElementPresent("Verify_Contact_Support_Text_2"), "C005:support@mapsynq.com text is not present in about menu screen");
        TestBase.getObject("Verify_Contact_Support_Text_1").click();
        Thread.sleep(3000);
        TestBase.driver.navigate().back();
        TestBase.get_Screenshot("src\\test\\resource\\OPTIONS_FUNCTIONS\\C016_Verify_Options_About_Screen/" + "Open Mail for sending the Galactio App" + ".jpg");
        TestBase.getObject("Mail_To_Send_Edit_field_Intex_Device").clear();
        TestBase.getObject("Mail_To_Send_Edit_field_Intex_Device").sendKeys(TestBase.TestData.getProperty("Mail_to_send_email_id"));
        TestBase.getObject("Mail_Id_Send_button_Intex_Device").click();
        TestBase.get_Screenshot("src\\test\\resource\\OPTIONS_FUNCTIONS\\C016_Verify_Options_About_Screen/" + "Successfully sending Message Toast Message c" + ".jpg");


        //Verify Share_App Options is present or not in About page
        Assert.assertTrue(TestBase.isElementPresent("Verify_Share_App_Text"), "C006:Share_App_Text is not present in option menu screen");
        Thread.sleep(2000);
        TestBase.getObject("Verify_Share_App_Text").click();
        Thread.sleep(2000);
        Assert.assertTrue(TestBase.isElementPresent("Like_the_Galactio_Experience_Text"), "C005:Like_the_Galactio_Experience_Text is not present in the header of Send Location screen");
        Assert.assertTrue(TestBase.isElementPresent("Tell_You_Friends_About_It_Text"), "C005:Tell_You_Friends_About_It_Text is not present in Send Location screen");
        Assert.assertTrue(TestBase.isElementPresent("Facebook_Text"), "C005:Facebook_Text is not present in Send Location map view screen");
        Assert.assertTrue(TestBase.isElementPresent("Email_Text"), "C005:Email_Text is not present in Send Location map view screen");
        Assert.assertTrue(TestBase.isElementPresent("Copy_Link_Text"), "C005:Copy_Link_Text is not present in Send Location map view screen");
        Assert.assertTrue(TestBase.isElementPresent("More_Text"), "C005:More_Text is not present in Send Location map view screen");
        TestBase.get_Screenshot("src\\test\\resource\\SEND_LOCATION_FUNCTION\\C0028_Verify_the_Various_Share_Options_In_Send_Location_Screen_From_My_Home_Location/" + "Various_Share_Options_Icons" + ".jpg");
        TestBase.get_Screenshot("src\\test\\resource\\OPTIONS_FUNCTIONS\\C016_Verify_Options_About_Screen/" + "Share App Screen_UI" + ".jpg");

    }
    @AfterTest

    public void quit(){

        if((TestBase.driver)!=null){
            try {
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        TestBase.driver.quit();
        System.out.println("exit");
    }
}