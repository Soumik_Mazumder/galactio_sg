import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import java.util.List;

/**
 * Created by Soumik on 5/3/2016.
 */
public class EXIT_FUNCTIONAL_TEST {

    @Test(priority = 1)
    public void C001_Verify_The_Exit_Functionality_From_Home_Office() throws Exception {
        TestBase utill = new TestBase();
        utill.initialize();
        Thread.sleep(5000);

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        }
        catch(Exception e){
            System.out.println("No Journey Popup Displaying");
        }

        //Go to Search Destination page (POI/HOME/OFFICE/RECENT/FAVOURITE) and return back
        TestBase.getObject("Search_Button").click();
        TestBase.getObject("POI_Options").click();
        TestBase.getObject("Emergency").click();
        TestBase.getObject("All_Emergency").click();
        Thread.sleep(5000);
        //Now return into the Dashboard

      //  Creating Loop to back from the screen
            for (int i = 0; i < 4; i++) {
                TestBase.getObject("Back_Button").click();
            }
    }

    @Test(priority = 2)
    public void C002_Verify_The_Exit_Functionality_From_SearchDestination() throws Exception {

        TestBase.getObject("Search_Button").click();

        //Search the location name list
        TestBase.getObject("Search_destination_field").click();
        TestBase.getObject("Search_destination_field").sendKeys(TestBase.TestData.getProperty("Search_Proper_Sequence_Location_1"));

        Thread.sleep(2000);
        TestBase.getObject("Search_button").click();

        //  Creating Loop to back from the screen
        for (int i = 0; i < 3; i++) {
            TestBase.getObject("Back_Button").click();
        }
        //Clear all the data Functionality Flow:
        TestBase.clear_data();
        TestBase.getObject("Back_Button").click();
    }

    @Test(priority = 3)
    public void C003_Verify_The_Exit_Functionality_From_Application() throws Exception {

        TestBase.driver.navigate().back();

        //Tap on YES button to close the application
        TestBase.getObject("Yes_Button").click();

    }

    @AfterTest
    public void quit() throws InterruptedException {

        if (TestBase.driver != null)
            Thread.sleep(4000);
        TestBase.driver.quit();
        System.out.println("exit");
    }
}
