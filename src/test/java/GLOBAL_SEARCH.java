import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

/**
 * Created by QI 33 on 22-06-2016.
 */
public class GLOBAL_SEARCH {

   @Test(priority = 1)

    //Verify the search destination screen all options and also screenshot
    public void C001_Verify_the_Label_For_Set_Destination_Page() throws Exception {

        TestBase utill = new TestBase();
        utill.initialize();
        TestBase.getObject("Search_option").click();
        TestBase.driver.navigate().back();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\Global_Search\\C001_Verify_the_Set_Destination_Page/" + "Search_Destination_screen" + ".jpg");
        Assert.assertTrue(TestBase.isElementPresent("Recent_button"), "C001:Recent text is not present in the Search_Destination_screen");
        Assert.assertTrue(TestBase.isElementPresent("POI"), "C002: POI text is not present in the Daashboard screen");
        Assert.assertTrue(TestBase.isElementPresent("Favourites"), "C003: Favourites text is not present in the Daashboard screen");
        Assert.assertTrue(TestBase.isElementPresent("Coordinates"), "C004: Coordinates text is not present in the Daashboard screen");
        Assert.assertTrue(TestBase.isElementPresent("My_Home_text_1"), "C006: My_Home text is not present in the Search_Location_Screen");
        Assert.assertTrue(TestBase.isElementPresent("My_Home_text_2"), "C006: Not set text is not present in the Search_Location_Screen");
        Assert.assertTrue(TestBase.isElementPresent("My_Office_text_1"), "C006: My_Office_text is not present in the Search_Location_Screen");
        Assert.assertTrue(TestBase.isElementPresent("My_Office_text_2"), "C007: Not set text is not present in the Search_Location_Screen");
        Assert.assertTrue(TestBase.isElementPresent("Back_button"), "C008: Back_button is not present in the Daashboard screen");
        TestBase.get_Screenshot("src\\test\\resource\\Global_Search\\C001_Verify_the_Set_Destination_Page/" + "Search_Destination_screen_Icons" + ".jpg");
        TestBase.getObject("Back_button").click();

    }

    @Test(priority = 2)

//Verify the search destination header along with voice recognize button also
    public void C002_Verify_The_SearchDestination_Header() throws Exception{

        TestBase.getObject("Search_option").click();
        Assert.assertTrue(TestBase.isElementPresent("Search_destination_text_field"), "C001: Search text is not present in the  Search Destination Screen");
        Thread.sleep(2000);
        try{
            Assert.assertTrue(TestBase.isElementPresent("Voice_recognize_button"),"C002: Voice_recognize_button is not present in the Search Destination Screen");
            TestBase.getObject("Voice_recognize_button").click();
            TestBase.get_Screenshot("src\\test\\resource\\Global_Search\\C002_Verify_The_SearchDestination_Header/" + "Voice_Recognize pop_up box" + ".jpg");
            TestBase.driver.navigate().back();
            TestBase.getObject("Back_button").click();}
        catch(Exception e){
            TestBase.getObject("Back_button").click();
        }}


//Check for Search option by input Location

    @Test(priority = 3)
    public void C003_Verify_Search_locations_From_Search_Destination_Text_Field() throws Exception {

        TestBase.getObject("Search_option").click();

        //Click on search destination field andd entering location1
        TestBase.getObject("Search_destination_edit_field").click();
        TestBase.getObject("Search_destination_edit_field").sendKeys(TestBase.TestData.getProperty("Search_Proper_Sequence_Location_1"));

        String Text1 = TestBase.getObject("Search_destination_edit_field").getText();

        Assert.assertTrue(TestBase.isElementPresent("Search_button"), "C003:Search_button is not present in search destination text field");
        TestBase.getObject("Search_button").click();

        //Take screenshot for location_1 result
        TestBase.get_Screenshot("src\\test\\resource\\Global_Search\\C003_Verify_Search_locations_From_Search_Destination_Text_Field/" + "location_1_Result" + ".jpg");
        TestBase.getObject("Back_button").click();


        //verify typed location is present in the Search_destination_edit_field if not give error message
        if (TestBase.getObject("Search_destination_edit_field").getText().contains(Text1)) {
            TestBase.getObject("Clear_Data_button").click();
        } else {
            System.out.println("Typed Location is not in search destination edit field");
        }
    }


    @Test(priority = 4)
    public void C004_Verify_Clear_Function_In_Search_Destination_Text_Field() throws Exception {

        Thread.sleep(1000);
        //Clear the typed location in search destination edit field
        Assert.assertFalse(TestBase.isElementPresent("Possible_Location_1"), "C004:sim lim is not present cleared in search destination text field");
    }



    @Test(priority = 5)
    public void C005_Verify_Possible_Location_option_When_Enter_Any_Location_In_Search_Destination_Text_Field() throws Exception {

        //Click on search destination field and entering location1
        TestBase.getObject("Search_destination_edit_field").click();
        TestBase.getObject("Search_destination_edit_field").sendKeys(TestBase.TestData.getProperty("Search_Proper_Sequence_Location_1"));

        String Text1 = TestBase.getObject("Search_destination_edit_field").getText();

        //verify and tap  on suggested location
        Assert.assertTrue(TestBase.isElementPresent("Possible_Location_1"), "C004:SIM LIM SQ is not present in the possible location option");
        TestBase.getObject("Possible_Location_1").click();

        //Take screenshot location_1 result
        TestBase.get_Screenshot("src\\test\\resource\\Global_Search\\C005_Verify_Possible_Location_option_When_Enter_Any_Location_In_Search_Destination_Text_Field/" + "Possible_location_1_Result" + ".jpg");
        TestBase.getObject("Back_button").click();

        //verify typed location is present in the Search_destination_edit_field if not give error message
        if (TestBase.getObject("Search_destination_edit_field").getText().contains(Text1)) {
            TestBase.getObject("Clear_Data_button").click();
        } else {
            System.out.println("Location is not there");
        }
    }


    @Test(priority = 6)
    public void C006_Verify_Next_Possible_Prediction_Locations_option_When_Enter_Any_Location_In_Search_Destination_Text_Field() throws Exception {


        //Click on search destination field and entering location1
        TestBase.getObject("Search_destination_edit_field").click();
        TestBase.getObject("Search_destination_edit_field").sendKeys(TestBase.TestData.getProperty("Search_Proper_Sequence_Location_1"));

        //Verify the Possible_Predication_Locations options is present or not and click on that
        Assert.assertTrue(TestBase.isElementPresent("Possible_Predication_Locations_1"), "C003:Location_1 is not present in the search destination edit field after back from Result screen");
        TestBase.getObject("Possible_Predication_Locations_1").click();

        //Verify the next Possible_Predication_Locations options is present or not after click on previous Possible_Predication_Locations
        Assert.assertTrue(TestBase.isElementPresent("Possible_Predication_Locations_2"), "C003:Possible_Predication_Locations_2 is not present in after tapping one locatiom");
        Assert.assertTrue(TestBase.isElementPresent("Possible_Predication_Locations_3"), "C003:Possible_Predication_Locations_2 is not present in after tapping one locatiom");
        Assert.assertTrue(TestBase.isElementPresent("Possible_Predication_Locations_4"), "C003:Possible_Predication_Locations_2 is not present in after tapping one locatiom");

        //tap on Possible_Predication_Locations_2
        TestBase.getObject("Possible_Predication_Locations_2").click();

        //Take search destination edit field location name in text1 and tap on search
        String Text1 = TestBase.getObject("Search_destination_edit_field").getText();
        System.out.println(Text1);
        TestBase.getObject("Search_button").click();

        //Take screenshot Location_1 result
        TestBase.get_Screenshot("src\\test\\resource\\Global_Search\\C006_Verify_Next_Possible_Predicition_Locations_option_When_Enter_Any_Location_In_Search_Destination_Text_Field/" + "Possible_Predicition_Result_For_Location_1_ " + ".jpg");
        TestBase.getObject("Back_button").click();

        //verify typed location is present in the Search_destination_edit_field if not give error message
        if (TestBase.getObject("Search_destination_edit_field").getText().contains(Text1)) {
            TestBase.getObject("Clear_Data_button").click();
        } else {
            System.out.println("Location is not there");
        }
    }


   @Test(priority = 7)
    public void C007_Verify_Postal_Code_search_After_Entering_Any_Postal_Code_From_Search_Destination() throws Exception {

       //Click on search destination field and send  Postal code
       TestBase.getObject("Search_destination_edit_field").click();
       TestBase.getObject("Search_destination_edit_field").sendKeys(TestBase.TestData.getProperty("Postal_Code_Data"));
       Assert.assertTrue(TestBase.isElementPresent("Postal_Code"), "C007:Postal code is not present in the search destination edit field");
       Assert.assertTrue(TestBase.isElementPresent("Search_Postal_Code_data1"), "C007:Search_Postal_Code_data1 is not present in possible prediction page");
       Assert.assertTrue(TestBase.isElementPresent("Search_Postal_Code_data2"), "C007:Possible_Predication_Locations_2 is not present possible prediction page");
       Assert.assertTrue(TestBase.isElementPresent("Search_Postal_Code_data3"), "C007:Possible_Predication_Locations_2 is not present possible prediction page");
       Assert.assertTrue(TestBase.isElementPresent("Search_Postal_Code_data4"), "C007:Possible_Predication_Locations_2 is not possible prediction page");
       TestBase.get_Screenshot("src\\test\\resource\\Global_Search\\C006_Verify_Postal_Code_search_After_Entering_Any_Postal_Code_From_Search_Destination/" + "Postal_Code_Possible_Prediction_options" + ".jpg");
       TestBase.getObject("Search_Postal_Code_data1").click();

       String postal_code= TestBase.getObject("Search_destination_edit_field").getText();
       Assert.assertTrue(TestBase.isElementPresent("Search_button"), "C006:Search_button is not present in the search destination edit field screen");
       TestBase.getObject("Search_button").click();
       TestBase.get_Screenshot("src\\test\\resource\\Global_Search\\C007_Verify_Postal_Code_search_After_Entering_Any_Postal_Code_From_Search_Destination/" + "Postal_Code1_Result" + ".jpg");
       TestBase.getObject("Back_button").click();

       //verify typed location is present in the Search_destination_edit_field if not give error message
       if (TestBase.getObject("Search_destination_edit_field").getText().contains(postal_code)) {
           TestBase.getObject("Clear_Data_button").click();
       } else {
           System.out.println("Postal code is not there");
       }
   }


    @Test(priority = 8)
    public void C008_Verify_Address_search_After_Entering_Any_Address_From_Search_Destination() throws Exception {

        TestBase.getObject("Search_destination_edit_field").click();
        TestBase.getObject("Search_destination_edit_field").sendKeys(TestBase.TestData.getProperty("Address_Search_Data1"));
        String address=  TestBase.getObject("Search_destination_edit_field").getText();
        TestBase.getObject("Search_button").click();
        TestBase.get_Screenshot("src\\test\\resource\\Global_Search\\C003_Check_Search_Input_Criteria_and_ResultSet/" + "Address_Search_Result" + ".jpg");
        TestBase.getObject("Back_button").click();

        //verify typed location is present in the Search_destination_edit_field if not give error message
        if (TestBase.getObject("Search_destination_edit_field").getText().contains(address)) {
            TestBase.getObject("Clear_Data_button").click();
        } else {
            System.out.println("Address is not there");
        }
    }



    //check search option in out of sequence search
    @Test(priority = 9)
    public void C009_Check_Out_Of_Sequence_Search() throws Exception {

        //Click on search destination field and check for Address result with screenshot
        TestBase.getObject("Search_destination_edit_field").click();
        TestBase.getObject("Search_destination_edit_field").sendKeys(TestBase.TestData.getProperty("Search_OutOf_Sequence_Location_1"));
        Assert.assertTrue(TestBase.isElementPresent("Search_out_of_sequence_data_1"), "C009:Search_out_of_sequence_data_1 is not present in the next possible prediction options");
        TestBase.getObject("Search_out_of_sequence_data_1").click();
        Assert.assertTrue(TestBase.isElementPresent("Possible_Location_1"), "C009:Possible_Location_1 is not present in the Possible_Location_option");
        Assert.assertTrue(TestBase.isElementPresent("Search_out_of_sequence_data_2"), "C009:Search_out_of_sequence_data_2 is not present in the next possible prediction options");
        TestBase.getObject("Search_out_of_sequence_data_2").click();
        String Out_Of_Sequence_text = TestBase.getObject("Search_destination_edit_field").getText();
        System.out.println("Out_Of_Sequence_Text_In_Search_destination_field:" + Out_Of_Sequence_text);
        TestBase.getObject("Search_button").click();
        TestBase.get_Screenshot("src\\test\\resource\\Global_Search\\C009_Check_Out_Of_Sequence_Search/" + "Out_Of_Sequence_Search_result" + ".jpg");
        TestBase.getObject("Back_button").click();

        if (TestBase.getObject("Search_destination_edit_field").getText().contains(Out_Of_Sequence_text)) {

            System.out.println("Out_Of_Sequence_Text_present_In_Search_destination_field_After_Back_from_Result");
            TestBase.getObject("Clear_Data_button").click();
        } else {
            System.out.println("Out_Of_Sequence_Text_Not_present_In_Search_destination_field_After_Back_from_Result");
        }

    }

    @Test(priority = 10)
    public void C010_Check_Out_Of_No_Result_Found_Message_After_Enter_Any_Invalid_Location_From_Search_Destination() throws Exception {


        TestBase.getObject("Search_destination_edit_field").click();
        TestBase.getObject("Search_destination_edit_field").sendKeys(TestBase.TestData.getProperty("No_Result_found_Location"));
        Assert.assertTrue(TestBase.isElementPresent("No_Result_Found_Message"), "C009:No_Result_Found_Message is not present in the when we enter invalid location");
        TestBase.getObject("Clear_Data_button").click();
    }

    @Test(priority = 11)
    public void C011_Verify_Result_text_in_header_of_the_result_screen_after_search_any_location_from_serach_destination() throws Exception {
        {
            TestBase.getObject("Search_destination_edit_field").click();
            TestBase.getObject("Search_destination_edit_field").sendKeys(TestBase.TestData.getProperty("Search_Proper_Sequence_Location_1"));
            TestBase.getObject("Search_button").click();
            Thread.sleep(2000);
            Assert.assertTrue(TestBase.isElementPresent("Result_text"), "C009:Result_text is not present in the header of the result screen");

        }

    }
        @Test(priority = 12)
        public void C012_Verify_Map_View_screen_after_tapping_any_Location_from_results_screen() throws Exception
        {
            TestBase.getObject("Possible_Location_1").click();
            TestBase.get_Screenshot("src\\test\\resource\\Global_Search\\C012_Verify_Result_text_in_header_of_the_result_screen_after_search_any_location_from_search_destination/" + "Map view screen" + ".jpg");

       for(int i=0;i<=3;i++){
           TestBase.getObject("Back_button").click();
       }
            TestBase.clear_data();
        }



    @AfterTest
    public void quit(){

        if((TestBase.driver)!=null){
            try {
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        TestBase.driver.quit();
        System.out.println("exit");
    }

}
