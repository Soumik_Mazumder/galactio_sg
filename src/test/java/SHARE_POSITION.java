import io.appium.java_client.TouchAction;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

/**
 * Created by Soumik on 6/23/2016.
 */
public class SHARE_POSITION {

    @Test(priority = 1)
    public void C001_Verify_the_Share_Position_from_Option_Icon_Text() throws Exception {
        TestBase utill = new TestBase();
        utill.initialize();
        Thread.sleep(2000);

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        } catch (Exception e) {
            System.out.println("No Journey Popup Displaying");
        }

        //Click on Option Button
        TestBase.getObject("Option_Button").click();
        TestBase.driver.scrollTo("Share Position");


        //Verify the Text and icon
        Assert.assertTrue(TestBase.isElementPresent("Share_position"), "Share_position is not present in screen");

    }

    @Test(priority = 2)
    public void C002_Verify_Share_Position_Option_when_not_Login() throws Exception {

        //Click on the Share position option when not login
        TestBase.getObject("Share_position").click();


        //Tesseract
        TestBase.getScreenshotForParticularPart("Toast_Parent","src\\test\\resource\\SHARE_POSITION\\C002_Verify_Share_Position_Option_when_not_Login/" + "Share_Position_Toast" + ".jpg");
        String result = TestBase.TextFromImage("src\\test\\resource\\SHARE_POSITION\\C002_Verify_Share_Position_Option_when_not_Login/" + "Share_Position_Toast" + ".jpg");
        System.out.println(result);
        String exp1 = "Please";
        String exp2 = "share feature";



        if ((result.toLowerCase().contains(exp1.toLowerCase())) && (result.toLowerCase().contains(exp2.toLowerCase()))) {
            System.out.println("passed");
        } else {
            Assert.assertTrue(1 > 2, "The toast notification is not displayed correctly for incorrect password");
        }

        //Take a screen shots for verifying the toast message
        TestBase.get_Screenshot("src\\test\\resource\\SHARE_POSITION\\C002_Verify_Share_Position_Option_when_not_Login/" + "Toast_Message" + ".jpg");
    }

    @Test(priority = 3)
    public void C003_Verify_Share_Position_Option_when_user_Login_Successfully() throws Exception {
        TestBase.getObject("Option_Button").click();
        TestBase.driver.scrollTo("Account");

        //LogIn into the MapSYNQ
        TestBase.getObject("Verify_Account_Button").click();
        TestBase.getObject("SharePosition_email_label").click();
        TestBase.getObject("SharePosition_email_label").sendKeys(TestBase.TestData.getProperty("SharePosition_user_name"));

        Thread.sleep(2000);
        TestBase.getObject("share_pwd_field").click();
        TestBase.getObject("share_pwd_field").sendKeys(TestBase.TestData.getProperty("SharePosition_pwd"));

        TestBase.driver.navigate().back();
        TestBase.getObject("share_Log_in_btn").click();
        Thread.sleep(3000);
        TestBase.getObject("Back_Button").click();

        // Click on Option and open the Share Position Page
        TestBase.getObject("Option_Button").click();
        Thread.sleep(1000);
        TestBase.driver.scrollTo("Share Position");
        //Thread.sleep(1000);
        TestBase.getObject("Share_position").click();

    }

    @Test(priority = 4)
    public void C004_Verify_the_Share_Page_when_No_Share_is_present() throws Exception {
        //Take a screen shots to verify the Share Page
        TestBase.get_Screenshot("src\\test\\resource\\SHARE_POSITION\\C004_Verify_the_Share_Page_when_No_Share_is_present/" + "UI_Share_Page" + ".jpg");

        //verify all the text in the share page
        Assert.assertTrue(TestBase.isElementPresent("Share_position"), "Share_position label is not there");

        //Share Text verification(Image view)
        //Tesseract
        TestBase.getScreenshotForParticularPart("Share_Details_text", "src\\test\\resource\\SHARE_POSITION\\C004_Verify_the_Share_Page_when_No_Share_is_present/" + "Share_Page_Text" + ".jpg");
        String result1 = TestBase.TextFromImage("src\\test\\resource\\SHARE_POSITION\\C004_Verify_the_Share_Page_when_No_Share_is_present/" + "Share_Page_Text" + ".jpg");
        System.out.println("******************");
        System.out.println(result1);
        Thread.sleep(1000);

       if ((result1.contains("Share Position")) && (result1.contains("Share your real-time driving"))&& (result1.contains("& traffic conditions"))&& (result1.contains("with your contacts. Provide your"))&&(result1.contains("friends with an answer to the"))) {
            System.out.println("Pass ");
        } else {
            Assert.assertTrue(1 > 2, "The toast notification is not displayed correctly for incorrect password");
        }

        //Verify Create New Share button
        Assert.assertTrue(TestBase.isElementPresent("New_Share_Button"), "New_Share_Button button is not present");


    }

    @Test(priority = 5)
    public void C005_Click_On_CREATE_NEW_SHARE_Button() throws Exception {

        //Click on the Share Button
        TestBase.getObject("New_Share_Button").click();

    }

    @Test(priority = 6)
    public void C006_Verify_the_Share_Position_page_After_click_on_New_Share_button_UI_and_Text_verification() throws Exception {

        //Verify the Share Position Page
        TestBase.get_Screenshot("src\\test\\resource\\SHARE_POSITION\\C006_Verify_the_Share_Position_page_After_click_on_New_Share_button_UI_and_Text_verification/" + "UI_Share_Page_after_NewShare_Clicked" + ".jpg");


        //Verify the all text in that page
        Assert.assertTrue(TestBase.isElementPresent("Share_text"), "Share_text option is not present");
        Assert.assertTrue(TestBase.isElementPresent("For_Set_Duration"), "For_Set_Duration text is not present");
        Assert.assertTrue(TestBase.isElementPresent("Show_Tracks"), "Show_Tracks  is not present");
        Assert.assertTrue(TestBase.isElementPresent("Share_Button"), "Share_Button is not present");
        Assert.assertTrue(TestBase.isElementPresent("Checked_Unchecked_Button"), "Checked_Unchecked_Button  is not present");
    }

    @Test(priority = 7)
    public void C007_Verify_the_Share_options() throws Exception {

        //Click on share option and verify all the options
        TestBase.getObject("For_Set_Duration").click();

        Assert.assertTrue(TestBase.isElementPresent("Forever_Share_Option"), "Forever_Share_Option is not present");
        Assert.assertTrue(TestBase.isElementPresent("Current_drive_Option"), "Current_drive_Option  is not present");

        TestBase.getObject("For_Set_Duration").click();

    }

    @Test(priority = 8)
    public void C008_Verify_the_Check_and_Uncheck_functionality_for_Show_Track_option() throws Exception {
        //Verify the Checked and unchecked Button
        Assert.assertTrue(TestBase.isElementPresent("Checked_Unchecked_Button"), "Checked_Unchecked_Button  is not present");
        TestBase.getObject("Checked_Unchecked_Button").click();
        Thread.sleep(2000);

        TestBase.get_Screenshot("src\\test\\resource\\SHARE_POSITION\\C008_Verify_the_Check_and_Uncheck_functionality_for_Show_Track_option/" + "Unchecked_ShowTrack" + ".jpg");
        Thread.sleep(2000);

        TestBase.getObject("Checked_Unchecked_Button").click();

        TestBase.get_Screenshot("src\\test\\resource\\SHARE_POSITION\\C008_Verify_the_Check_and_Uncheck_functionality_for_Show_Track_option/" + "checked_ShowTrack" + ".jpg");
        Thread.sleep(2000);

    }

    @Test(priority = 9)
    public void C009_Verify_the_Share_Button_functionality_where_user_should_landed_on_New_Share_Page() throws Exception {
        TestBase.getObject("Share_Button").click();
        TestBase.get_Screenshot("src\\test\\resource\\SHARE_POSITION\\C009_Verify_the_Share_Button_functionality_where_user_should_landed_on_New_Share_Page/" + "Loading_Share_Screen" + ".jpg");
        Thread.sleep(3000);

    }

    @Test(priority = 10)
    public void C010_Verify_the_NEW_SHARE_page_UI_and_text() throws Exception {

        //Verify the New share page
        Assert.assertTrue(TestBase.isElementPresent("New_Share"), "New_Share header is not present");
        Assert.assertTrue(TestBase.isElementPresent("Ready_Share_Text"), "Ready_Share_Text text is not present");
        Assert.assertTrue(TestBase.isElementPresent("Choose_recipients_text"), "Choose_recipients_text  is not present");

        Assert.assertTrue(TestBase.isElementPresent("Facebook_Option"), "Facebook_Option  is not present");
        Assert.assertTrue(TestBase.isElementPresent("Email_Options"), "Email_Options  is not present");
        Assert.assertTrue(TestBase.isElementPresent("Copy_Link"), "Copy_Link  is not present");
        Assert.assertTrue(TestBase.isElementPresent("More_Options"), "More_Options  is not present");
        //Assert.assertTrue(TestBase.isElementPresent("map_SYNQ_Link"), "map_SYNQ_Link  is not present");
        Assert.assertTrue(TestBase.isElementPresent("Done_Button"), "Done_Button  is not present");

        //Screenshots for UI verification
        TestBase.get_Screenshot("src\\test\\resource\\SHARE_POSITION\\C010_Verify_the_NEW_SHARE_page_UI_and_text/" + "Share_Screen_UI" + ".jpg");

    }

    @Test(priority = 11)
    public void C011_Verify_Share_functionality_using_Facebook() throws Exception {

        //Click on Facebook Option and make sure Facebook app should be present and setup into the device
        TestBase.getObject("Facebook_Option").click();
        Thread.sleep(5000);

        //Take a screen shots to verify the facebook share options
        TestBase.get_Screenshot("src\\test\\resource\\SHARE_POSITION\\C011_Verify_Share_functionality_using_Facebook/" + "Facebook_Share_popup" + ".jpg");

        //Click on Post button to post in facebook
        TestBase.getObject("POST_Button_For_Facebook").click();
        Thread.sleep(3000);



        TestBase.get_Screenshot("src\\test\\resource\\SHARE_POSITION\\C011_Verify_Share_functionality_using_Facebook/" + "toast_message" + ".jpg");
        Thread.sleep(1000);
    }

    @Test(priority = 12)
    public void C012_Verify_Share_Functionality_Using_Email() throws Exception {
        //Verify email option
        TestBase.getObject("Email_Options").click();
        Thread.sleep(1000);

        //Verify the email content and details
        TestBase.getObject("Share_email").click();
        try{
            TestBase.getObject("r_gmail").click();
        }catch(Exception e){

            Thread.sleep(2000);
            //TestBase.driver.navigate().back();


            if((TestBase.getObject("Share_email_Body").getText().contains("I'm driving with Galactio GPS Navigation App."))&&(TestBase.getObject("Share_email_Body").getText().contains("Hey,")) &&(TestBase.getObject("Share_email_Body").getText().contains("You can follow my drive here http://www.mapsynq.com/"))){

        }else{
                Assert.assertTrue(1>2, "mail body shows wrong content");
            }

            if(TestBase.getObject("Mail_Subject_Share").getText().contains("Galactio Android Navigation App!")){
                System.out.println("passed");
            }else{
                Assert.assertTrue(1>2, "mail subject shows wrong content");
            }

            try {
                Thread.sleep(3000);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }

            TestBase.get_Screenshot("src\\test\\resource\\SHARE_POSITION\\C012_Verify_Share_Functionality_Using_Email/" + "screen1" + ".jpg");

        }

        //Enter the valid email id and password and send the email successfully
        TestBase.getObject("To_email_Section").sendKeys("soumik.mazumder@quantuminventions.com");
        Thread.sleep(1000);
        TestBase.getObject("Send_button_Email").click();

        //Take a screenshots for successful send message
        TestBase.get_Screenshot("src\\test\\resource\\SHARE_POSITION\\C012_Verify_Share_Functionality_Using_Email/" + "Send_message_successful" + ".jpg");

    }

    @Test(priority = 13)
    public void C013_Verify_Share_Functionality_Using_Copy_Link() throws Exception {
        TestBase.getObject("Copy_Link").click();
        Thread.sleep(1000);
        TestBase.get_Screenshot("src\\test\\resource\\SHARE_POSITION\\C013_Verify_Share_Functionality_Using_Copy_Link/" + "Copy_Link" + ".jpg");

    }

    @Test(priority = 14)
    public void C014_Verify_Share_Functionality_Using_More_option_verification() throws Exception {

        TestBase.getObject("More_Options").click();

        //Take a screen shots for More options verification
        TestBase.get_Screenshot("src\\test\\resource\\SHARE_POSITION\\C014_Verify_Share_Functionality_Using_More_option_verification/" + "More_Options" + ".jpg");
        TestBase.driver.navigate().back();



    }

    @Test(priority = 15)
    public void C015_Verify_DoneButton_Functionality_to_create_Share() throws Exception {
        //Click on Done Button
        TestBase.getObject("Done_Button").click();

    }

    @Test(priority = 16)
    public void C016_Checking_the_LIVE_SHARE_Location() throws Exception {

        //Go to Share Position page and verify the Live share
        TestBase.getObject("Option_Button").click();
        TestBase.driver.scrollTo("Share Position");
        TestBase.getObject("Share_position").click();
        Thread.sleep(2000);

        Assert.assertTrue(TestBase.isElementPresent("Live_Section"), "Live_Section  is not present");

    }

    @Test(priority = 17)
    public void C017_Select_any_Live_Share_Verify_the_LocationShare_Page() throws Exception {

        //Click on Active Live share
        TestBase.getObject("Share_Status_Live_History").click();

        //Verify the Share page


        Assert.assertTrue(TestBase.isElementPresent("Share_position"), "Share_position header  is not present");
        Assert.assertTrue(TestBase.isElementPresent("Share_text"), "Share_text  is not present");
        Assert.assertTrue(TestBase.isElementPresent("Duration_Text"), "Duration_Text  is not present");
        Assert.assertTrue(TestBase.isElementPresent("Show_Tracks_Text"), "Show_Tracks_Text  is not present");
        Assert.assertTrue(TestBase.isElementPresent("Link"), "Link  is not present");
        Assert.assertTrue(TestBase.isElementPresent("Disable_Button"), "Disable_Button  is not present");

        //Verify with screen shots
        TestBase.get_Screenshot("src\\test\\resource\\SHARE_POSITION\\C017_Select_any_Live_Share_Verify_the_LocationShare_Page/" + "Share_Screen_LiveSHare" + ".jpg");


    }

    @Test(priority = 18)
    public void C018_Check_the_Share_Button_functionality_and_verify_from_Live_Share() throws Exception {

        //Click on Share and Done Button
        TestBase.getObject("Share_Button_LiveShare").click();
        Thread.sleep(2000);

        //Click on Done Button
        TestBase.getObject("Done_Button").click();

    }

    @Test(priority = 19)
    public void C019_Check_the_Disable_Button_function_and_verify_from_Live_Share() throws Exception {

        //Go to Share page and select Live share  to check the disable button
        TestBase.getObject("Option_Button").click();
        TestBase.driver.scrollTo("Share Position");
        TestBase.getObject("Share_position").click();
        Thread.sleep(2000);

        Assert.assertTrue(TestBase.isElementPresent("Live_Section"), "Live_Section  is not present");

        //Click on Active Live share
        TestBase.getObject("Share_Status_Live_History").click();


        Assert.assertTrue(TestBase.isElementPresent("Disable_Button"), "Disable_Button  is not present");
        TestBase.getObject("Disable_Button").click();
    }

    @Test(priority = 20)
    public void C020_Verify_the_History_Section_after_Disable_the_Share() throws Exception {

        //Take a screen shots after disable the share
        TestBase.get_Screenshot("src\\test\\resource\\SHARE_POSITION\\C020_Verify_the_History_Section_after_Disable_the_Share/" + "History_Share" + ".jpg");

        //Verify the History page
        Assert.assertTrue(TestBase.isElementPresent("History_Section"), "Disable_Button  is not present");

    }

    @Test(priority = 21)
    public void C021_Verify_Long_tap_Function_and_popup_verification() throws Exception {

        //Long Press on Disabled share
        TouchAction action = new TouchAction(TestBase.driver);
        action.longPress(TestBase.getObject("Share_Status_Live_History")).perform();
        Thread.sleep(5000);

        //Verify all the options for delete share popup
        Assert.assertTrue(TestBase.isElementPresent("Delete_Option"), "Delete_Option  is not present");
        Assert.assertTrue(TestBase.isElementPresent("Delete_All_Option"), "Delete_All_Option  is not present");
        Assert.assertTrue(TestBase.isElementPresent("Cancel_Option"), "Cancel_Option  is not present");
        Thread.sleep(2000);

    }

    @Test(priority = 22)
    public void C022_Verify_the_Cancel_button_function_and_check() throws Exception {

        //Click on Cancel button
        TestBase.getObject("Cancel_Option").click();
        Assert.assertFalse(TestBase.isElementPresent("Cancel_Option"), "Cancel_Option  is Still present in the screen");

    }

    @Test(priority = 23)
    public void C023_Verify_the_DeleteAll_button_function_and_check() throws Exception {

        //Long Press on Disabled share
        TouchAction action = new TouchAction(TestBase.driver);
        action.longPress(TestBase.getObject("Share_Status_Live_History")).perform();
        Thread.sleep(5000);
        TestBase.getObject("Delete_All_Option").click();

        Assert.assertFalse(TestBase.isElementPresent("Share_Status_Live_History"), "All History are still not deleted");
    }

    @Test(priority = 24)
    public void C024_Verify_the_Share_Feature_using_Forever_Share_type() throws Exception {
        //Go to Share page to select Forever share
        TestBase.getObject("New_Share_Button").click();
        Thread.sleep(1000);

        //Click on share option and verify all the options
        TestBase.getObject("For_Set_Duration").click();
        Thread.sleep(1000);
        TestBase.getObject("Forever_Share_Option").click();

        //Verify the Share page after selecting the Forever type of share
        TestBase.get_Screenshot("src\\test\\resource\\SHARE_POSITION\\C024_Verify_the_Share_Feature_using_Forever_Share_type/" + "Forever_Share_Page" + ".jpg");
        Thread.sleep(1000);

        TestBase.getObject("Share_Button").click();
        //Click on Done Button
        TestBase.getObject("Done_Button").click();

        //Verify the Forever share successful or Not
        TestBase.getObject("Option_Button").click();
        TestBase.driver.scrollTo("Share Position");
        TestBase.getObject("Share_position").click();
        Thread.sleep(2000);

        //Verify the forever share present in the Live share section
        Assert.assertTrue(TestBase.isElementPresent("Sharing_Forever"), "Sharing_Forever  is not present");

    }

    @Test(priority = 25)
    public void C025_Verify_the_Back_functionality_for_Share_Position() throws Exception {

        //Click on Back and return to the dashboard
        TestBase.getObject("Back_Share").click();

    }

    @Test(priority = 26)
    public void C026_Verify_After_SharePosition_test_Logout_Successfully() throws Exception {


    }


    @AfterTest
    public void quit() throws InterruptedException {

        if (TestBase.driver != null)
            Thread.sleep(4000);
        TestBase.driver.quit();
        System.out.println("exit");
    }

 }
